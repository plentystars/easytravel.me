<?php

/**
 * This is the model class for table "tariffs_372".
 *
 * The followings are the available columns in table 'tariffs_372':
 * @property integer $id
 * @property string $country
 * @property string $country_ru
 * @property double $cost_call_in
 * @property double $cost_call_out
 * @property double $cost_sms_out
 */
class Tariffs372 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tariffs372 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tariffs_372';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('country, cost_call_in, cost_call_out, cost_sms_out', 'required'),
			array('cost_call_in, cost_call_out, cost_sms_out', 'numerical'),
			array('country, country_ru', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, country, country_ru, cost_call_in, cost_call_out, cost_sms_out', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'country' => 'Country',
			'country_ru' => 'Country Ru',
			'cost_call_in' => 'Cost Call In',
			'cost_call_out' => 'Cost Call Out',
			'cost_sms_out' => 'Cost Sms Out',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('country_ru',$this->country_ru,true);
		$criteria->compare('cost_call_in',$this->cost_call_in);
		$criteria->compare('cost_call_out',$this->cost_call_out);
		$criteria->compare('cost_sms_out',$this->cost_sms_out);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}