<?php

/**
 * This is the model class for table "certificates".
 *
 * The followings are the available columns in table 'certificates':
 * @property integer $id
 * @property string $type
 * @property string $certificate
 * @property double $discount_p
 * @property double $discount_v
 * @property string $use
 * @property integer $disabled
 *
 * The followings are the available model relations:
 * @property CertificatesCountries[] $certificatesCountries
 * @property CertificatesOrders[] $certificatesOrders
 * @property CertificatesTypes[] $certificatesTypes
 * @property CertificatesUsers[] $certificatesUsers
 * @property Orders[] $orders
 */
class Certificates extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certificates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('certificate', 'required'),
			array('disabled', 'numerical', 'integerOnly'=>true),
			array('discount_p, discount_v', 'numerical'),
			array('type, use', 'length', 'max'=>10),
			array('certificate', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, certificate, discount_p, discount_v, use, disabled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'certificatesCountries' => array(self::HAS_MANY, 'CertificatesCountries', 'certificate_id'),
			'certificatesOrders' => array(self::HAS_MANY, 'CertificatesOrders', 'certificate_id'),
			'rCertificatesTypes' => array(self::HAS_MANY, 'CertificatesTypes', 'certificate_id'),
			//'certificatesUsers' => array(self::HAS_MANY, 'CertificatesUsers', 'id_certificate'),
			'rCertificatesUsers' => array(self::HAS_MANY, 'CertificatesUsers', 'certificate_id'),
			'orders' => array(self::HAS_MANY, 'Orders', 'certificate_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'certificate' => 'Certificate',
			'discount_p' => 'Discount P',
			'discount_v' => 'Discount V',
			'use' => 'Use',
			'disabled' => 'Disabled',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('certificate',$this->certificate,true);
		$criteria->compare('discount_p',$this->discount_p);
		$criteria->compare('discount_v',$this->discount_v);
		$criteria->compare('use',$this->use,true);
		$criteria->compare('disabled',$this->disabled);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Certificates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getDiscount($certificate = NULL) {
		$result	= NULL;
		$model	= self::model()->findByAttributes(array('certificate'=>$certificate));
		if ($model) {
			if ($model->discount_p)
				$result = (string)($model->discount_p * 100) . '%';
			if ($model->discount_v)
				$result = (string)($model->discount_v) . '$';
		}
		
		return $result;
	}
}
