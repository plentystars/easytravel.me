<?php

/**
 * This is the model class for table "certificates_countries".
 *
 * The followings are the available columns in table 'certificates_countries':
 * @property integer $id
 * @property integer $certificate_id
 * @property string $country
 * @property integer $order_type_id
 * @property string $model
 *
 * The followings are the available model relations:
 * @property OrderTypes $orderType
 * @property Certificates $certificate
 */
class CertificatesCountries extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certificates_countries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('country, model', 'required'),
			array('certificate_id, order_type_id', 'numerical', 'integerOnly'=>true),
			array('country', 'length', 'max'=>255),
			array('model', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, certificate_id, country, order_type_id, model', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orderType' => array(self::BELONGS_TO, 'OrderTypes', 'order_type_id'),
			'certificate' => array(self::BELONGS_TO, 'Certificates', 'certificate_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'certificate_id' => 'Certificate',
			'country' => 'Country',
			'order_type_id' => 'Order Type',
			'model' => 'Model',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('certificate_id',$this->certificate_id);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('order_type_id',$this->order_type_id);
		$criteria->compare('model',$this->model,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CertificatesCountries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
