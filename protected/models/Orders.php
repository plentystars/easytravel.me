<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property integer $user_id
 * @property integer $type_id
 * @property integer $delivery_id
 * @property integer $payment_system_id
 * @property string $payment_status
 * @property double $cost
 * @property integer $certificate_id
 * @property string $certificate
 * @property string $datetime
 *
 * The followings are the available model relations:
 * @property OrderType1[] $orderType1s
 * @property OrderType2[] $orderType2s
 * @property OrderType3[] $orderType3s
 * @property OrderTypes $type
 * @property Users $user
 * @property Delivery $delivery
 * @property Certificates $certificate0
 * @property PaymentSystem $paymentSystem
 */
class Orders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, type_id, delivery_id, payment_system_id, certificate_id', 'numerical', 'integerOnly'=>true),
			array('cost', 'numerical'),
			array('payment_status, certificate', 'length', 'max'=>100),
			array('datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, type_id, delivery_id, payment_system_id, payment_status, cost, certificate_id, certificate, datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rOrderType1'		=> array(self::HAS_ONE, 'OrderType1', 'order_id'),
			'rOrderType2'		=> array(self::HAS_ONE, 'OrderType2', 'order_id'),
			'rOrderType3'		=> array(self::HAS_ONE, 'OrderType3', 'order_id'),
			'rOrderType4'		=> array(self::HAS_ONE, 'OrderType4', 'order_id'),
			'rOrderType5'		=> array(self::HAS_ONE, 'OrderType5', 'order_id'),
			'rType'				=> array(self::BELONGS_TO, 'OrderTypes', 'type_id'),
			'rDelivery'			=> array(self::BELONGS_TO, 'Delivery', 'delivery_id'),
			'rUsers'			=> array(self::BELONGS_TO, 'Users', 'user_id'),
			'rPaymentSystem'	=> array(self::BELONGS_TO, 'PaymentSystem', 'payment_system_id'),
			'rCertificate'		=> array(self::BELONGS_TO, 'Certificates', 'certificate_id'),
		);
	}
	
	//возвращает название платежной системы
	public function getPaymentSystemName() {
		$result = '';
		$model = PaymentSystem::model()->findByPk($this->payment_system_id);
		if($model)
			$result = $model->name;
			
		return $result;
	}
  
  public function getOrderAll() {
    $obj = array();
    $obj['main']        = $this->attributes;
    $obj['description'] = $this->{'rOrderType' . $this->type_id} ? $this->{'rOrderType' . $this->type_id}->attributes : NULL;
    $obj['delivery']    = $this->rDelivery ? $this->rDelivery->attributes : NULL;

    return $obj;
  }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'type_id' => 'Type',
			'delivery_id' => 'Delivery',
			'payment_system_id' => 'Payment System',
			'payment_status' => 'Payment Status',
			'cost' => 'Cost',
			'certificate_id' => 'Certificate',
			'certificate' => 'Certificate',
			'datetime' => 'Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('type_id',$this->type_id);
		$criteria->compare('delivery_id',$this->delivery_id);
		$criteria->compare('payment_system_id',$this->payment_system_id);
		$criteria->compare('payment_status',$this->payment_status,true);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('certificate_id',$this->certificate_id);
		$criteria->compare('certificate',$this->certificate,true);
		$criteria->compare('datetime',$this->datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
