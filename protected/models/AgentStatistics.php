<?php

/**
 * This is the model class for table "agent_statistics".
 *
 * The followings are the available columns in table 'agent_statistics':
 * @property integer $id
 * @property integer $certificate_id
 * @property string $user_agent
 * @property string $ip_address
 * @property string $referer
 * @property string $datetime
 *
 * The followings are the available model relations:
 * @property Certificates $certificate
 */
class AgentStatistics extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AgentStatistics the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agent_statistics';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('certificate_id', 'numerical', 'integerOnly'=>true),
			array('user_agent, referer', 'length', 'max'=>255),
			array('ip_address', 'length', 'max'=>30),
			array('datetime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, certificate_id, user_agent, ip_address, referer, datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'certificate' => array(self::BELONGS_TO, 'Certificates', 'certificate_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'certificate_id' => 'Certificate',
			'user_agent' => 'User Agent',
			'ip_address' => 'Ip Address',
			'referer' => 'Referer',
			'datetime' => 'Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('certificate_id',$this->certificate_id);
		$criteria->compare('user_agent',$this->user_agent,true);
		$criteria->compare('ip_address',$this->ip_address,true);
		$criteria->compare('referer',$this->referer,true);
		$criteria->compare('datetime',$this->datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}