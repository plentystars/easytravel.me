<?php

/**
 * This is the model class for table "countries".
 *
 * The followings are the available columns in table 'countries':
 * @property integer $id
 * @property string $name_ru
 * @property string $name_eng
 * @property integer $weight
 * @property string $type
 * @property integer $zone_1
 * @property integer $zone_2
 * @property integer $zone_3
 * @property integer $zone_4
 * @property integer $zone_5
 * @property integer $group_1
 * @property integer $group_2
 * @property integer $group_3
 * @property integer $group_4
 * @property integer $group_5
 * @property integer $group_6
 * @property integer $disabled
 */
class Countries extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Countries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'countries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_ru, name_eng, type', 'required'),
			array('weight, zone_1, zone_2, zone_3, zone_4, zone_5, group_1, group_2, group_3, group_4, group_5, group_6, disabled', 'numerical', 'integerOnly'=>true),
			array('name_ru, name_eng', 'length', 'max'=>150),
			array('type', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name_ru, name_eng, weight, type, zone_1, zone_2, zone_3, zone_4, zone_5, group_1, group_2, group_3, group_4, group_5, group_6, disabled', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_ru' => 'Name Ru',
			'name_eng' => 'Name Eng',
			'weight' => 'Weight',
			'type' => 'Type',
			'zone_1' => 'Zone 1',
			'zone_2' => 'Zone 2',
			'zone_3' => 'Zone 3',
			'zone_4' => 'Zone 4',
			'zone_5' => 'Zone 5',
			'group_1' => 'Group 1',
			'group_2' => 'Group 2',
			'group_3' => 'Group 3',
			'group_4' => 'Group 4',
			'group_5' => 'Group 5',
			'group_6' => 'Group 6',
			'disabled' => 'Disabled',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('name_eng',$this->name_eng,true);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('zone_1',$this->zone_1);
		$criteria->compare('zone_2',$this->zone_2);
		$criteria->compare('zone_3',$this->zone_3);
		$criteria->compare('zone_4',$this->zone_4);
		$criteria->compare('zone_5',$this->zone_5);
		$criteria->compare('group_1',$this->group_1);
		$criteria->compare('group_2',$this->group_2);
		$criteria->compare('group_3',$this->group_3);
		$criteria->compare('group_4',$this->group_4);
		$criteria->compare('group_5',$this->group_5);
		$criteria->compare('group_6',$this->group_6);
		$criteria->compare('disabled',$this->disabled);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}