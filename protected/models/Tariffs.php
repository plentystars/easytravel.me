<?php

/**
 * This is the model class for table "tariffs".
 *
 * The followings are the available columns in table 'tariffs':
 * @property integer $id
 * @property integer $parent_tariff_id
 * @property integer $tariff_id
 * @property integer $order_type_id
 * @property string $name
 * @property string $settings
 */
class Tariffs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tariffs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tariff_id, order_type_id, name', 'required'),
			array('parent_tariff_id, tariff_id, order_type_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('settings', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_tariff_id, tariff_id, order_type_id, name, settings', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_tariff_id' => 'Parent Tariff',
			'tariff_id' => 'Tariff',
			'order_type_id' => 'Order Type',
			'name' => 'Name',
			'settings' => 'Settings',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_tariff_id',$this->parent_tariff_id);
		$criteria->compare('tariff_id',$this->tariff_id);
		$criteria->compare('order_type_id',$this->order_type_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('settings',$this->settings,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tariffs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getQuantityMb() {
		var_dump($this);
	}
}
