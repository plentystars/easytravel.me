<?php

/**
 * This is the model class for table "certificates_types".
 *
 * The followings are the available columns in table 'certificates_types':
 * @property integer $id
 * @property integer $certificate_id
 * @property integer $order_type_id
 * @property string $custom_type
 *
 * The followings are the available model relations:
 * @property CertificatesParams[] $certificatesParams
 * @property OrderTypes $orderType
 * @property Certificates $certificate
 */
class CertificatesTypes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certificates_types';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('certificate_id, order_type_id', 'numerical', 'integerOnly'=>true),
			array('custom_type', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, certificate_id, order_type_id, custom_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'certificatesParams' => array(self::HAS_MANY, 'CertificatesParams', 'certificate_type_id'),
			'orderType' => array(self::BELONGS_TO, 'OrderTypes', 'order_type_id'),
			'certificate' => array(self::BELONGS_TO, 'Certificates', 'certificate_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'certificate_id' => 'Certificate',
			'order_type_id' => 'Order Type',
			'custom_type' => 'Custom Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('certificate_id',$this->certificate_id);
		$criteria->compare('order_type_id',$this->order_type_id);
		$criteria->compare('custom_type',$this->custom_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CertificatesTypes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
