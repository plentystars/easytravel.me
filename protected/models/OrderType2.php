<?php

/**
 * This is the model class for table "order_type_2".
 *
 * The followings are the available columns in table 'order_type_2':
 * @property integer $id
 * @property integer $order_id
 * @property string $countries
 * @property integer $tariff_sim_id
 * @property integer $equipment_id
 * @property integer $type_sim_id
 * @property integer $balans_id
 * @property double $add_balans
 * @property integer $bonus
 *
 * The followings are the available model relations:
 * @property Orders $order
 */
class OrderType2 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_type_2';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tariff_sim_id, type_sim_id, balans_id', 'required'),
			array('order_id, tariff_sim_id, type_sim_id, equipment_id, balans_id, bonus', 'numerical', 'integerOnly'=>true),
			array('add_balans', 'numerical'),
			array('countries', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, order_id, countries, tariff_sim_id, type_sim_id, equipment_id, balans_id, add_balans, bonus', 'safe', 'on'=>'search'),
		);
	}
	
	public function beforeValidate(){
		if(is_array($this->countries))
			$this->countries = implode($this->countries);

        return parent::beforeValidate();
    }
	
	//получить название добавленного баланса
	public function getBalansName() {
		return $this->convertToName('order_type_2.balans_id', $this->balans_id);
	}
	
	//получить название типа sim-карты
	public function getTypeSimName() {
		return $this->convertToName('order_type_1.type_sim_id', $this->type_sim_id);
	}
	
	//получить название sim-карты
	public function getTariffSimName() {
		return $this->convertToName('order_type_2.tariff_sim_id', $this->tariff_sim_id);
	}
	
	//TODO: вынести в поведение
	private function convertToName($key = NULL, $value = NULL) {
		$result = '';
		$mKeyValue = KeyValue::model()->findByAttributes(
			array(
				'key'	=> $key,
				'value'	=> $value
			)
		);
		
		if($mKeyValue)
			$result = $mKeyValue->name;
		
		return $result;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rOrders' => array(self::HAS_ONE, 'Orders', 'order_id'),
			'rEquipments'	=> array(self::HAS_ONE, 'Equipments', 'equipment_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'Order',
			'countries' => 'Countries',
			'tariff_sim_id' => 'Tariff Sim',
			'equipment_id' => 'Equipment',
			'type_sim_id' => 'Type Sim',
			'balans_id' => 'Balans',
			'add_balans' => 'Add Balans',
			'bonus' => 'Bonus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('countries',$this->countries,true);
		$criteria->compare('tariff_sim_id',$this->tariff_sim_id);
		$criteria->compare('type_sim_id',$this->type_sim_id);
		$criteria->compare('equipment_id',$this->equipment_id);
		$criteria->compare('balans_id',$this->balans_id);
		$criteria->compare('add_balans',$this->add_balans);
		$criteria->compare('bonus',$this->bonus);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderType2 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
