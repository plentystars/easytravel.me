<?php

/**
 * This is the model class for table "order_type_1".
 *
 * The followings are the available columns in table 'order_type_1':
 * @property integer $id
 * @property integer $order_id
 * @property string $countries
 * @property string $days
 * @property string $date_activation
 * @property integer $tariff_id
 * @property integer $type_sim_id
 * @property integer $equipment_id
 *
 * The followings are the available model relations:
 * @property Orders $order
 */
class OrderType1 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order_type_1';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('countries, days, date_activation, tariff_id, type_sim_id', 'required'),
			array('order_id, tariff_id, type_sim_id, equipment_id', 'numerical', 'integerOnly'=>true),
			array('countries', 'length', 'max'=>255),
			array('days, date_activation', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, order_id, countries, days, date_activation, tariff_id, type_sim_id, equipment_id', 'safe', 'on'=>'search'),
		);
	}
	
	//перед валидацией страны преобразовуются к строке, т. к. в POST передаются массивом
	public function beforeValidate(){
		if(is_array($this->countries))
			$this->countries = implode($this->countries);

        return parent::beforeValidate();
    }
	
	//возвращает страны в текстовом виде, т. к. в базе сохранены в виде: 1,2,3
	public function getCountriesName() {
		return self::convertCountries($this->countries);
	}
	
	public function getTariffName() {
		$result		= '';
		$mTariffs	= Tariffs::model()->findByAttributes(array('order_type_id'=>1, 'tariff_id'=>$this->tariff_id));
		if ($mTariffs) {
			$settings	= CJSON::decode($mTariffs->settings, false);
			$result		= $settings->display_name;
		}
		
		return $result;
		//return $this->convertToName('order_type_1.tariff_id', $this->tariff_id);
	}
	
	//public static function getTypeSimName()
	
	public function getTypeSimName() {
		return $this->convertToName('order_type_1.type_sim_id', $this->type_sim_id);
	}
	
	public function getEquipmentName() {
		$result = '';
		$model = Equipments::model()->findByPk($this->equipment_id);
		if($model)
			$result = $model->name;
		
		return $result;
	}
	
	//TODO: вынести в поведение
	private function convertToName($key = NULL, $value = NULL) {
		$result = '';
		$mKeyValue = KeyValue::model()->findByAttributes(
			array(
				'key'	=> $key,
				'value'	=> $value
			)
		);
		
		if($mKeyValue)
			$result = $mKeyValue->name;
		
		return $result;
	}
	
	//конвертирует страны из вида array(1,2,3) или '1,2,3' в 'Аргентина, Ямайка'
	public static function convertCountries($countries = array()) {
		$result = array();
		if(!is_array($countries))
			$countries = explode(',', $countries);
			
		$items	= Countries::model()->findAllByPk($countries);

		if($items)
			foreach($items as $item)
				$result[] = $item->name_ru;
		
		return implode(', ', $result);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rOrders'		=> array(self::HAS_ONE, 'Orders', 'order_id'),
			'rEquipments'	=> array(self::HAS_ONE, 'Equipments', 'equipment_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'Order',
			'countries' => 'Countries',
			'days' => 'Days',
			'date_activation' => 'Date Activation',
			'tariff_id' => 'Tariff',
			'type_sim_id' => 'Type Sim',
			'equipment_id' => 'Equipment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('countries',$this->countries,true);
		$criteria->compare('days',$this->days,true);
		$criteria->compare('date_activation',$this->date_activation,true);
		$criteria->compare('tariff_id',$this->tariff_id);
		$criteria->compare('type_sim_id',$this->type_sim_id);
		$criteria->compare('equipment_id',$this->equipment_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderType1 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
