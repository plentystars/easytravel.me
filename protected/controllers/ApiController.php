<?php

class ApiController extends Controller {

  protected function toXML($array) {
    
    //function defination to convert array to xml
    function array_to_xml($array, &$xml_user_info) {
      foreach($array as $key => $value) {
          if(is_array($value)) {
              if(!is_numeric($key)){
                  $subnode = $xml_user_info->addChild("$key");
                  array_to_xml($value, $subnode);
              }else{
                  //$subnode = $xml_user_info->addChild("item$key");
                  $subnode = $xml_user_info->addChild("order");
                  array_to_xml($value, $subnode);
              }
          }else {
              $xml_user_info->addChild("$key",htmlspecialchars("$value"));
          }
      }
    }

    //creating object of SimpleXMLElement
    $xml_user_info = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><orders></orders>");

    //function call to convert array to xml
    array_to_xml($array,$xml_user_info);

    return $xml_user_info->asXML();
  }

	public function actionOrders() {
    $params = new stdClass;
    
    $params->start_date = Yii::app()->request->getParam('start_date');
    $params->end_date   = Yii::app()->request->getParam('end_date');
    $params->type       = Yii::app()->request->getParam('type');
    $params->password   = Yii::app()->request->getParam('password');
    
    $results = array();
    if ($params->password == 'jsbjlbyc') {
    
      $criteria = new CDbCriteria;
      $criteria->addBetweenCondition('datetime', $params->start_date, $params->end_date);
      $msOrders = Orders::model()->findAll($criteria);
      
      if ($msOrders) {
        foreach($msOrders as $mOrders) {
          $results[] = $mOrders->getOrderAll();
        }
      }
    }
    
    header("Content-Type: text/xml");
    echo $this->toXML($results);
	}
  
  /*public function actionOrderTypes() {
    var_dump(11111);
  }*/
}