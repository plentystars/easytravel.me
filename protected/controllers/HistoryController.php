<?php

class HistoryController extends Controller {

	public function actionIndex() {
		if(Yii::app()->user->isGuest)
			$userRoles = array('Guest');
		else {
			$userRoles = Rights::getAssignedRoles(Yii::app()->user->Id);
			$userRoles = array_keys($userRoles);
		}
		//var_dump($userRoles); die();
		if(array_search('Admin', $userRoles) !== false) {
			$this->render('index');
		} elseif(array_search('Agent', $userRoles) !== false) {
			$items = array();
			$modelsCertificatesAgents = CertificatesUsers::model()->findAllByAttributes(array('id_user'=>Yii::app()->user->id, 'type'=>'agent'));

			if($modelsCertificatesAgents)
				foreach($modelsCertificatesAgents as $modelCertificatesAgents) {
					$status = CertificatesUsers::model()->findByAttributes(array('id_certificate'=>$modelCertificatesAgents->id_certificate, 'type'=>'user')) ? 1 : 0;
					$items[] = array(
						'certificate'=>
							$modelCertificatesAgents->rCertificates->c_series.
							' '.
							$modelCertificatesAgents->rCertificates->c_number,
						'status'=>$status);
				}
			$this->render('Agent', array('items'=>$items));
		} elseif(array_search('Authenticated', $userRoles) !== false) {
			$orders = Orders::model()->findAllByAttributes(array('user_id'=>Yii::app()->user->id), array('order'=>'datetime DESC'));
			$this->render('index', array('orders'=>$orders));
		} elseif(array_search('Guest', $userRoles) !== false) {
			//$this->render('index');
		}
	}
}
