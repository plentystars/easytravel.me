<?php

class SiteController extends RController {

	/*public function actions() {
		return array(
			'page'	=> array(
				'class'	=> 'CViewAction',
			),
		);
	}*/
	
	public function filters() {
		return array(
			'rights',
		);
	}
	
	public function actionLanguage($id = NULL) {
		Languages::setLanguage($id);
		$this->redirect(Yii::app()->request->urlReferrer);
	}
	
	public function actionLogin() {
		$registrationForm	= new RegistrationForm;
		$profile			= new Profile;
		$profile->regMode	= true;
		
		$userLogin = new UserLogin;
		
		if ($reqUserLogin = Yii::app()->request->getPost('UserLogin')) {
			echo UActiveForm::validate(array($userLogin));
			Yii::app()->end();
		}

		echo $this->getModalActionContent('loginform', 
			array(
				'model_registration'	=> $registrationForm,
				'profile'				=> $profile,
				'userLogin'				=> $userLogin
			)
		);
	}
	
	public function actionDelivery() {
		$model = new Delivery;
		
		if ($reqDelivery = Yii::app()->request->getPost('Delivery')) {
		
			$validate = CActiveForm::validate(array($model));
			if ($validate == '[]')
				$this->setSessionOrder(NULL, NULL, $reqDelivery);
			
			
			echo $validate;
				
			Yii::app()->end();
		}
		
		$sOrders	= Yii::app()->session->get('Orders');
		$sOrderType	= Yii::app()->session->get('OrderType');
		
		if ($sOrders && $sOrderType) {
			
			switch($sOrders['type_id']) {
				case 1:
					$cost				= $this->getCost($sOrders, $sOrderType);
					$object				= new OrderType1;
					$object->attributes	= $sOrderType;
					
					$order = array(
						'type_sim'			=> $object->getTypeSimName(),
						'tariff'			=> $object->getTariffName(),
						'countries'			=> $object->getCountriesName(),
						'date_activation'	=> $object->date_activation,
						'equipment'			=> $object->getEquipmentName(),
						'certificate'		=> Certificates::getDiscount($sOrders['certificate']),
						'cost_usd'			=> $cost->main,
						'cost_rub'			=> $cost->main * $this->getRate()
					);
					
					$htmlOrderInfo = $this->renderPartial('//modals/actions/order_info_type1', array('order'=>$order), true);
				break;
				
				case 2:
					$orders = array();
					$cost	= $this->getCostType2($sOrders, $sOrderType);
					
					foreach($sOrderType as $item) {
						
						$object				= new OrderType2;
						$object->attributes	= $item;
						
						$order = array(
							'type_sim'			=> $object->getTypeSimName(),
							'start_balans'		=> $object->getBalansName(),
							'added_balans'		=> $item['add_balans'],
						);
						
						$orders['items'][] = $order;
					}
					
					$orders['certificate']	= Certificates::getDiscount($sOrders['certificate']);
					$orders['cost_usd']		= $cost->main;
					$orders['cost_rub']		= $cost->main * $this->getRate();
					
					$htmlOrderInfo = $this->renderPartial('//modals/actions/order_info_type2', array('orders'=>$orders), true);
				break;
				
				default:
					Yii::app()->end();
				break;
			}
			
			echo $this->getModalActionContent('delivery', 
						array(
							'model'			=> $model,
							'htmlOrderInfo'	=> $htmlOrderInfo
						)
					);
		}
	}
	
	// Выбор метода оплаты
	public function actionChoosePayment() {
		$sOrders		= array_merge(Yii::app()->session['Orders'], array('cost'=>$this->getCost()->main));
		$sOrderType		= Yii::app()->session['OrderType'];
		$sDelivery		= Yii::app()->session['Delivery'];

		if ($sOrders && $sOrderType) {

			// проверяем существует ли для этого типа заказа доставка
			$mOrderTypes = OrderTypes::model()->findByPk($sOrders['type_id']);
			if ($mOrderTypes) {
				$settings = CJSON::decode($mOrderTypes->settings, false);
				if (!$settings->delivery) $sDelivery = NULL;
			}
			//
		
			$order_id = $this->saveOrder($sOrders, $sOrderType, $sDelivery);
			if ($order_id) {
				echo $this->getModalActionContent('choosepayments', array('order_id'=>$order_id));
			}
		}
	}
	
	public function actionEquipmentInfo() {
		$id = Yii::app()->request->getPost('id');
		$model = Equipments::model()->findByPk($id);
		if ($model)
			echo $this->getModalActionContent('equipment_more_info', array('model'=>$model));
	}
	
	
	public function actionDeleteSession() {
		//var_dump(Yii::app()->session->destroy());
	}
	
	public function actionGetSessionOrder() {
		$json			= new stdClass;
		$json->success	= false;
		
		if ($sOrders = Yii::app()->session->get('Orders'))
			$json->orders = $sOrders;
		
		if ($sOrderType = Yii::app()->session->get('OrderType'))
			$json->order_type = $sOrderType;
			
		if ($sDelivery = Yii::app()->session->get('Delivery'))
			$json->delivery = $sDelivery;
			
		if ($sOrders || $sOrderType)
			$json->success = true;
		
		echo CJSON::encode($json);
	}
	
	public function actionGetTariff371Info() {
		$json			= new stdClass;
		$json->success	= false;
		$reqOrderType	= Yii::app()->request->getPost('OrderType');
		
		if ($reqOrderType && isset($reqOrderType['countries'])) {
			$mTariffs371 = Tariffs371::model()->findByPk($reqOrderType['countries']);
			
			if ($mTariffs371) {
				$json->success	= true;
				$json->items	= $mTariffs371->attributes;
			}
		}
		
		echo CJSON::encode($json);
	}
	
	public function actionGetEquipment() {
		$json			= new stdClass;
		$json->success	= false;
		
		$sOrderType = Yii::app()->session->get('OrderType');
		if ($sOrderType && isset($sOrderType['equipment_id'])) {
			$mEquipments = Equipments::model()->findByPk($sOrderType['equipment_id']);
			if ($mEquipments) {
				$json->success	= true;
				$json->id		= $mEquipments->id;
				$json->name		= $mEquipments->name;
			}
		}

		echo CJSON::encode($json);		
	}
	
	public function actionDeleteEquipment() {
		$json			= new stdClass;
		$json->success	= false;
		
		$id = Yii::app()->request->getPost('id');
		
		$sOrderType = Yii::app()->session->get('OrderType');
		if ($sOrderType && $sOrderType['equipment_id'] && ($sOrderType['equipment_id'] == $id)) {
			$json->success = true;
			unset($_SESSION['OrderType']['equipment_id']);
		}
		
		echo CJSON::encode($json);
	}
	
	public function actionCheckSessionOrder() {
		//unset($_SESSION)
		$json				= new stdClass;
		$json->success		= false;
		$json->order_type	= NULL;
		$sOrders			= Yii::app()->session['Orders'];

		if ($sOrders) $json->order_type = $sOrders['type_id'];

		echo CJSON::encode($json);
	}
	
	private function getModalPageContent($view, $params = array()) {
		Yii::app()->clientScript->scriptMap = array('jquery.js'=>false);
		$json			= new stdClass;
		$json->success	= true;
		$json->content	= $this->renderPartial('//modals/pages/' . $view, $params, true);
		return CJSON::encode($json);
	}
	
	private function getModalActionContent($view, $params = array()) {
		Yii::app()->clientScript->scriptMap = array('jquery.js'=>false);
		$json			= new stdClass;
		$json->success	= true;
		$json->content	= $this->renderPartial('//modals/actions/' . $view, $params, true, true);
		return CJSON::encode($json);
	}
	
	public function actionGetModalPage() {
		$page = Yii::app()->request->getPost('page');
		echo $this->getModalPageContent($page);
	}
	
	
	private function convertPOSTArray($data) {
		
		function calculateCount($array) {
			return count($array);
		}
		
		$max = max(array_map('calculateCount', $data));

		$items = array();
		for($i=0; $i < $max; $i++)
			foreach($data as $key => $value)
				$items[$i][$key] = isset($value[$i]) ? $value[$i] : NULL;
					
		return $items;
	}
	
	public function actionIndex() {
		$baseUrl = Yii::app()->request->baseUrl;
		Yii::app()->clientScript->scriptMap = array('jquery.js'=>false);

		Yii::app()->clientScript->registerScript(
			'app_settings',
			'
			var returnHash = "' . Yii::app()->session->get('returnHash') . '";
			var app_settings = {
				language: "' . Yii::app()->language . '",
				url: {
					addorder: "' . Yii::app()->createUrl('AddOrder') . '",
					addequipment: "' . Yii::app()->createUrl('AddEquipment') . '",
					deleteequipment: "' . Yii::app()->createUrl('DeleteEquipment') . '",
					getmodalpage: "' . Yii::app()->createUrl('GetModalPage') . '",
					getcost: "' . Yii::app()->createUrl('GetCost') . '",
					getsessionorder: "' . Yii::app()->createUrl('GetSessionOrder') . '",
					getequipment: "' . Yii::app()->createUrl('GetEquipment') . '",
					gettariff371info: "' . Yii::app()->createUrl('GetTariff371Info') . '",
					getcost: "' . Yii::app()->createUrl('GetCost') . '",
					gettariffs: "' . Yii::app()->createUrl('GetTariffs') . '",
					getformpayment: "' . Yii::app()->createUrl('GetFormPayment') . '",
					getvoiptariffs: "' . Yii::app()->createUrl('GetVoipTariffs') . '"
				}
			};',
			CClientScript::POS_HEAD
		);

		$this->layout = 'column1';

		$this->render('index', array(
			'baseUrl'		=> $baseUrl,
			'order_type_1'	=>	array(
				'countries'			=> Countries::model()->findAllByAttributes(array('disabled'=>0)),
				'date_activation'	=> $this->getDateOffset(date('d-m-Y'), 7),
				'tariffs'			=> Tariffs::model()->findAllByAttributes(array('order_type_id'=>1, 'parent_tariff_id'=>NULL)),
				'type_sim'			=> KeyValue::model()->findAllByAttributes(array('key' => 'order_type_1.type_sim_id'))
			),
			'order_type_2'	=>	array(
				'countries'		=> Tariffs371::model()->findAll(array('order'=>'country ASC')),
				'default_cost'	=> $this->getAdditionalCost(2),
				'type_sim'		=> KeyValue::model()->findAllByAttributes(array('key' => 'order_type_1.type_sim_id'))
			),
			'order_type_5'	=>	array(
				'items'		=> KeyValue::model()->findAllByAttributes(array('key' => 'order_type_5.tariff_id', 'disabled'=>0))
			),
			'equipment'		=>	array(
				'items'		=> Equipments::model()->findAll()
			),
			'faq'			=>	array(
				'items'		=> Faq::model()->findAll()
			)
		));
        //SendMail::send('andpash@gmail.com', 'test', 'test');
		//$this->renderRoaming3G();
	}

	// Интернет в роуминге
	/*public function actionRoaming3G() {
		$this->renderRoaming3G('roaming3g');
	}*/
	
	/*private function renderRoaming3G($view = 'index') {
		$this->render($view,
			array(
				'countries'				=> Countries::model()->findAllByAttributes(array('disabled'=>0)),
				'date_start_activation'	=> $this->getDateOffset(date('d-m-Y'), 7),
				'tariffs'				=> KeyValue::model()->findAllByAttributes(array('key'=>'order_type_1.tariff_id')),
				'types_sim'				=> KeyValue::model()->findAllByAttributes(array('key'=>'order_type_1.type_sim_id'))
			)
		);
	}*/
	
	// Sim-карты Gb2Go
	/*public function actionShopkeepgo() {
		$tariffs = KeyValue::model()->findAllByAttributes(
			array('key'	=> 'order_type_4.tariff_id')
		);
		$this->render('shopkeepgo', array('tariffs'=>$tariffs));
	}*/
	
	// Мобильная связь
	/*public function actionCalculatorPrepaid() {
		$connection	= Yii::app()->db;
		//$sql		= 'SELECT `country` `name` FROM (SELECT `country` FROM `tariffs_371` UNION SELECT `country` FROM `tariffs_372`) t GROUP BY `country` ASC';
		$sql		= 'SELECT `country` `name`, `country_ru` `name_ru` FROM `tariffs_371` ORDER BY `name` ASC';
		$command	= $connection->createCommand($sql);
		$countries	= $command->queryAll();
		
		$types_sim = KeyValue::model()->findAllByAttributes(array('key'=>'order_type_1.type_sim_id'));
		
		$this->render('calculator_prepaid',
			array(
				'countries'	=> $countries,
				'types_sim'	=> $types_sim
			)
		);
	}*/
	
	// Пополнить баланс
	/*public function actionAddCash371372() {
		Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/resources/jquery/jquery.maskedinput.min.js');
		$msKeyValue = KeyValue::model()->findAllByAttributes(
			array(
				'key'=>'order_type_5.tariff_id'
			)
		);
		$this->render('addcash371372',
			array('tariffs_gb2go'	=> $msKeyValue)
		);
	}*/
	/*public function actionAddCash371372() {
		
	}*/
	
	// Выбор оборудования
	/*public function actionChooseEquipment() {
		$sOrders	= Yii::app()->session['Orders'];
		$sOrderType	= Yii::app()->session['OrderType'];
		
		if(!self::issetOrderInSession())
			self::goHome();
		
		$html_values		= $this->getOrderHtmlValues($sOrders, $sOrderType);
		$selected_equipment	= isset($sOrderType['equipment_id']) ? $sOrderType['equipment_id'] : 0;
		$equipments			= Equipments::model()->findAll();

		$this->render('equipment',
			array(
				'html_values'			=> $html_values,
				'equipments'			=> $equipments,
				'selected_equipment'	=> $selected_equipment
			)
		);
    }*/
    
	// Доставка
    /*public function actionDelivery() {
		if(!self::issetOrderInSession())
			self::goHome();
			
		$sOrders		= Yii::app()->session['Orders'];
		$sOrderType		= Yii::app()->session['OrderType'];
		$sDelivery		= Yii::app()->session['Delivery'];

		//валидация формы доставки
		$postDelivery = Yii::app()->request->getPost('Delivery');
		
		if($postDelivery) {
			$modelDelivery				= new Delivery;
			$modelDelivery->attributes	= $postDelivery;
			$validate = CActiveForm::validate($modelDelivery);
			echo $validate;
			
			if($validate == '[]')
				Yii::app()->session['Delivery'] = $postDelivery;

			Yii::app()->end();
		}
		//
			
		$html_values	= $this->getOrderHtmlValues($sOrders, $sOrderType);
		
		$moduleUser		= Yii::app()->getModule('user');
		$modelUser		= $moduleUser->user(Yii::app()->user->id);

		$modelDelivery			= new Delivery;
		$modelDelivery->email	= $modelUser->email;

		if($sDelivery)
			$modelDelivery->attributes = $sDelivery;
		
		$this->render('delivery', array('html_values'=>$html_values, 'modelDelivery'=>$modelDelivery));
	}*/
	
	// Выбор метода оплаты
	/*public function actionChoosePayment() {
		if(!self::issetOrderInSession())
			self::goHome();
		
		$sOrders		= array_merge(Yii::app()->session['Orders'], array('cost'=>$this->getCost()->main));
		$sOrderType		= Yii::app()->session['OrderType'];
		$sDelivery		= Yii::app()->session['Delivery'];
		
		$html_values = $this->getOrderHtmlValues($sOrders, $sOrderType);
		if($sOrders && $sOrderType) {
			$order_id = $this->saveOrder($sOrders, $sOrderType, $sDelivery);
			if($order_id)
				$this->render('choose_payment', array('html_values'=>$html_values, 'order_id'=>$order_id));
		} else
			self::goHome();
	}*/
	
	
	/*public function actionSetStateOrder() {
		$json			= new stdClass;
		$json->success	= false;
		
		$rOrders	= Yii::app()->request->getPost('Orders');
		$rOrderType	= Yii::app()->request->getPost('OrderType');
		if ($rOrders && $rOrderType) {
			$this->setSessionOrder($rOrders, $rOrderType);
			$json->success = true;
		}
		
		echo CJSON::encode($json);
	}*/
	
	private function toCP1251($s) {
		return iconv('utf-8', 'cp1251', $s);
	}
	
	//возвращает курс рубля к доллару
	protected function getRate() {
		//return 49.5;
    $mKV = KeyValue::model()->findByAttributes(array('key'=>'rate', 'name'=>'USD'));
    if ($mKV)
      return $mKV->cost;
    else
      return 55;
	}
	
	public function actionDownloadsPrice() {
		$tariffs		= array('371');
		$filename		= 'tariffs_' . implode('_', $tariffs);
		$columns		= array(
			'A'=>array('title'=>'Страна', 'attribute'=>'country_ru'), 
			'B'=>array('title'=>'Входящие', 'attribute'=>'cost_call_in'), 
			'C'=>array('title'=>'Исходящие', 'attribute'=>'cost_call_out'), 
			'D'=>array('title'=>'СМС', 'attribute'=>'cost_sms_out')
		);
		$header_style	= array(
			'font'  => array(
				'bold'  => true
			)
		);
		
		Yii::import('ext.phpexcel.XPHPExcel');      
		$objPHPExcel = XPHPExcel::createPHPExcel();
		
		$objPHPExcel->createSheet(NULL, count($tariffs)-1);
		
		foreach($tariffs as $tariff_key => $tariff_value) {
		
			foreach($columns as $column_key => $column_value) {
				$objPHPExcel->setActiveSheetIndex($tariff_key)->setCellValue($column_key . 1, $column_value['title']);
				$objPHPExcel->getActiveSheet()->getStyle($column_key . 1)->applyFromArray($header_style);
				$objPHPExcel->getActiveSheet()->getColumnDimension($column_key)->setAutoSize(true);
			}
			
			$modelTariffsName = 'Tariffs' . $tariff_value;
			$modelsTariffs = $modelTariffsName::model()->findAll();
			if($modelsTariffs)
				foreach($modelsTariffs as $index => $modelTariff)
					foreach($columns as $column_key => $column_value)
						$objPHPExcel->setActiveSheetIndex($tariff_key)->setCellValue($column_key . ($index + 2), $modelTariff->{$column_value['attribute']});
			
			$objPHPExcel->getActiveSheet()->setTitle($tariff_value);
			$objPHPExcel->setActiveSheetIndex($tariff_key)->insertNewRowBefore(1, 1)->setCellValue('A1', 'Тарифы на связь в роуминге Easytravel.me');
		}
		
		// Redirect output to a client's web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename. '.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		Yii::app()->end();
	}
	
	public function actionGetYml() {
		Yii::import('application.extensions.yml.*');
		$yml = new yml;
		$yml->set_shop('easytravel.me', 'easytravel.me', 'http://easytravel.me');
		$yml->add_currency('RUR', 1, 0);
		$yml->add_category($this->toCP1251('Sim-карты Gb2Go'), 1);
		$item = array(
			'url'=>'http://easytravel.me/shopkeepgo',
			'price'=>$this->getRate() * $this->tariff_gb2go[1],
			'currencyId'=>'RUR',
			'categoryId'=>1,
			'name'=>$this->toCP1251('SIM-карта Gb2Go Europe'),
			'description'=>$this->toCP1251('36 стран Евросоюза, 1Гб трафика, Русскоязычная техподдержка, Выгодная тарификация - округление сессий до 1 Кб'),
		);
		$yml->add_offer(1, $item, true);
		
		$item = array(
			'url'=>'http://easytravel.me/shopkeepgo',
			'price'=>$this->getRate() * $this->tariff_gb2go[2],
			'currencyId'=>'RUR',
			'categoryId'=>1,
			'name'=>$this->toCP1251('SIM-карта Gb2Go Ukraine'),
			'description'=>$this->toCP1251('Вся Украина, 13Гб трафика, Русскоязычная техподдержка, Выгодная тарификация - округление сессий до 1 Кб'),
		);
		$yml->add_offer(2, $item, true);
		
		$item = array(
			'url'=>'http://easytravel.me/shopkeepgo',
			'price'=>$this->getRate() * $this->tariff_gb2go[3],
			'currencyId'=>'RUR',
			'categoryId'=>1,
			'name'=>$this->toCP1251('SIM-карта Gb2Go Europe+America'),
			'description'=>$this->toCP1251('Европа + Америка, 1Гб трафика, Русскоязычная техподдержка, Выгодная тарификация - округление сессий до 1 Кб '),
		);
		$yml->add_offer(3, $item, true);
		
		header("Content-Type: text/xml");
		echo $yml->get_xml();
	}
	
  public function actionCorporateClient() {
    $model = new MessagesCorpClient;

    if ($reqUserLogin = Yii::app()->request->getPost('MessagesCorpClient')) {
      $result = UActiveForm::validate(array($model));
      
      if ($result == '[]') {
        $model->save();
      }
      
			echo UActiveForm::validate(array($model));
			Yii::app()->end();
		}
    
		echo $this->getModalActionContent('corporate', array('model'=>$model));
  }
  
	public function actionVoip() {
		$models = Voip::model()->findAll();
		echo $this->getModalActionContent('voip', array('models'=>$models));
	}
	
	public function actionGetVoipTariffs() {
		$json			= new stdClass;
		$json->success	= false;
		
		$id = Yii::app()->request->getParam('id');
		
		$model = Voip::model()->findByPk($id);
		
		if ($model) {
			$criteria = new CDbCriteria();
			//$criteria->addSearchCondition('country_en', $model->country_en);
			$criteria->addCondition('country_en LIKE "' . $model->country_en . '%"');
			$models = Voip::model()->findAll($criteria);
			if ($models)
				$json->success	= true;
				$json->items	= $models;
		}
		
		echo CJSON::encode($json);
	}
	
	public function actionTariffsByCountry() {
		echo $this->getModalActionContent('tariffsbycountry');
	}
	
	public function actionCountriesList() {
		$json			= new stdClass;
		$json->success	= false;
		$json->items	= array();
		$type			= Yii::app()->request->getPost('type');

		if ($type) {
			switch($type) {
				case '1':
					$models = Tariffs371::model()->findAll();
				break;
				case '2':
					$models = Countries::model()->with('rSingle')->findAllByAttributes(array('disabled'=>0));
				break;
			}

			if ($models) {
				$json->success = true;

				foreach($models as $model) {
					switch($type) {
						case '1':
							$tariff = array(
								'call_in'	=> $model->cost_call_in,
								'call_out'	=> $model->cost_call_out,
								'sms'		=> $model->cost_sms_out,
								'voip'		=> NULL,
								'mb'		=> NULL
							);
						break;
						case '2':
							$tariff = array(
								'call_in'	=> NULL,
								'call_out'	=> NULL,
								'sms'		=> NULL,
								'voip'		=> NULL,
								'mb'		=> $model->rSingle['cost_tariff_3']/500
							);
						break;
					}
					$json->items[] = array('id'=>$model->id, 'text'=> $model->name, 'tariff'=>$tariff);
				}
			}
		}

		echo CJSON::encode($json);
	}

	public function actionError($id = NULL) {
		$this->render('error', array('error'=>$id));
	}

	private function saveOrder($orders = NULL, $orderType = NULL, $delivery = NULL) {
		$result = NULL;

		if($orders && $orderType) {

			$delivery_id = NULL;
			
			if($delivery) {
				$modelDelivery				= new Delivery;
				$modelDelivery->attributes	= $delivery;
				$modelDelivery->save();
				$delivery_id				= $modelDelivery->getPrimaryKey();
			}
				
			$modelOrders					= new Orders;
			$modelOrders->attributes		= $orders;
			$modelOrders->delivery_id		= $delivery_id;
			$modelOrders->user_id			= Yii::app()->user->id;

			if($modelOrders->save()) {
				$order_id		= $modelOrders->getPrimaryKey();

				$orderTypeArray	= array();
				
				if (!isset($orderType[0])) $orderTypeArray[] = $orderType;
				else $orderTypeArray = $orderType;

				foreach($orderTypeArray as $orderType) {
					//certificate
					if($mCert = Certificate::getValid(array_merge($orders, $orderType))) {
						$modelOrders->certificate_id = $mCert->id;
						Certificate::setRelations($mCert);
					}
					//
										
					$modelOrderType	= 'OrderType' . $modelOrders->type_id;
					$modelOrderType	= new $modelOrderType;
			
					if(isset($orderType['countries']) && is_array($orderType['countries']))
						$orderType['countries']	= implode(',', $orderType['countries']);
			
					$modelOrderType->attributes	= $orderType;
					$modelOrderType->order_id	= $order_id;
					$modelOrderType->save();
				}
				
				/*Notification::send(NULL, 'order', array(
					'mOrders'		=> Orders::model()->findByPk($modelOrders->id),
					'mOrderType'	=> $modelOrderType,
					'mDelivery'		=> $delivery_id ? $modelDelivery : NULL
				));*/

				Yii::app()->session['Orders'] = array_merge(Yii::app()->session['Orders'], array('id'=>$order_id));
				
				$result = $order_id;
			}
		}

		return $result;
	}
	
	/*private function orderNotification($subject, $content, $emails = NULL) {
		$count = count($emails);
		if($emails === NULL)
			$emails = Yii::app()->params['nEmails'];
		
		foreach($emails as $email) {
			if(SendMail::send($email, $subject, $content) === NULL) $count--;
			else $count++;
		}
		
		return $count == 0 ? true : false;
	}*/
	
	public function actionGetFormPayment($id = NULL) {
		$sOrders		= Yii::app()->session['Orders'];

		if (!($sOrders || $sOrders['id'] || $sOrders['type_id'])) {
			$json = new stdClass;
			$json->success = false;
			echo CJSON::encode($json);
			Yii::app()->end();
		}

		if ($mOrders = Orders::model()->findByPk($sOrders['id'])) {
			$mOrders->payment_system_id = $id;
			$mOrders->save();
		}

		$mOrderType = 'OrderType' . $sOrders['type_id'];
		$mOrderType = $mOrderType::model()->findByAttributes(
			array(
				'order_id'	=> $sOrders['id']
			)
		);

		switch($sOrders['type_id']) {
			case 1:
				$description = 'Заказ ' . $mOrderType->countriesName;
			break;
			case 2:
				$description = 'Заказ ' . $mOrderType->countries;
			break;
			case 3:
				$description = 'Пополнение счета ' . $mOrderType->fullNumber;
			break;
			case 4:
				$description = 'Заказ ' . $mOrderType->tariffName;
			break;
			case 5:
				$description = 'Пополнение ICCID ' . $mOrderType->iccid;
			break;
		}

		Yii::import('ext.payments.*');
		$payment = new Payments();

		$payment->order_params = array(
			'order_id'		=> $sOrders['id'],
			'description'	=> $description,
			'cost'			=> ceil($mOrders->cost * $this->getRate()),
			'currency'		=> 'RUR'
		);
		$payment->getForm($id);
	}
	
	public function actionPayment($id = NULL) {
		$request	= $_REQUEST;
		$result		= false;
		$firstQuery	= false;

		if($request && $id) {
			$mLogs			= new Logs;
			$mLogs->content	= CJSON::encode($request);
			$mLogs->save();
			
			Yii::import('ext.payments.*');
			if($order_id = Payments::getResponse($id, $request)) {
				$mOrders = Orders::model()->findByPk($order_id);
				if($mOrders) {

					if($mOrders->payment_status != 1) $firstQuery = true;

					$mOrders->payment_status = 1;
					if($mOrders->save()) {
					
						// если сертификат одноразовый, то делаем его disabled
						$mCertificates = $mOrders->rCertificate;
						if($mCertificates && $mCertificates->use == 'one') {
							$mCertificates->disabled = 1;
							$mCertificates->update();
						}
						//

						//send notification, if query (from payment service) is first
						if($firstQuery) {
							$params = array(
								'mOrders'		=> $mOrders,
								'mOrderType'	=> $mOrders->{'rOrderType' . $mOrders->type_id},
								'mDelivery'		=> $mOrders->rDelivery
							);
							Notification::send(NULL, 'order', $params);
						}
						//
						$result = true;
					}
				}
			}
		}
		
		echo (int)$result;
	}
	
    private function getCommonZones($countries = NULL) {
        $same   = true;
        $result = NULL;
        $count  = count($countries);
        
        if($mCountries = Countries::model()->findAllByPk($countries)) {
            foreach($mCountries as $mCountriesItem)
                $countriesZones[] = array(
                        '1'     => $mCountriesItem->zone_1,
                        '2'     => $mCountriesItem->zone_2,
                        '3'     => $mCountriesItem->zone_3,
                        '4'     => $mCountriesItem->zone_4,
                        '5'     => $mCountriesItem->zone_5,
                        /*'6'     => $mCountriesItem->group_1,
                        '7'     => $mCountriesItem->group_2,
                        '8'     => $mCountriesItem->group_3,
                        '9'     => $mCountriesItem->group_4,
                        '10'    => $mCountriesItem->group_5,
                        '11'    => $mCountriesItem->group_6*/
                );

				$zonesSumArr = $this->getZonesSum($countriesZones);

				if(!in_array($count, $zonesSumArr)) $same = false;

                $result = $this->getZonesResult($zonesSumArr, $count, $same);
        }
        
        return $result;
    }
	
	//применение скидки
	private function applyDiscount($cost = 0, $params = array()) {
		$result				= new stdClass;
		$result->discount	= 0;
		$result->old		= $cost;
		$result->main		= $cost;
		
		$params['cost'] = $cost;
		if ($params['certificate'] != '') {
			$cert = Certificate::getCost($params);
			
			if (isset($cert['cost']) && isset($cert['discount'])) {
				$result->discount	= $cert['discount'];
				$result->old		= $cost;
				$result->main		= $cert['cost'];
			}
		}
		
		return $result;
	}
	
	//получить стоимость текущего заказа
	private function getCost($sOrders = NULL, $sOrderType = NULL) {
		if (!$sOrders)		$sOrders	= Yii::app()->session['Orders'];
		if (!$sOrderType)	$sOrderType	= Yii::app()->session['OrderType'];
		
		$methodName = 'getCostType' . $sOrders['type_id'];
		if(method_exists($this, $methodName))
			return $this->$methodName($sOrders, $sOrderType);
	}
	
	private function getCostType1($reqOrders, $reqOrderType) {
		$default_params = array(
			'type_id'		=> NULL,
			'equipment_id'	=> NULL,
			'tariff_id'		=> NULL,
			'countries'		=> array(),
			'certificate'	=> NULL,
			'days'			=> NULL
		);

		$params = array_merge($default_params, $reqOrders, $reqOrderType);
		
		$resultArray		= array();
		$countriesZones		= array();
		$same				= true;
		$costTariff			= 0;
		$quantityMb			= 1;
		$tariffGb2Go		= false;
		
		$cost						= new stdClass;
		$cost->main					= 0;
		//$cost->equipment			= 0;
		//$cost->equipment_deposit	= 0;
		$cost->old					= 0;
		$cost->discount				= 0;
		$cost->common_zones			= $this->getCommonZones($params['countries']);

		$countCountries = $params['countries'] ? count($params['countries']) : 0;
		
		$msCountries	= NULL;
		
		if ($params['tariff_id']) {
		
			$mTariffs		= Tariffs::model()->findByAttributes(array('tariff_id'=>$params['tariff_id']));
			$tariffSettings	= CJSON::decode($mTariffs->settings, false);
			$quantityMb		= $tariffSettings->quantity_mb;
			
			if (isset($tariffSettings->type) && $tariffSettings->type == 'gb2go') {
				$cost->main		= $tariffSettings->cost;
				$cost->mb		= round($tariffSettings->cost/$tariffSettings->quantity_mb, 2);
				$tariffGb2Go	= true;
				//return $cost;
			} else
				if ($countCountries > 1) {
					$msCountries = Countries::model()->findAllByPk($params['countries']);

					foreach($msCountries as $mCountries)
						$countriesZones[] = array(
								'1'     => $mCountries->zone_1,
								'2'     => $mCountries->zone_2,
								'3'     => $mCountries->zone_3,
								'4'     => $mCountries->zone_4,
								'5'     => $mCountries->zone_5
						);
						
					$resultArray = $this->getZonesSum($countriesZones);

					if(!in_array($countCountries, $resultArray)) $same = false;

					$resultArray = $this->getZonesResult($resultArray, $countCountries, $same);

					$criteria = new CDbCriteria();
					if($same) {
						$criteria->select = 'min(cost_tariff_' . $params['tariff_id'] . ') as cost';
						$criteria->addInCondition('id', $resultArray);
						$modelZones = Zones::model()->find($criteria);
						$costTariff = $modelZones->cost;
					} else {
						$criteria->select = 'max(cost_tariff_' . $params['tariff_id'] . ') as cost';
						$criteria->addInCondition('id', $resultArray);
						$modelZones = Zones::model()->find($criteria);
						$costTariff = $modelZones->cost;
					}
				} else {
					$mTariffsRoaming3gSingle = TariffsRoaming3gSingle::model()->findByAttributes(
						array(
							'country_id'	=> reset($params['countries'])
						)
					);
					
					if($mTariffsRoaming3gSingle && $params['tariff_id']) {
						$attrTariff = 'cost_tariff_' . $params['tariff_id'];
						$costTariff = $mTariffsRoaming3gSingle->$attrTariff;
					}
				}
			
		}
		
		$mEquipments		= Equipments::model()->findByPk($params['equipment_id']);
		if ($mEquipments) {
			//$cost->equipment			+= $mEquipments->price_rent * $params['days'];
			//$cost->equipment_deposit	+= $mEquipments->price_deposit;
			//$cost->main					+= $cost->equipment + $cost->equipment_deposit;
			$cost->main					+= $mEquipments->cost;
		}

		if (!$tariffGb2Go) {
			$cost->main += $params['days'] * $costTariff;
			$cost->mb	= round($costTariff/$quantityMb, 2);
			
			//добавленная стоимость: sim-карта
			if ($cost->main > 0)
				$cost->main += $this->getAdditionalCost($params['type_id']);
		}

		//применение скидки
		if ($discount = $this->applyDiscount($cost->main, $params)) {
			$cost = (object)array_merge((array)$cost, (array)$discount);
		}
		
		return $cost;
	}
	
	//private function getCostType2Many($reqOrdersOne, $reqOrderTypeMany) {
	private function getCostType2($reqOrdersOne, $reqOrderTypeMany) {
		$obj = new stdClass;
		foreach($reqOrderTypeMany as $reqOrderType) {
			//$response = $this->getCostType2($reqOrdersOne, $reqOrderType);
			$response = $this->getCostType2Item($reqOrdersOne, $reqOrderType);

			foreach($response as $key => $value) {
				$obj->{$key} = isset($obj->{$key}) ? $obj->{$key} : NULL;

				if (is_float($value) || is_int($value)) $obj->{$key} += $value;
				elseif ($value !== NULL) $obj->{$key} = $value;
			}
		}
		
		return $obj;
	}
	
	//private function getCostType2($reqOrders, $reqOrderType) {
	private function getCostType2Item($reqOrders, $reqOrderType) {
		$default_params = array(
			'type_id'		=> NULL,
			'tariff_sim_id'	=> NULL,
			'type_sim_id'	=> NULL,
			'balans_id'		=> NULL,
			'add_balans'	=> NULL,
			'certificate'	=> NULL,
		);
		
		$params = array_merge($default_params, $reqOrders, $reqOrderType);
		
		$response				= new stdClass;
		$response->success		= false;
		$response->bonus		= 0;
		$response->cost			= 0;
		
		//if(count($params['countries']) > 0) {
			$cost_sim		= 10;
			$cost_balans	= 0;

			if($params['balans_id']) {
				$mKeyValue = KeyValue::model()->findByAttributes(
					array(
						'key'=>'order_type_2.balans_id',
						'value'=>$params['balans_id']
					)
				);
				if($mKeyValue)
					$cost_balans = $mKeyValue->cost;
			}
			
			if($params['add_balans'] == '')
				$params['add_balans'] = 0;

			//$mTariffs	= Tariffs371::model()->findByAttributes(array('country'=>$params['countries']));
			//$mTariffs371	= Tariffs371::model()->findByAttributes(array('country'=>$params['countries'][0]));
						
			//if($mTariffs) {
				$response->success			= true;
				$response->cost				= $cost_sim + $cost_balans + $params['add_balans'];
				$response->main				= $response->cost;
				
				if($response->cost >= 30 && $response->cost < 50)
					$response->bonus = $response->cost * 0.1;
				elseif($response->cost >= 50)
					$response->bonus = $response->cost * 0.2;
			//}
		//}
		
		//применение скидки
		if ($discount = $this->applyDiscount($response->cost, $params)) {
			$response = (object)array_merge((array)$response, (array)$discount);
		}
			
		return $response;
	}
	
	private function getCostType3($reqOrders, $reqOrderType) {
		$default_params = array(
			'type_id'		=> NULL,
			'certificate'	=> NULL,
			'prefix'		=> '',
			'number'		=> '',
			'cost'			=> 0
		);
		
		$params = array_merge($default_params, $reqOrders, $reqOrderType);
		
		$response = new stdClass;
		$response->main = $params['cost'];
		
		return $response;
		
	}
	
	private function getCostType4($reqOrders, $reqOrderType) {
		$default_params = array(
			'type_id'		=> NULL,
			'tariff_id'		=> NULL,
			'certificate'	=> NULL,
			'quantity'		=> NULL,
			'cost'			=> 0
		);
		
		$params = array_merge($default_params, $reqOrders, $reqOrderType);
		
		$response = new stdClass;
		
		$mKeyValue = KeyValue::model()->findByAttributes(
			array('key'=>'order_type_4.tariff_id', 'value'=>$reqOrderType['tariff_id'])
		);
		
		/*$params['cost'] = $mKeyValue->cost;
		
		$certificate = Certificate::getCost($params);
		if($certificate) {
			$params['cost']		= $certificate['cost'];
			$params['discount']	= $certificate['discount'];
		}*/

		//применение скидки
		if ($discount = $this->applyDiscount($mKeyValue->cost, $params)) {
			$response = (object)array_merge((array)$response, (array)$discount);
		}

		$response->success	= true;
		//$response->main		= $params['quantity'] * $params['cost'];
		$response->main		= $params['quantity'] * $discount->main;
		
		return $response;
	}
	
	private function getCostType5($reqOrders, $reqOrderType) {
		$default_params = array(
			'type_id'		=> NULL,
			'certificate'	=> NULL,
			'iccid'			=> '',
			'cost'			=> 0
		);
		
		$params = array_merge($default_params, $reqOrders, $reqOrderType);
		
		$mKeyValue = KeyValue::model()->findByAttributes(
			array(
				'key'=>'order_type_5.tariff_id',
				'value'=>$params['tariff_id'],
        'disabled'=>0
			)
		);
		
		$response		= new stdClass;
		$response->main = $mKeyValue->cost;//$params['cost'];
		
		return $response;
		
	}
	
	
	private function setSessionOrder($orders = NULL, $orderType = NULL, $delivery = NULL) {
		if ($orders)
			Yii::app()->session['Orders'] = $orders;
			
		if ($orderType)
			Yii::app()->session['OrderType'] = $orderType;
		
		if ($delivery)
			Yii::app()->session['Delivery'] = $delivery;
	}

	static function issetOrderInSession() {
		$result		= false;
		$sOrders	= Yii::app()->session['Orders'];
		$sOrderType	= Yii::app()->session['OrderType'];

		if($sOrders && $sOrderType) {
			if(isset($sOrders['type_id'])) {
				$modelName = 'OrderType' . $sOrders['type_id'];

				if(@class_exists($modelName)) {
					$mOrderType				= new $modelName;
					$mOrderType->attributes	= $sOrderType;
					$result					= $mOrderType->validate();
				}
			}
		}

		return $result;
	}
    
    private function getAdditionalCost($type = NULL) {
        $cost = 0;
        if($type) {
            $mOrderTypes = OrderTypes::model()->findByPk($type);
            if($mOrderTypes && $mOrderTypes->settings != '') {
                $settings = CJSON::decode($mOrderTypes->settings, false);

                if($settings && $settings->add_cost)
                    foreach($settings->add_cost as $item)
                        if(isset($item->cost))
                            $cost += $item->cost;
            }
        }
        
        return $cost;
    }
	
	public function actionGetOrderEquipment() {
		$json = new stdClass;
		$json->success	= false;
		$sOrderType		= Yii::app()->session->get('OrderType');
		
		if ($sOrderType && isset($sOrderType['equipment_id'])) {
			$json->success		= true;
			$json->equipment_id	= $sOrderType['equipment_id'];
		}
		
		echo CJSON::encode($json);
	}
	
	public function actionAddEquipment() {
		$json			= new stdClass;
		$json->success	= false;
		$sOrders		= Yii::app()->session->get('Orders');
		$equipment_id	= Yii::app()->request->getPost('equipment_id');
		
		if ($sOrders && isset($sOrders['type_id']) && $equipment_id) {
			if ($sOrders['type_id'] == 1 || $sOrders['type_id'] == 2) {
				Yii::app()->session['OrderType'] = array_merge(
					Yii::app()->session['OrderType'],
					array('equipment_id'=>$equipment_id)
				);
				$json->success		= true;
				$json->equipment_id	= $equipment_id;
			}
		}
		
		echo CJSON::encode($json);
	}
	
	/*
	public function actionAOrderType1Equipment() {
		$json       	= new stdClass;
		$sOrders		= Yii::app()->session['Orders'];
		$reqOrderType	= Yii::app()->request->getPost('OrderType');
		
		$json->success 	= false;
		if(!($sOrders && $reqOrderType)) {
			echo CJSON::encode($json);
			Yii::app()->end();
		}

		$json->success = true;
		$this->setSessionOrder($sOrders, $reqOrderType);
		
		$cost = $this->getCost();
		$json->equipment_name			= '';
		$json->equipment_deposit_cost	= $cost->equipment_deposit;
		$json->cost						= $cost->main;		

		if (isset($reqOrderType['equipment_id']) && $reqOrderType['equipment_id'] > 0) {
			$modelEquipments = Equipments::model()->findByPk($reqOrderType['equipment_id']);
			if ($modelEquipments)
				$json->equipment_name = $modelEquipments->name;
		} else
			unset($_SESSION['OrderType']['equipment_id']);
		
		echo CJSON::encode($json);
		Yii::app()->end();
	} */

	//получаем интервал дат по начальной дате и количеству дней
	private function getDateInterval($date, $days) {
		$datetime	= new DateTime($date);
		$start		= $datetime->format('d.m.Y');
		
		$datetime->modify('+' . $days . ' day');
		
		$end	= $datetime->format('d.m.Y');
		return $start . '-' . $end;
	}

	//получить дату со смещением в days дней
	private function getDateOffset($date, $days) {
		$datetime = new DateTime($date);
		$datetime->modify('+' . $days . ' day');
		return $datetime->format('d-m-Y');
	}

	public function actionAddOrder() {
		$reqOrders		= Yii::app()->request->getPost('Orders');
		$reqOrderType	= Yii::app()->request->getPost('OrderType');
		
		if ($reqOrders && $reqOrderType)
			$this->setSessionOrder($reqOrders, $this->convertPOSTArray($reqOrderType));
		//var_dump($reqOrders, $this->convertPOSTArray($reqOrderType)); die();
		//var_dump();
		//var_dump($this->setSessionOrder(array('a'=>1), array('b'=>2)));
	
		//echo CJSON::encode($this->getCost($reqOrders, $reqOrderType));
	}
	
	
	public function actionGetCost() {
		$reqOrders		= Yii::app()->request->getPost('Orders');
		$reqOrderType	= Yii::app()->request->getPost('OrderType');
		
		if ($reqOrders && $reqOrders['type_id'] == 2) {
			$reqOrderType	= $this->convertPOSTArray($reqOrderType);
			echo CJSON::encode($this->getCostType2($reqOrders, $reqOrderType));
		} else {
			$this->setSessionOrder($reqOrders, $reqOrderType);	
			echo CJSON::encode($this->getCost($reqOrders, $reqOrderType));
		}
		
		Yii::app()->end();
	}
	
	/*public function actionGetCostType2() {
		
	}*/
	
	//получить тарифы для order type 1
	public function actionGetTariffs() {
		$reqOrderType		= Yii::app()->request->getPost('OrderType');
		$countries			= isset($reqOrderType['countries']) ? $reqOrderType['countries'] : array();
		$msTariffs			= Tariffs::model()->findAllByAttributes(array('order_type_id'=>1));
		$groupsCountries	= Countries::model()->getGroups($countries);

		$json = new stdClass;
		$json->success = false;
		
		if ($msTariffs) {
			$json->success = true;
			foreach($msTariffs as $mTariffs) {
			
				$item = CJSON::decode($mTariffs->settings, false);
				$groupsResult = array_intersect($item->groups, $groupsCountries);

				//сбрасываем индексы массива и приводим их к типу int
				$groupsResult		= array_values(array_map('intval', $groupsResult));

				$disabled = ($groupsResult == $groupsCountries && (count($groupsResult) + count($groupsCountries) > 0)) ? false : true;
				$display_name = Yii::t('app', $item->display_name);

        //var_dump();
				if (isset($item->type) && $item->type == 'gb2go') {
				
					if (!isset($tariffIdGb2Go)) {
						$json->items[$mTariffs->tariff_id] = array('id' => $mTariffs->tariff_id, 'name' => $display_name, 'disabled' => $disabled);
						$tariffIdGb2Go = $mTariffs->tariff_id;
					} else {
						if ($json->items[$tariffIdGb2Go]['disabled'] === true)
							$json->items[$tariffIdGb2Go] = array('id' => $mTariffs->tariff_id, 'name' => $display_name, 'disabled' => $disabled);	
					}
				} else
					$json->items[$mTariffs->tariff_id] = array('id' => $mTariffs->tariff_id, 'name' => $display_name, 'disabled' => $disabled);
			}
		}

		echo CJSON::encode($json);
	}

	private function getZonesSum($zones = array()) {
		$result = NULL;

		if(count($zones) > 0)
			foreach($zones as $zone)
				if($result === NULL)
					$result = $zone;
				else
					foreach($result as $key => $value)
						$result[$key] += $zone[$key];
		return $result;
	}
    
    private function getZonesResult($data, $count = NULL, $same = true) {
        //удаляем лишние нули, а оставляем только идентификаторы зон
        $result = array();

        foreach($data as $key => $value) {
            if($value != 0)
                if($same) {
                    if($value == $count)
                        $result[] = $key;
                } else
                    $result[] = $key;
        }

        return $result;
    }
	
	protected function getKeyValueName($key, $value) {
		$model = KeyValue::model()->findByAttributes(array('key'=>$key, 'value'=>$value));
		return $model->name;
	}
	
	/*private function getContentNotification($args) {
		if(isset($args['order_id']) && isset($args['client'])) {
			$model = Orders::model()->findByPk($args['order_id']);
			if($model) {
				$modelDelivery = NULL;
				if($model->delivery_id)
					$modelDelivery = $model->rDelivery;
					
				$modelOrderType	= $model->{'rOrderType' . $model->type_id};
				
				switch($model->type_id) {
					case 1:
					case 2:
					case 3:
					case 4:
						return $this->renderPartial('email/' . $args['client'] . '/type_' . $model->type_id . '_notification',
							array(
								'modelOrderType'	=> $modelOrderType,
								'modelDelivery'		=> $modelDelivery
							), true);
					break;
				}
			}
		}
	} */
	
	/*public function actionReadExcel() {
		Yii::import('application.extensions.custom.*');
        $parser = new parse();
        $parser->filename = 'resources/tariffs_02062014.xlsx';
        $parser->startRow = 8;
        $parser->endRow = 179;
        $parser->highestColumn = 'F';
        $parser->updateZones();
        //$parser->importTariffsRoaming3G();
		die();
	}*/
}
