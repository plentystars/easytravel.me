<?php
	$baseUrl = Yii::app()->request->baseUrl;
	Yii::app()->clientScript
			->registerCssFile($baseUrl . '/resources/css/bootstrap.css')
			->registerCssFile($baseUrl . '/resources/css/bootstrap-slider.css')
			->registerCssFile($baseUrl . '/resources/datepicker/css/datepicker.css')
			->registerCssFile($baseUrl . '/resources/select2/select2.css')
			->registerCssFile($baseUrl . '/resources/css/font-awesome.css')
			->registerCssFile($baseUrl . '/resources/css/normalize.css')
			->registerCssFile($baseUrl . '/resources/css/main.css')
			->registerCssFile($baseUrl . '/resources/css/modal.css');
			
	Yii::app()->clientScript
			->registerScriptFile('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', CClientScript::POS_HEAD)
			->registerScriptFile($baseUrl . '/resources/js/jquery.maskedinput.js', CClientScript::POS_HEAD)
			->registerScriptFile($baseUrl . '/resources/js/bootstrap.js', CClientScript::POS_HEAD)
			->registerScriptFile($baseUrl . '/resources/js/bootstrap-slider.js', CClientScript::POS_HEAD)
			->registerScriptFile($baseUrl . '/resources/datepicker/js/bootstrap-datepicker.js', CClientScript::POS_HEAD)
			->registerScriptFile($baseUrl . '/resources/select2/select2.js', CClientScript::POS_HEAD)
			->registerScriptFile($baseUrl . '/resources/js/plugins.js', CClientScript::POS_HEAD)
			->registerScriptFile($baseUrl . '/resources/js/main.js', CClientScript::POS_HEAD);
			
	if (Yii::app()->language !== 'en')
			Yii::app()->clientScript
				->registerScriptFile($baseUrl . '/resources/datepicker/js/locales/bootstrap-datepicker.' . Yii::app()->language . '.js', CClientScript::POS_HEAD);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Дешевый мобильный интернет, мобильная связь, VoIP и 3g роутеры с easytravel.me</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
		<script>
			//$("#icalc-sl2").slider();
			
			//common
			function getData(data, url, default_values) {
				var result = default_values ? default_values : {success: false};

				$.ajax({
					url: url,
					type: 'POST',
					data: data,
					dataType: 'json',
					async: false,
					cache: false,
					success: function(data) {result = data;}
				});

				return result;
			}
	
			function setData(data, url, default_values) {
				return getData(data, url, default_values);
			}

			function getModalData(url, data) {
				if (!data) data = {};
				var result = {title: '', content: ''};
				
				tjson = $.ajax({
					url: url,
					type: 'POST',
					data: data,
					dataType: 'json',
					async: false,
					cache: false,
					success: function(data) {
						if (data.success) result = data;
						//result = data;
					},
					error: function(xhr) {
						if (xhr.status === 403) {
							returnHash = url;
							// get login modal if access denied
							result = getModalData('login');
						}
					}
				});

				return result;
			}
			
			function showModalPage(page, data) {
				if (!data)
					data = {page: page};

				var data = getModalData(app_settings.url.getmodalpage, data);
				var modalId = '#modal';
				$(modalId + ' .modal-content').html(data.content);
				$(modalId).modal();
			}
			
			function showModalAction(url, data) {
				var data = getModalData(url, data);
				var modalId = '#modal';
				$(modalId + ' .modal-content').html(data.content);
				$(modalId).modal();
			}
			
			function scrollAnimate(selector, href) {
				//var pos = {'#chooseTariff': 100};
				var pos = {};
				var id = href ? href : $(selector).attr('href');
				pos = pos[id] ? pos[id] : 0;
				$('html, body').animate({scrollTop: $(id).offset().top - 200}, 1500);
			}
		</script>
		
		
		
		<!--- CHAT  --->
<script type="text/javascript">
_shcp = []; _shcp.push({widget_id : 629677, widget : "Chat"}); (function() { var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true; hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://widget.siteheart.com/apps/js/sh.js"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(hcc, s.nextSibling); })();
</script>
<!-- END CHAT ---->

<!--- GA --->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44122386-1', 'auto');
  ga('send', 'pageview');

</script>

<!--END GA --->


<!----METRIKA----->

<!-- Yandex.Metrika counter -->

<script type="text/javascript">

(function (d, w, c) {

 (w[c] = w[c] || []).push(function() {

 try {

 w.yaCounter22557637 = new Ya.Metrika({id:22557637,

 webvisor:true,

 clickmap:true,

 trackLinks:true,

 accurateTrackBounce:true});

 } catch(e) { }

 });

 var n = d.getElementsByTagName("script")[0],

 s = d.createElement("script"),

 f = function () { n.parentNode.insertBefore(s, n); };

 s.type = "text/javascript";

 s.async = true;

 s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

 if (w.opera == "[object Opera]") {

 d.addEventListener("DOMContentLoaded", f, false);

 } else { f(); }

})(document, window, "yandex_metrika_callbacks");

</script>

<noscript><div><img src="//mc.yandex.ru/watch/22557637" style="position:absolute; left:-9999px;" 

alt="" /></div></noscript>

<!-- /Yandex.Metrika counter -->

    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<?php echo $content; ?>
    </body>
	<script>
		$('.scrollTo').click(function() {
			//var href = $(this).attr('href');
			scrollAnimate(this);
		});
		if (document.location.hash == '#login')
			showModalAction('login');
	</script>
</html>