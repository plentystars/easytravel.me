<section class="container content-internet-calc  bt-clr-icalc container-sm-height" id="section-universal">
	<h1><?php echo Yii::t('app', 'CARD_UNIVERSAL'); ?></h1>
	<form class="form-horizontal" role="form" id="OrderType2">
		<input type="hidden" name="Orders[type_id]" value="2" />
		<div class="row row-sm-height">
			<div class="col-md-6 mcalc-left col-sm-height">
					<select name="OrderType[countries][]" style="width: 100%;" placeholder="<?php echo Yii::t('app', 'CHOOSE_COUNTRY'); ?>">
						<option selected></option>
						<?php
							foreach($data['countries'] as $country)
								echo '<option value="' . $country['id'] . '">' . $country['name'] . '</option>';
						?>
					</select>
					<div class="row mcalc-tariffs hidden-xs text-center">
						<div class="col-sm-4">
							<?php echo Yii::t('app', 'CALLS_OUT_SHORT'); ?>
							<p class="OrderType2_costOut text-center"><span>0</span> $</p>
						</div>
						<div class="col-sm-4">
							<?php echo Yii::t('app', 'SMS'); ?> 
							<p class="OrderType2_costSms text-center"><span>0</span> $</p>
						</div>
						<div class="col-sm-4">
							<?php echo Yii::t('app', 'CALLS_IN_SHORT'); ?>
							<p class="OrderType2_costIn text-center"><span>0</span> $</p>
						</div>
					</div>
					<!---Mobile adapt---->
					<div class="row">
						<div class="hidden-lg hidden-md hidden-sm padlformobile">
							<p class="OrderType2_costOut"><?php echo Yii::t('app', 'CALLS_OUT'); ?>: <strong><span>0</span> $</strong></p>
							<p class="OrderType2_Sms"><?php echo Yii::t('app', 'SMS'); ?>: <strong><span>0</span> $</strong></p>
							<p class="OrderType2_costIn"><?php echo Yii::t('app', 'CALLS_IN'); ?>: <strong><span>0</span> $</strong></p>
						</div>
					</div>
					
					<!---END Mobile adapt---->
					
					
					<div class="mcalc-slider-1">
						<p class="caption text-right"><a href="#" class="countrylist" modal-action="voip"><?php echo Yii::t('app', 'VOIP_COST_LINK'); ?></a></p>
						<p class="caption"><?php echo Yii::t('app', 'NEED_BALANCE_DESCRIPTION'); ?></p>
						<p><?php echo Yii::t('app', 'QUANTITY_MIN_OUT_DAY'); ?>
						<span><span id="mcalc-sl1-val" class="mcalc-slider-1-val">50</span></span></p>
						<input id="mcalc-sl1" type="text" data-slider-min="1" data-slider-max="200" data-slider-step="1" data-slider-value="50" value="50">
					</div>
					<div class="mcalc-slider-1">							
						<p><?php echo Yii::t('app', 'QUANTITY_SMS_DESCRIPTION'); ?>
						<span><span id="mcalc-sl2-val" class="mcalc-slider-1-val">10</span></span></p>
						<input id="mcalc-sl2" type="text" data-slider-min="1" data-slider-max="40" data-slider-step="1" data-slider-value="10" value="10">
					</div>
					<div class="mcalc-final">
						<p class="caption"><?php echo Yii::t('app', 'COST_CAPTION_NONTEMPLATE'); ?></p>
						<div class="row">
							<div class="col-sm-8 mcalc-capt">
								<?php echo Yii::t('app', 'RECOMENDED_BALANCE_DAY'); ?>:  
							</div>
							<div class="col-sm-4 mcalc-sum">
								<span id="OrderType2_recBalans">0</span> $
							</div>
						</div>
					</div>
			</div>
			<div class="col-md-6 mcalc-right col-sm-height">
					<div id="OrderType2_items"></div>
					<div class="row">
						<div class="col-sm-8 col-md-8">
						  <div class="icalc-simtype">
							<div class="form-group">
								<label for="certificate"><?php echo Yii::t('app', 'ORDER_CERTIFICATE'); ?></label>
								<input type="text" class="form-control" name="Orders[certificate]" id="certificate" placeholder="<?php echo Yii::t('app', 'ENTER') . ' ' . Yii::t('app', 'ORDER_CERTIFICATE'); ?>">
							  </div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="btn-promocode">
								<button type="button" class="btn btn-default applyPromo"><?php echo Yii::t('app', 'APPLY'); ?></button>
							</div>
						</div>
					</div>
					<div class="addmoresim">
						<a id="OrderType2_addsim" href="#"><i class="fa fa-plus-square-o " style="padding-left: 10px;"></i> <?php echo Yii::t('app', 'ADD_TO_ORDER_SIMCARD'); ?></a>
					</div>
					<div class="mcalc-final">
						<p class="caption"><?php echo Yii::t('app', 'COST_SIMCARD_WITHOUT_BALANCE'); ?> - 10 $</p>
						<div class="row">
							<div class="col-sm-4 mcalc-capt">
								<?php echo Yii::t('app', 'ORDER_COST'); ?>: 
							</div>
							<div class="col-sm-8 mcalc-sum">
                                <!-- 
                                <span id="OrderType2_cost">0</span>$ 
                                <span class="s-oldprice">999 $</span>
                                -->
                                <span class="order-cost cost-main">0</span><span> $</span>
								<span class="order-cost s-oldprice">0</span><span class="s-oldprice"> $</span>
                            </div>
						</div>
					</div>
			</div>
		</div>
		<div class="icalc-orderbtn">
			 <button id="OrderType2_submit" type="button" class="btn btn-default"><?php echo Yii::t('app', 'ORDER'); ?></button>
		</div>
	</form>
	<!--
	<div class="howtouse"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/play-btn.png" alt="Video"> Как пользоваться калькулятором</a></div>
	<h3>Как пользоваться калькулятором</h3>
	<div class="icalc-video">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video.png" class="img-responsive" alt="Video">
	</div>
	-->
</section>

<script>

	var orderType2Name	= 'OrderType2';
	var costTariff		= {};
	var idSliderMinutes	= '#mcalc-sl1';
	var idSliderSms		= '#mcalc-sl2';

	$(['#', orderType2Name, ' select[name="OrderType[countries][]"]'].join(''))
		.select2().on('change', function() {
			
			var id = $(['#', orderType2Name, ' [name="OrderType[countries][]"]'].join('')).val();
		
			var data = getData({OrderType: {countries:[id]}}, app_settings.url.gettariff371info);
			if (data.success) {
				costTariff = data.items;
				$(['.', orderType2Name, '_costIn span'].join('')).html(costTariff.cost_call_in);
				$(['.', orderType2Name, '_costOut span'].join('')).html(costTariff.cost_call_out);
				$(['.', orderType2Name, '_costSms span'].join('')).html(costTariff.cost_sms_out);

				var minutes	= $([idSliderMinutes, '-val'].join('')).html();
				var sms		= $([idSliderSms, '-val'].join('')).html();
				updateRecBalans(minutes, sms, data.items);
		}
	});

	var dataSession = getData({},app_settings.url.getsessionorder);
	
	var orderType2Item = $('<?php echo str_replace(array("\r", "\n"), '', $this->renderPartial('//partials/universal_order_item', array('data'=>$data), true)); ?>');
	
	orderType2Item
		.find(['.', orderType2Name, '_balansId button'].join(''))
		.unbind('click')
		.bind('click', function() {
			$(this)
				.parent()
				.find('input[name="OrderType[balans_id][]"]')
				.val($(this).attr('item-id'))
				.trigger('change');
		});
		
	orderType2Item
		.find('input[name="OrderType[add_balans][]"]')
		.unbind('keyup change')
		.bind('keyup change', function() {
			updateCostOrderType2();
		});
		
	orderType2Item
		.find('input[name="OrderType[balans_id][]"]')
		.unbind('change')
		.bind('change', function() {
			var value			= $(this).val();
			var class_active	= 'active';
			var buttons			= $(this).parent().find('button');

			$.each(buttons, function(index, el) {
				if ($(el).attr('item-id') == value)
					$(el).addClass(class_active);
				else
					$(el).removeClass(class_active);
			});

			updateCostOrderType2();
		});
	
	if (dataSession.success && dataSession.order_type && dataSession.orders && dataSession.orders.type_id == 2) {
		$.each(dataSession.order_type, function(index, el) {
			var obj = orderType2Item.clone(true);
			
			$.each(el, function(key, value) {
				obj.find('[name="OrderType[' + key + '][]"]').val(value).trigger('change');
			});
			$(['#', orderType2Name, '_items'].join('')).append(obj);
		});

		$(['#', orderType2Name, ' [name="OrderType[countries][]"]'].join(''))
			.val(dataSession.order_type[0].countries)
			.trigger('change');
		$(['#', orderType2Name, ' [name="Orders[certificate]"]'].join(''))
			.val(dataSession.orders.certificate);
	} else {
		$(['#', orderType2Name , '_items'].join('')).append(orderType2Item);
	}
	
	updateCostOrderType2();
	
	$(['#', orderType2Name , '_addsim'].join('')).click(function() {
		var orderType2ItemClone = orderType2Item.clone(true);
		
		orderType2ItemClone.append(
			$('<a style="color: #ff0000;" href="#"><i class="fa fa-times" title="DELETE"></i> delete</a>').bind('click', function() {
				$(this).unbind('click');
				$(this).parent().remove();
				updateCostOrderType2();
				return false;
			})
		);

		$(['#', orderType2Name , '_items'].join('')).append(orderType2ItemClone);

		updateCostOrderType2();
		return false;
	});
	
	$(['#', orderType2Name , '_submit'].join(''))
		.click(function() {
			var form = $(this).closest('form');
			setData(form.serialize(), app_settings.url.addorder);
			showModalAction('delivery');
			return false;
	});

	$([idSliderMinutes, idSliderSms].join(' ,'))
		.slider()
		.on('slide', function(slideEvt) {
			$('#' + $(this).attr('id') + '-val').text(slideEvt.value);
		})
		.on('slideStop', function(event) {
			if ('#' + event.target.id == idSliderMinutes)
				updateRecBalans(event.value, $(idSliderSms).val());
			else if ('#' + event.target.id == idSliderSms)
				updateRecBalans($(idSliderMinutes).val(), event.value);
		});
	
	function updateRecBalans(minutes, sms) {		
		var cost = (costTariff.cost_call_out || 0) * minutes + (costTariff.cost_sms_out || 0) * sms;
		$(['#', orderType2Name, '_recBalans'].join('')).html(Math.round(cost));
	}
	
	function updateCostOrderType2() {
		var result = getData($(['#', orderType2Name].join('')).serialize(), app_settings.url.getcost);
		
		if (result.success) {
            console.info(['#', orderType2Name, ' .order-cost', ' .cost-main'].join(''));
			$(['#', orderType2Name, ' .order-cost', '.cost-main'].join('')).html(result.main);
            
            var form = ['#', orderType2Name].join('');

            if (result.old)
                $(form).find('.order-cost.s-oldprice').html(result.old);
				
            if (result.main === result.old)
                $(form).find('.s-oldprice').hide();
            else
                $(form).find('.s-oldprice').show();
        }
	}
	
	$(['#', orderType2Name, ' .applyPromo'].join('')).click(function() {
		updateCostOrderType2();
	});
</script>