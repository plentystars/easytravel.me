<section class="container content-internet addcash" id="section-addcash">
	<h1><?php echo Yii::t('app', 'ADD_CASH'); ?></h1>
		<div class="row">
		<form class="form-horizontal" role="form" id="OrderType3">
			<input type="hidden" name="Orders[type_id]" value="3" />
			<div class="col-md-6">
				<div class="addcash-left">
					<p><?php echo Yii::t('app', 'SIM_CARD'); ?></p>
					<span>easyroaming</span>
					<div class="icalc-simtype">
						<div class="form-group">
							<label for="telnumber3712"><?php echo Yii::t('app', 'PHONE_NUMBER'); ?></label>
							<input type="text" name="OrderType[number]" class="form-control easy-form-input masked-3712" id="telnumber3712" placeholder="+3712">
						 </div>
					</div>
					<div class="form-group">
						<label for="addc3712-sum" class="col-sm-3  easy-form-label"><?php echo Yii::t('app', 'SUM'); ?></label>
						<div class="col-sm-9">
							<input type="text" name="OrderType[cost]" class="form-control easy-form-input" id="addc3712-sum" placeholder="$">
						</div>
				  </div>
					<div class="addcashbtn">
						<button type="submit" class="btn btn-default" disabled="disabled"><?php echo Yii::t('app', 'TOPUP'); ?></button>
					</div>
				</div>
			</div>
		</form>

		<form class="form-horizontal" role="form" id="OrderType5">
			<input type="hidden" name="Orders[type_id]" value="5" />
			<div class="col-md-6">
				<div class="addcash-right">
					<p><?php echo Yii::t('app', 'SIM_CARD'); ?></p>
					<span>Keepgo</span>
					<div class="icalc-simtype">
						<div class="form-group">
							<label for="ICCID"><?php echo Yii::t('app', 'NUMBER_ICCID'); ?></label>
							<input type="text" name="OrderType[iccid]" class="form-control easy-form-input" id="ICCID" />
						 </div>
					</div>
					<div class="form-group">
						<label for="addc3712-sum" class="col-sm-3 easy-form-label"><?php echo Yii::t('app', 'SUM'); ?></label>
						<div class="col-sm-9">
							<select name="OrderType[tariff_id]" class="form-control easy-form-input">
								<?php foreach($data['items'] as $item): ?>
									<option value="<?php echo $item->value; ?>"><?php echo $item->name . ' - ' . $item->cost . '$'; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="addcashbtn">
						<button type="submit" class="btn btn-default" disabled="disabled"><?php echo Yii::t('app', 'TOPUP'); ?></button>
					</div>
				</div>
			</div>
		</form>
	</div>
	<!--
	<div class="howtouse"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/play-btn.png" alt="Video"> Как пополнить баланс сим-карты KeepGo</a></div>
	<h3>Как пополнить баланс сим-карты KeepGo</h3>
	<div class="icalc-video">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video.png" class="img-responsive" alt="Video">
	</div>
	-->
<!--	
<div style="margin: 20px; border: 1px #000 solid;">
	<form id="test_order_1">
		<input type="hidden" name="Orders[type_id]" value="3" />
		<input type="text" name="OrderType[number]"  value="123456789" />
		<input type="text" name="OrderType[cost]" value="12" />
		<button type="submit"><?php echo Yii::t('app', 'TOPUP'); ?></button>
	</form>
</div>
<div style="margin: 20px; border: 1px #000 solid;">
	<form id="test_order_2">
		<input type="hidden" name="Orders[type_id]" value="5" />
		<input type="text" name="OrderType[iccid]" value="12345678" />
		<select name="OrderType[tariff_id]">
			<option value="1" selected>1</option>
		</select>
		<button type="submit"><?php echo Yii::t('app', 'TOPUP'); ?></button>
	</form>
</div>
-->
</section>

<script>
	var orderType3Name	= 'OrderType3';
	var orderType5Name	= 'OrderType5';

	$(".masked-3712").mask("+37129999999");
	
	$(['#', orderType3Name].join('')).change(function() {
		var mode = true;
		if (
			($('input[name="OrderType[number]"]').val().length == 12) &&
			($('input[name="OrderType[cost]"]').val().length >= 1)
		)
			mode = false;
		
		$(this).find('button[type="submit"]').attr('disabled', mode);
	});
	
	//$(['#', orderType3Name, ' [type="submit"]', ',', '#', orderType5Name, ' [type="submit"]'].join('')).click(function() {
	$('#OrderType3 [type="submit"], #OrderType5 [type="submit"]').click(function() {
		var form = $(this).closest('form');
		setData(form.serialize(), app_settings.url.getcost, {main: 0});
		showModalAction('choosepayment');

		return false;
	});
	
	$('#test_order_1 [type="submit"], #test_order_2 [type="submit"]').click(function() {
		var form = $(this).closest('form');
		console.info(form);
		console.info(setData(form.serialize(), app_settings.url.getcost, {main: 0}));
		showModalAction('choosepayment');
		//alert(111);
		return false;
	});
	
	$(['#', orderType5Name].join('')).change(function() {
		var mode = true;
		if ($('input[name="OrderType[iccid]"]').val().length > 5)
			mode = false;

		$(this).find('button[type="submit"]').attr('disabled', mode);
	});
</script>