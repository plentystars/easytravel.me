<div class="fixheadertop">
	<div class="container">
<header>	
	 <div class="container">
		<div class="row">
			<div class="col-sm-8 col-md-8">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/logo.png" class="img-responsive">
			</div>
			<div class="col-sm-4 col-md-4" style="text-align: right;">
				<div class="row citychoose">
					<div class="col-sm-3" style="float:right; margin-bottom: 10px;">
						<div class="dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"><?php echo Yii::app()->language; ?><i class="fa fa-chevron-down" style="padding-left: 10px;"></i></button>
							<ul class="dropdown-menu dd-menu-clr" role="menu" aria-labelledby="dropdownMenu1">
								<?php $languages = Languages::getList(); ?>
								
								<?php foreach($languages as $language): ?>
								<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo Yii::app()->createUrl('/language/' . $language); ?>"><?php echo $language; ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
					<style>
						 .btn-link {
							color: #2d2d2d;
							font-weight: bold;
														padding: 0px 4px;
						 }
						 
						 .dropdown-menu.phonelist {
							left: -150px;
						 }
						 
						.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .dropdown-submenu:hover > a, .dropdown-submenu:focus > a {
						color: #ffffff;
						background-color: #C90A0A;
						background-image: -moz-linear-gradient(top, #46486b, #333554);
						background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#46486b), to(#333554));
						background-image: -webkit-linear-gradient(top, #46486b, #333554);
						background-image: -o-linear-gradient(top, #46486b, #333554);
						background-image: linear-gradient(to bottom, #46486b, #333554);
						}
						
						
						 </style>
						 <?php echo Yii::t('app', 'HOT_LINE'); ?>: 
						 <div class="btn-group">
							   <a class="btn dropdown-toggle btn-link" data-toggle="dropdown" href="#" id="dropdown-current-phone">
								  (495) 212-90-06
								<span class="caret"></span>
							  </a>
							  <ul class="dropdown-menu phonelist" role="menu" aria-labelledby="dropdownMenu" id="dropdown-phones">
								  <li><a tabindex="-1" href="#">Москва +7 <span>(495) 212-90-06</span></a></li>
								  <li class="divider"></li>
								  <li><a tabindex="-1" href="#">Санкт-Петербург +7 <span>(812) 748-23-00</span></a></li>
								  <li class="divider"></li>
								  <li><a tabindex="-1" href="#">Нижний Новгород +7 <span>(831) 280-83-11</span></a></li>
								  <li class="divider"></li>
								  <li><a tabindex="-1" href="#">Ростов-на-Дону +7 <span>(863) 303-30-11</span></a></li>
								  <li class="divider"></li>
								  <li><a tabindex="-1" href="#">Самара +7 <span>(846) 212-95-12</span></a></li>
								</ul>
							</div>
							<div><a href="mailto:assist@easyroaming.ru">assist@easyroaming.ru</a></div>

				
			</div>
		</div>
	</div>
	<p class="header_caption">Интернет-магазин <span class="color-orange">доступного интернета</span> и <span class="color-green">мобильной связи</span> для путешественников</p>
</header>