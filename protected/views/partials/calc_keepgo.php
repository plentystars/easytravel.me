<style>
    .s-oldprice {display: none;}
</style>

<section class="container content-internet-calc  bt-clr-icalc container-sm-height" id="section-keepgo">
	<h1><?php echo Yii::t('app', 'CARD_INTERNET'); ?></h1>
	<form class="form-horizontal" role="form" id="OrderType1">
		<input type="hidden" name="Orders[type_id]" value="1" />
		<div class="row row-sm-height">
			<div class="col-md-6 icalc-left col-sm-height">
					<select name="OrderType[countries][]" placeholder="<?php echo Yii::t('app', 'CHOOSE_COUNTRY'); ?>" style="width: 100%; margin-bottom: 10px;" multiple>
						<?php
							foreach($data['countries'] as $country)
								echo '<option value="' . $country->id . '">' . $country->name . '</option>';
						?>
					</select>
					
						<!---style>
							.icalc-slider-1 .block-tariffs > button {
								width: 33.3%;
								}
							.pad0 {
								padding-top: 0px;
							}	
						</style>
						  <div class="icalc-slider-1 pad0">
							<p>Интернет-пакет</p>
							<div class="btn-group order-tariffs block-tariffs">
							  <button type="button" class="btn btn-default">40 Мб/сутки</button>
							  <button type="button" class="btn btn-default">100 Мб/сутки</button>
							  <button type="button" class="btn btn-default">1 Гб/месяц</button>
							</div>
						  </div>
						  <div class="icalc-slider-1 pad0">
							<p>Индивидуальная карта</p>
							<div class="btn-group order-tariffs block-tariffs">
							  <button type="button" class="btn btn-default">50 Мб/сутки</button>
							  <button type="button" class="btn btn-default">100 Мб/сутки</button>
							  <button type="button" class="btn btn-default">500 Мб/сутки</button>
							</div>
						  </div -->
					
					<div class="form-group addmoresim">
					<label class="col-sm-7  easy-form-label"><?php echo Yii::t('app', 'ORDER_DATE_ACTIVATION'); ?>: </label>
					<div class="col-sm-5">
					  <input name="OrderType[date_activation]" readonly="readonly" class="form-control easy-form-input datepicker" data-date-format="dd-mm-yyyy" data-date="<?php echo $data['date_activation']; ?>" value="<?php echo $data['date_activation']; ?>" style="cursor: pointer;">
					  <span class="gotooltip" data-toggle="tooltip"  title="<?php echo Yii::t('app', 'KEEPGO_ADD_DAYS_TOOLTIP'); ?>"> <i class="fa fa-question-circle" style="padding-right: 3px;"></i></span>
					</div>
				  </div>

				  <div class="icalc-slider-1">
					<p>
						<?php echo Yii::t('app', 'TRIP_PERIOD'); ?> <span>(<?php echo Yii::t('app', 'TRIP_PERIOD_HELP'); ?>)<span class="icalc-slider-1-bef-input col-sm-2"><input name="OrderType[days]" id="icalc-slider-1-input" class="form-control easy-form-input"></span></span>
					</p>
					<input id="icalc-sl1" type="text">
				  </div>
				  
				  <div class="icalc-slider-1">
					<p><?php echo Yii::t('app', 'ORDER_TARIFF'); ?></p>
					<div class="btn-group order-tariffs" id="OrderType1_tariffs"></div>
					<input type="hidden" name="OrderType[tariff_id]" field-trigger="change" />
				  </div>
				  <div class="icalc-simtype">
					<div class="form-group">
						<label for="simtype"><?php echo Yii::t('app', 'ORDER_TYPE_SIM'); ?></label>
						<select name="OrderType[type_sim_id]" class="form-control">
							<?php foreach($data['type_sim'] as $item): ?>
								<option value="<?php echo $item->value; ?>"><?php echo $item->name; ?></option>
							<?php endforeach; ?>
						  <!-- <option>SIM</option>
						  <option>nanoSIM</option>
						  <option>microSIM</option> -->
						</select>
					  </div>
					</div>
					<div class="row">
						<div class="col-sm-8 col-md-8">
						  <div class="icalc-simtype">
							<div class="form-group">
								<label for="certificate"><?php echo Yii::t('app', 'ORDER_CERTIFICATE'); ?></label>
								<input type="text" name="Orders[certificate]" class="form-control" id="certificate" placeholder="<?php echo Yii::t('app', 'ENTER') . ' ' . Yii::t('app', 'ORDER_CERTIFICATE'); ?>">
							  </div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="btn-promocode">
								<button type="button" class="btn btn-default applyPromo"><?php echo Yii::t('app', 'APPLY'); ?></button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<p id="OrderType1Equipment">
								<a class="scrollTo equipment-add" href="#section-equipment">
									<i style="padding-left: 10px;" class="fa fa-plus-square-o "></i>
									<?php echo Yii::t('app', 'ORDER_ADD_EQUIPMENT'); ?>
									<input type="hidden" name="OrderType[equipment_id]" />
								</a>
								
								<span class="equipment-item">
									<span class="equipment-item-name"></span>
									<span><a href="#" class="equipment-item-delete" style="display: none;"><i class="fa fa-times" title="<?php echo Yii::t('app', 'DELETE'); ?>"></i></a></span>
								</span>
							</p>
						</div>
					</div>
					<div class="icalc-final">
						<div class="row">
							<div class="col-sm-8 icalc-full">
								<p><?php echo Yii::t('app', 'ORDER_COST'); ?>:</p>
								<!----span class="order-cost">0</span><span> $</span--->
								<span class="order-cost s-discount">0</span><span class="s-discount"> $</span>
								<span class="order-cost s-oldprice">0</span><span class="s-oldprice"> $</span>
							</div>
							<div class="col-sm-4 icalc-mb">
								<p><?php echo Yii::t('app', 'ORDER_COST_MB'); ?></p>
								<span class="order-cost-mb">0</span><span> $</span>
							</div>
						</div>
					</div>
			</div>
			<?php $this->renderPartial('//partials/calc_keepgo_mb'); ?>
		</div>
		<div class="icalc-orderbtn">
			 <button disabled="disabled" type="button" class="btn btn-default submit-order"><?php echo Yii::t('app', 'ORDER'); ?></button>
		</div>
	</form>
	<!--
	<div class="howtouse"><a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/play-btn.png" alt="Video"> Как пользоваться калькулятором</a></div>
	<h3>Как пользоваться калькулятором</h3>
	<div class="icalc-video">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video.png" class="img-responsive" alt="Video">
	</div>
	-->
</section>

<script>
	var orderType1Name = 'OrderType1';

	$(['#', orderType1Name, ' .applyPromo'].join('')).click(function() {
		updateCostOrderType1(this);
	});

	updateTariffs();
	
	$(['#', orderType1Name, ' .submit-order'].join('')).click(function() {
		showModalAction('delivery');
		return false;
	});
	
	//--------- countries -----------
	$(['#', orderType1Name, ' select[name="OrderType[countries][]"]'].join(''))
		.select2()
		.on('change', function() {
			updateTariffs();
			updateCostOrderType1(this);
		});
	//--------- /countries -----------
	
	//--------- days slider & input -----------
	var idSliderDays		= "#icalc-sl1";
	var idSliderDaysValue	= "#icalc-slider-1-input";
	var sliderDaysOptions	= {min: 5, max: 90, value: 14, step: 1};
	
	$(idSliderDays).slider(sliderDaysOptions);
	$(idSliderDays)
		.on('slide', function(slideEvt) {
			$(idSliderDaysValue).val(slideEvt.value);
		})
		.on('slideStop', function(event) {
			updateCostOrderType1(this);
		});
	
	$(idSliderDaysValue).val(sliderDaysOptions.value);
	$(idSliderDaysValue).bind('keyup change', function() {
		if (!($(this).val() < sliderDaysOptions.min)) {
			$(idSliderDays).slider('setValue', $(this).val());
			updateCostOrderType1(this);
		}
	});
	
	$('.datepicker').datepicker({language: app_settings.language, autoclose: true, startDate: '+7d'});
	//--------- /days slider & input -----------
	
	//--------- tariff buttons -----------
	function updateTariffs() {
		var tariffs = getData($(['#', orderType1Name].join('')).serialize(), app_settings.url.gettariffs);
		
		if (tariffs.success) {
			var tariffIdField	= $(['#', orderType1Name, ' [name="OrderType[tariff_id]"]'].join(''));
			var tariffsBlock	= ['#', orderType1Name, '_tariffs'].join('');
			$(tariffsBlock).html('');
			
			$.each(tariffs.items, function(index, tariff) {
				var disabled	= tariff.disabled == true ? 'disabled="disabled"' : '';
				var html		= '<button ' + disabled + ' type="button" class="btn btn-default" tariff-id="' + tariff.id + '">' + tariff.name + '</button>';
				var htmlEvent	= $(html).click(function() {
					var tariffId = $(this).attr('tariff-id');
					if ((tariffId == tariff.id) && !disabled)
						tariffIdField.val(tariff.id).change();
					else
						tariffIdField.val('').change();
				});

				$(tariffsBlock).append(htmlEvent);
			});
		}
	}
	
	$(['#', orderType1Name, ' [name="OrderType[tariff_id]"]'].join('')).change(function() {
		var self = this;
		$.each($(['#', orderType1Name, '_tariffs button'].join('')), function(index, el) {
			if ($(el).attr('tariff-id') == $(self).val())
				$(el).addClass('active');
			else
				$(el).removeClass('active');
		});
		
		updateCostOrderType1(this);
	});
	//--------- /tariff buttons -----------
	
	//--------- equipment -----------
	$(['#', orderType1Name, ' [name="OrderType[equipment_id]"]'].join('')).change(function() {
		var equipment = getData(null, app_settings.url.getequipment);//getEquipment();
		var equipmentBlock = ['#', orderType1Name, 'Equipment'].join('');

		//console.info(this);
		if (equipment.success) {
			$(equipmentBlock).find('.equipment-item-name').html(equipment.name);
			$(equipmentBlock).find('.equipment-item-delete').attr('item-id', equipment.id).show();
			$(equipmentBlock).find('.equipment-item').show();
			$(equipmentBlock).find('.equipment-item').siblings().hide();
		} else {
			$(equipmentBlock).find('.equipment-add').show();
			$(equipmentBlock).find('.equipment-add').siblings().hide();
		}
		updateCostOrderType1(this);
	});
	
	$('.equipment-item-delete').click(function() {
		var id = $(this).attr('item-id');
		//var result = deleteEquipment(id);
		var result = setData({id: id}, app_settings.url.deleteequipment);
		if (result.success) {
			$(['#', orderType1Name, ' [name="OrderType[equipment_id]"]'].join('')).val('');
			$(['#', orderType1Name, ' [name="OrderType[equipment_id]"]'].join('')).trigger('change');
			updateCostOrderType1(this);
		}
		
		return false;
	});
	//--------- equipment -----------
	
	function updateCostOrderType1(el) {
		var form = $(el).closest('form');
		var type = form.find('[name="Orders[type_id]"]').val();
		var cost = getData(form.serialize(), app_settings.url.getcost, {main: 0});
        //console.info(this);
		switch(type) {
			case '1':
				if (cost.main >= 0) {
					$(form).find('.order-cost').html(cost.main);
					$(form).find('.order-cost-mb').html(cost.mb);
				}
				
				if (cost.old > 0)
					$(form).find('.order-cost.s-oldprice').html(cost.old);
				
				if (cost.main === cost.old)
					$(form).find('.s-oldprice').hide();
				else
					$(form).find('.s-oldprice').show();

				if (cost.main > 0)
					$(form).find('.submit-order').attr('disabled', false);
				else
					$(form).find('.submit-order').attr('disabled', true);
			break;
		}
	}
	
	function updateOrderType1() {
		var data = getData(null, app_settings.url.getsessionorder);
		
		if (data.success) {
			if (data.orders && data.orders.type_id == 1) {
				var formOrder = $('#OrderType' + data.orders.type_id);
				
				if (data.order_type) {
				
					var fields = ['equipment_id', 'countries', 'tariff_id', 'days', 'date_activation', 'type_sim_id'];

					$.each(fields, function(key, value){
						var name = $.isArray(data.order_type[value]) ? '[name="OrderType[' + value + '][]"]' : '[name="OrderType[' + value + ']"]';
						var field	= formOrder.find(name);
						field.val(data.order_type[value]);
						field.trigger('change');
					});
				}
			}
		}
		
	}
	
	updateOrderType1();
</script>


 <script>
   $(function () { $("[data-toggle='tooltip']").tooltip(); });
</script>

