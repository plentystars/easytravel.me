<section class="container content-internet-calc  bt-clr-icalc rentgoods" id="section-equipment">
	<h1><?php echo Yii::t('app', 'EQUIPMENT'); ?></h1>
	<div id="carousel-modems" class="carousel slide hidden-xs" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
		<?php
			for($i = 1; $i <= ceil(count($data['items'])/2); $i++) {
				$class = $i == 1 ? 'active' : '';
				echo '<li data-target="#carousel-modems" data-slide-to="' . ($i-1) . '" class="' . $class . '"></li>';
			}
		?>
	  </ol>
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner">
		<?php
			$items = array();
			$i = 1;

			foreach($data['items'] as $item) {
				$items[$i][] = '
				<div class="col-sm-6 col-md-6 rg-body border-r" item-id="' . $item->id . '">
					<img src="' . Yii::app()->request->baseUrl . $item->img . '" class="img-responsive">
					<h4>' . $item->name . '</h4>
					<p>' . Yii::t('app', 'COST') . ': <strong>$' . $item->cost . '</strong> <a href="#" class="equipment-more-info">' . Yii::t('app', 'READ_MORE') . '</a></p>
					<div class="btn-addequip">
						<button type="button" class="btn btn-default addEquipment">' . Yii::t('app', 'ADD_TO_ORDER') . '</button>
					</div>
				</div>';
				
				if (count($items[$i])%2 == 0)
					$i++;
			}
			
			foreach($items as $key => $item) {
				$class = $key == 1 ? 'item active' : 'item';
				echo '<div class="' . $class . '"><div class="row">' . implode($item, '') . '</div></div>';
			}
		?>
	  </div>
	  <!-- Controls --->
	  <a class="left carousel-control" href="#carousel-modems" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-modems" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>
</section>

<script>
    $('#carousel-modems').carousel({interval: false});
	$('.addEquipment').click(function() {
		var itemId = $(this).closest('div[item-id]').attr('item-id');
		addEquipment(itemId);
		
		return false;
	});
	
	function addEquipment(equipment_id) {
		setData({equipment_id: equipment_id}, app_settings.url.addequipment);
		var orderData = getData(null, app_settings.url.getsessionorder);
		
		if (orderData.success) {
			var type_id = orderData.orders.type_id;
			var href;
			
			switch(type_id) {
				case '1':
					href = '#section-keepgo';
				break;
				case '2':
					href = '#section-';
				break;
				default:
					showModalPage('notorder');
					return false;
				break;
			}

			$('#OrderType' + type_id + ' [name="OrderType[equipment_id]"]').val(equipment_id).trigger('change');
			scrollAnimate(undefined, href);
		} else
			showModalPage('notorder');
	}
	
	$('.equipment-more-info').click(function() {
		console.info(this);
		var itemId = $(this).parent().parent().attr('item-id');
		showModalAction('EquipmentInfo', {id: itemId});
		return false;
	});
</script>


	<!----------------------------------------------------------------------------------------------------------------------
	MOBILE Only--------------------------------------------------------------------------------------------------------->
	
	<section class="container content-internet-calc pad40 hidden-lg hidden-md hidden-sm mobile-equip">
	<div class="panel-group" id="mobile-equip">
		<?php foreach($data['items'] as $item): ?>
		<div class="panel panel-default">
			<div class="panel-heading">
			  <h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#mobile-equip" href="#mobile-e<?php echo $item->id; ?>">
				 <?php echo $item->name; ?>
				</a>
			  </h4>
			</div>
			<div id="mobile-e<?php echo $item->id; ?>" class="panel-collapse collapse">
			  <div class="panel-body">
				<p>
					<img src="<?php echo Yii::app()->request->baseUrl . $item->img; ?>" class="img-responsive">
				</p>
				<p>Цена: <strong>$<?php echo $item->cost; ?></strong></p>
				<a href="#">Подробнее</a>
			 </div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</section>

<!----------------------------------------------------------------------------------------------------------------------
MOBILE Only---END------------------------------------------------------------------------------------------------------>