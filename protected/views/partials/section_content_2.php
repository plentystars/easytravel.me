<div id="section-content-2"></div>
<section class="container content-internet pad40">
	<?php echo Yii::t('app', 'SECTION_2', array('{path}'=>Yii::app()->request->baseUrl)); ?>
</section>

<script>
	$('.hide-show-content').hide();
	
	$('.hide-show-link').click(function() {
		var objHideShowContent = $(this).closest('div.hide-show').find('.hide-show-content:first');
		
		if (objHideShowContent.is(':hidden'))
			objHideShowContent.show('fast');
		else
			objHideShowContent.hide();

		return false
	});
</script>