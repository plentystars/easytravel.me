<section class="container content-internet et-tariffs pad40 " id="section-tariffs">
	<h1><?php echo Yii::t('app', 'TARIFF_PLANS_SERVICES'); ?></h1>
	
	<div id="carousel-tarifs" class="carousel slide hidden-xs" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#carousel-tarifs" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-tarifs" data-slide-to="1"></li>
			<li data-target="#carousel-tarifs" data-slide-to="2"></li>
		</ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<div class="item active">
				<div class="row tariff-service-menu">
					<?php if (Yii::app()->language == 'ru'): ?>
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default active" item-id="1">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_1_NAME'); ?>
						</button>
					</div>
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default" item-id="2">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_2_NAME'); ?>
						</button>
					</div>
					<?php else: ?>
					<div class="col-sm-12 col-md-12">
						<button type="button" class="btn btn-default active" item-id="2">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_2_NAME'); ?>
						</button>
					</div>
					<?php endif; ?>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-12 tariff-service-content">

						<?php if (Yii::app()->language == 'ru'): ?>
						<div item-content="1" class="active">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_1_DESCRIPTION'); ?>
						</div>
						<div item-content="2">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_2_DESCRIPTION'); ?>
						</div>
						<?php else: ?>
						<div item-content="2" class="active">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_2_DESCRIPTION'); ?>
						</div>
						<?php endif; ?>

						<div item-content="3">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_3_DESCRIPTION'); ?>
						</div>
						<div item-content="4">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_4_DESCRIPTION', array('{path}'=>Yii::app()->request->baseUrl)); ?>
						</div>
					</div>
				</div>
				
				<div class="row tariff-service-menu">
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default" item-id="3">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_3_NAME'); ?>
						</button>
					</div>
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default" item-id="4">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_4_NAME'); ?>
						</button>
					</div>
				</div>
			</div>
			
			<div class="item">
				<div class="row tariff-service-menu">
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default active" item-id="5">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_5_NAME'); ?>
						</button>
					</div>
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default" item-id="6">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_6_NAME'); ?>
						</button>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-12 tariff-service-content">
						<div item-content="5" class="active">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_5_DESCRIPTION'); ?>
						</div>
						<div item-content="6">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_6_DESCRIPTION'); ?>
						</div>
						<div item-content="7">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_7_DESCRIPTION'); ?>
						</div>
						<div item-content="8">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_8_DESCRIPTION'); ?>
						</div>
					</div>
				</div>
				
				<div class="row tariff-service-menu">
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default" item-id="7">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_7_NAME'); ?>
						</button>
					</div>
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default" item-id="8">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_8_NAME'); ?>
						</button>
					</div>
				</div>
			</div>
			
			<div class="item">
				<div class="row tariff-service-menu">
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default active" item-id="9">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_9_NAME'); ?>
						</button>
					</div>
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default" item-id="10">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_10_NAME'); ?>
						</button>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-12 tariff-service-content">
						<div item-content="9" class="active">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_9_DESCRIPTION'); ?>
						</div>
						<div item-content="10">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_10_DESCRIPTION'); ?>
						</div>
						<div item-content="11">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_11_DESCRIPTION'); ?>
						</div>
						<!-- <div item-content="12">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_12_DESCRIPTION'); ?>
						</div> -->
					</div>
				</div>
				
				<div class="row tariff-service-menu">
					<div class="col-sm-6 col-md-6">
						<button type="button" class="btn btn-default" item-id="11">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_11_NAME'); ?>
						</button>
					</div>
					<div class="col-sm-6 col-md-6">
						<!-- <button type="button" class="btn btn-default" item-id="12">
							<?php echo Yii::t('app', 'TARIFF_SERVICE_12_NAME'); ?>
						</button> -->
					</div>
				</div>
			</div>
	  </div>

	  <!-- Controls --->
	  <a class="left carousel-control" href="#carousel-tarifs" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-tarifs" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>
	
</section>

<script>
	var class_active = 'active';

	$('[item-content]').not(['.', class_active].join('')).hide();
	
	$('.tariff-service-menu button').click(function() {
		var itemId			= $(this).attr('item-id');
		var objCarouselItem	= $(this).closest('div.item');
		var objItemContent	= objCarouselItem.find('[item-content="' + itemId + '"]');
		
		objItemContent.show();
		objItemContent.siblings().hide();
		
		$.each(objCarouselItem.find('.tariff-service-menu button'), function(index, el) {
			if ($(el).attr('item-id') == itemId)
				$(el).addClass(class_active);
			else
				$(el).removeClass(class_active);
		});
	});
</script>
	
	<!----------------------------------------------------------------------------------------------------------------------
	MOBILE Only--------------------------------------------------------------------------------------------------------->
	
	<section class="container content-internet et-tariffs pad10 hidden-lg hidden-md hidden-sm">
	<div class="panel-group" id="mobile-tariffs">
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t1">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_1_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t1" class="panel-collapse collapse">
		  <div class="panel-body">
			<?php echo Yii::t('app', 'TARIFF_SERVICE_1_DESCRIPTION'); ?>
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#mobile-t2">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_2_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t2" class="panel-collapse collapse">
		  <div class="panel-body">
			<?php echo Yii::t('app', 'TARIFF_SERVICE_2_DESCRIPTION'); ?>
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t3">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_3_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t3" class="panel-collapse collapse">
		  <div class="panel-body">
			<?php echo Yii::t('app', 'TARIFF_SERVICE_3_DESCRIPTION'); ?>
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t4">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_4_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t4" class="panel-collapse collapse">
		  <div class="panel-body">	
			<?php echo Yii::t('app', 'TARIFF_SERVICE_4_DESCRIPTION', array('{path}'=>Yii::app()->request->baseUrl)); ?>		
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t5">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_5_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t5" class="panel-collapse collapse">
		  <div class="panel-body">
			<?php echo Yii::t('app', 'TARIFF_SERVICE_5_DESCRIPTION'); ?>
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t6">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_6_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t6" class="panel-collapse collapse">
		  <div class="panel-body">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_6_DESCRIPTION'); ?>
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t7">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_7_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t7" class="panel-collapse collapse">
		  <div class="panel-body">
			<?php echo Yii::t('app', 'TARIFF_SERVICE_7_DESCRIPTION'); ?>
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t8">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_8_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t8" class="panel-collapse collapse">
		  <div class="panel-body">
			<?php echo Yii::t('app', 'TARIFF_SERVICE_8_DESCRIPTION'); ?>
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t9">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_9_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t9" class="panel-collapse collapse">
		  <div class="panel-body">
			<?php echo Yii::t('app', 'TARIFF_SERVICE_9_DESCRIPTION'); ?>
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t10">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_10_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t10" class="panel-collapse collapse">
		  <div class="panel-body">
			<?php echo Yii::t('app', 'TARIFF_SERVICE_10_DESCRIPTION'); ?>
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-tariffs" href="#mobile-t11">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_11_NAME'); ?>
			</a>
		  </h4>
		</div>
		<div id="mobile-t11" class="panel-collapse collapse">
		  <div class="panel-body">
				<?php echo Yii::t('app', 'TARIFF_SERVICE_11_DESCRIPTION'); ?>
		 </div>
		</div>
	  </div>

	</div>
</section>

	<!----------------------------------------------------------------------------------------------------------------------
	MOBILE Only---END------------------------------------------------------------------------------------------------------>
<script>
    $('#carousel-tarifs').carousel({interval: false});
</script>