<section class="container bt-clr-carousel" id="section-header">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		<li data-target="#carousel-example-generic" data-slide-to="3"></li>
		<li data-target="#carousel-example-generic" data-slide-to="4"></li>
		<a href="#" class="slider-pause"><i class="fa fa-pause"></i></a>
	  </ol>

	  <!-- Wrapper for slides -->
	  <div class="carousel-inner">
		<div class="item active">
		  <img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/image1.png" alt="...">
		  <div class="carousel-caption">
				<?php echo Yii::t('app', 'SLIDER_1'); ?>
				<!---div class="carousel-go">
						<a class="scrollTo" href="#section-keepgo"><button type="button" class="btn btn-default"><?php echo Yii::t('app', 'GO_TO'); ?></button></a>
					</div--->
			</div>
		</div>
		<div class="item">
		  <img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/image2.png" alt="...">
		  <div class="carousel-caption">
				<?php echo Yii::t('app', 'SLIDER_2'); ?>
				<!---div class="carousel-go">
						<a class="scrollTo" href="#section-content-1"><button type="button" class="btn btn-default"><?php echo Yii::t('app', 'GO_TO'); ?></button></a>
					</div--->
			</div>
		</div>
		<div class="item">
		  <img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/image3.png" alt="...">
		  <div class="carousel-caption">
				<?php echo Yii::t('app', 'SLIDER_3'); ?>
				<!-- <div class="carousel-go">
						<a href="http://easyroaming.ru/" target="_blank"><button type="button" class="btn btn-default"><?php echo Yii::t('app', 'GO_TO'); ?></button></a>
					</div> -->
			</div>
		</div>
		<div class="item">
		  <img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/image4.png" alt="...">
			<div class="carousel_copyright">
				<a class="scrollTo" href="#carousel_copyright">©</a>
			</div>
		  <div class="carousel-caption">
				<?php echo Yii::t('app', 'SLIDER_4'); ?>
						<!---div class="carousel-go">
						<a class="scrollTo" href="#section-content-2"><button type="button" class="btn btn-default"><?php echo Yii::t('app', 'GO_TO'); ?></button></a>
					</div--->
				</div>
		</div>
		<div class="item">
		  <img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/image5.png" alt="...">
		  <div class="carousel-caption">
				<?php echo Yii::t('app', 'SLIDER_5'); ?>
				<!---div class="carousel-go">
						<a class="scrollTo" href="#section-forpartners"><button type="button" class="btn btn-default"><?php echo Yii::t('app', 'GO_TO'); ?></button></a>
					</div--->
			</div>
		</div>
	  </div>

	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>
</section>
<script>
	$('.slider-pause').click(function() {
		$(this).parent().parent().carousel('pause');
		return false;
	});
</script>
