
<nav class="navbar navbar-default bt-clr-nav" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		<span class="sr-only">Навигация</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

  <div class="container">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	  <ul class="nav navbar-nav">
		<li><a href="#section-keepgo"><?php echo Yii::t('app', 'MOBILE_INTERNET'); ?></a></li>
		<li class="hidden-xs"><span>|</span></li>
		<li><a href="#section-universal"><?php echo Yii::t('app', 'MOBILE_CALLS'); ?></a></li>
		<li class="hidden-xs"><span>|</span></li>
		<li><a href="#section-equipment"><?php echo Yii::t('app', 'EQUIPMENT'); ?></a></li>
		<li class="hidden-xs"><span>|</span></li>
		<li><a href="#section-addcash"><?php echo Yii::t('app', 'ADD_CASH'); ?></a></li>
		<li class="hidden-xs"><span>|</span></li>
		<li><a href="#" modal-action="CorporateClient"><?php echo Yii::t('app', 'FOR_CORPORATE_CLIENTS'); ?></a></li>
	  </ul>
	  <ul class="nav navbar-nav navbar-right">
		<li><a href="https://siteheart.com/webconsultation/629677" target="_blank"><i class="fa fa-question-circle" style="padding-right: 3px;"></i> <?php echo Yii::t('app', 'ASK_QUESTION'); ?></a></li>
		<?php if (Yii::app()->user->isGuest): ?>
		<li class="enter"><a href="#" modal-action="login"><?php echo Yii::t('app', 'LOGIN'); ?></a></li>
		<?php else: ?>
		<li class="enter"><a href="/user/profile"><?php echo Yii::app()->user->name; ?></a></li>
		<?php endif; ?>
	  </ul>
	</div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

</div></div><!-- /.fixheadertop .container --- start in header.php -->

<script>
	$('#bs-example-navbar-collapse-1 li a').click(function() {
		/*$(this).parent()
			.addClass('active')
			.siblings().removeClass('active');*/
		if (!$(this).attr('modal-action') && !$(this).attr('modal-page') && ($(this).attr('href').indexOf('#') !== -1))
			scrollAnimate(this);
	});
</script>