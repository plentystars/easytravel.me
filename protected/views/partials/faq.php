<?php
	$faq_settings = array(
		1	=> array(
			'button'	=> array(
				'top_text'		=> Yii::t('app', 'SIM_CARD'),
				'bottom_text'	=> 'easyroaming'
			),
			'category' 	=> Yii::t('app', 'SIM_CARD') . ' easyroaming',
			'class'		=> 'faq-371'
		),
		2	=> array(
			'button'	=> array(
				'top_text'		=> '&nbsp;',
				'bottom_text'	=> Yii::t('app', 'SERVICE')
			),
			'category'	=> Yii::t('app', 'SERVICE'),
			'class'		=> 'faq-ser'
		),
		3	=> array(
			'button'	=> array(
				'top_text'		=> Yii::t('app', 'SIM_CARD'),
				'bottom_text'	=> 'Keepgo'
			),
			'category'	=> Yii::t('app', 'SIM_CARD') . ' Keepgo',
			'class'		=> 'faq-keepgo'
		)
	);
?>

<section class="container content-internet faq pad40">
	<h1><?php echo Yii::t('app', 'FAQ'); ?></h1>
	<div class="row">
		<?php foreach($faq_settings as $key => $faq_item): ?>
		<a href="#" tab-item="<?php echo $key; ?>">
			<div class="col-sm-4 col-md-4 <?php echo $faq_item['class']; ?>">
				<p><?php echo $faq_item['button']['top_text']; ?></p>
				<span><?php echo $faq_item['button']['bottom_text']; ?></span>
			</div>
		</a>
		<?php endforeach; ?>
	</div>
</section>

<?php
	$sections = array();
	foreach($data['items'] as $item) {
		$class_in = isset($sections[$item->category]) ? '' : ' in';
		
		$sections[$item->category][] = '
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion' . $item->category . '" href="#collapse' . $item->id . '">' . $item->question . '</a>
					</h4>
				</div>
				<div id="collapse' . $item->id . '" class="panel-collapse collapse' . $class_in . '">
					<div class="panel-body">' . $item->answer . '</div>
				</div>
			</div>
		';
	}
?>

<?php foreach($sections as $category => $section): ?>
<section class="container content-internet-calc faq-akkord pad40" tab-item-content="<?php echo $category; ?>">
	<h1><?php echo $faq_settings[$category]['category']; ?></h1>
	<div class="panel-group" id="accordion<?php echo $category; ?>">
	<?php foreach($section as $item): ?>
		<?php echo $item; ?>
	<?php endforeach; ?>
	</div>
</section>
<?php endforeach; ?>

<script>
	$('[tab-item-content]').hide();
	
	$('[tab-item]').click(function() {
		var itemId = $(this).attr('tab-item');

		$.each($('[tab-item-content]'), function(index, el) {
			var itemContentId = $(el).attr('tab-item-content');
			if (itemContentId == itemId)
				$(el).show('slow');
			else
				$(el).hide();
		});

		return false;
	});
</script>



<?php if(false): ?>
<!--------------Перенести в ЛК ---------------->




<section class="container content-internet faq pad40 profile">
	<h1>Профиль</h1>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3 lk-data">Имя</div>
		<div class="col-sm-3">Alex</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3 lk-data">Фамилия</div>
		<div class="col-sm-3">Zay</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3 lk-data">Электронная почта</div>
		<div class="col-sm-3">newntel@gmail.com</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3 lk-data">Дата регистрации</div>
		<div class="col-sm-3">2013-07-24 08:26:07</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3 lk-data">Статус</div>
		<div class="col-sm-3">Активирован</div>
		<div class="col-sm-3"></div>
	</div>
</section>

<section class="container content-internet faq pad40 profile">
	<h1>Профиль</h1>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="form-group">
			<label for="lk-name" class="col-sm-3 easy-form-label">Имя *</label>
			<div class="col-sm-3 ">
				<input type="text" class="form-control easy-form-input" id="lk-name" placeholder="Alex">
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="form-group">
			<label for="lk-fam" class="col-sm-3 easy-form-label">Фамилия *</label>
			<div class="col-sm-3 ">
				<input type="text" class="form-control easy-form-input" id="lk-fam" placeholder="Zay">
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			* Обязательные поля.
		</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<div class="addcashbtn">
				<button type="button" class="btn btn-default">Сохранить</button>
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</section>


<section class="container content-internet faq pad40 profile">
	<h1>История заказов</h1>
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-2">Дата</div>
		<div class="col-sm-2">Заказ</div>
		<div class="col-sm-2">Сумма</div>
		<div class="col-sm-2">Бонус</div>
		<div class="col-sm-2">Статус</div>
		<div class="col-sm-1"></div>
	</div>
	
	
	
		<div class="panel-group" id="accordion-lk">
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion-lk" href="#lk1">
			  	<div class="row">
					<div class="col-sm-1"></div>
					<div class="col-sm-2">01.09.2014</div>
					<div class="col-sm-2">Андорра</div>
					<div class="col-sm-2">$1285.44	</div>
					<div class="col-sm-2"></div>
					<div class="col-sm-2">В обработке</div>
					<div class="col-sm-1"></div>
				</div>
			</a>
		  </h4>
		</div>
		<div id="lk1" class="panel-collapse collapse">
		  <div class="panel-body">
			<div class="row">
				<div class="col-md-12 order-info">
					<div class="icalc-simtype">
							<p class="caption">Сим-карта: <a href="#">easyroaming</a></p>
							<p class="caption">Страна: <a href="#">Италия</a></p>
							<p class="caption">Дата активации карты: <a href="#">15.09.2014</a></p>
							<p class="caption">Тарифный план: <a href="#">100 Мб/сутки</a></p>
							<p class="caption">Тип сим-карты: <a href="#">nanoSIM</a></p>
							<p class="caption">Промокод: <a href="#">-7%</a></p>
							<div class="mcalc-final">
								<div class="row">
									<div class="col-sm-4">
										Сумма заказа:
									</div>
									<div class="col-sm-8 mcalc-sum">
										1285.44 $ / 48847 руб.
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion-lk" href="#lk2">
				<div class="row">
					<div class="col-sm-1"></div>
					<div class="col-sm-2">01.09.2014</div>
					<div class="col-sm-2">Андорра</div>
					<div class="col-sm-2">$1285.44	</div>
					<div class="col-sm-2"></div>
					<div class="col-sm-2">В обработке</div>
					<div class="col-sm-1"></div>
				</div>
			</a>
		  </h4>
		</div>
		<div id="lk2" class="panel-collapse collapse">
		  <div class="panel-body">
							<div class="row">
				<div class="col-md-12 order-info">
					<div class="icalc-simtype">
							<p class="caption">Сим-карта: <a href="#">easyroaming</a></p>
							<p class="caption">Страна: <a href="#">Италия</a></p>
							<p class="caption">Дата активации карты: <a href="#">15.09.2014</a></p>
							<p class="caption">Тарифный план: <a href="#">100 Мб/сутки</a></p>
							<p class="caption">Тип сим-карты: <a href="#">nanoSIM</a></p>
							<p class="caption">Промокод: <a href="#">-7%</a></p>
							<div class="mcalc-final">
								<div class="row">
									<div class="col-sm-4">
										Сумма заказа:
									</div>
									<div class="col-sm-8 mcalc-sum">
										1285.44 $ / 48847 руб.
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		  </div>
		</div>
	  </div>

	</div>
					
</section>
<?php endif; ?>