<?php if(false): ?>
<section class="container content-internet video-i pad40">
	<h1>Видео-инструкции</h1>
	<div id="carousel-instr" class="carousel slide hidden-xs" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
		<li data-target="#carousel-instr" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-instr" data-slide-to="1"></li>
		<li data-target="#carousel-instr" data-slide-to="2"></li>
	  </ol>
	 <div style="padding: 5px 55px 5px 50px;">
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner">
		<div class="item active">
			<div class="row ptitle">
				<div class="col-sm-4 col-md-4">
					<p>
						Как активировать услугу <strong>Call Back</strong>
					</p>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
				</div>
				<div class="col-sm-4 col-md-4">
					<p>
						Как звонить с сим-карты <strong>Еasyroaming</strong>
					</p>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
				</div>
				<div class="col-sm-4 col-md-4">
					<p>
						Проверка баланса
					</p>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
				</div>
			</div>
		</div>
		<div class="item">
			<div class="row ptitle">
				<div class="col-sm-4 col-md-4">
					<p>
						Как активировать услугу <strong>Call Back</strong>
					</p>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
				</div>
				<div class="col-sm-4 col-md-4">
					<p>
						Как звонить с сим-карты <strong>Еasyroaming</strong>
					</p>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
				</div>
				<div class="col-sm-4 col-md-4">
					<p>
						Проверка баланса
					</p>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
				</div>
			</div>
		</div>
		<div class="item">
			<div class="row ptitle">
				<div class="col-sm-4 col-md-4">
					<p>
						Как активировать услугу <strong>Call Back</strong>
					</p>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
				</div>
				<div class="col-sm-4 col-md-4">
					<p>
						Как звонить с сим-карты <strong>Еasyroaming</strong>
					</p>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
				</div>
				<div class="col-sm-4 col-md-4">
					<p>
						Проверка баланса
					</p>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
				</div>
			</div>
		</div>

	  </div>
	</div> <!--paddings div-->

	  <!-- Controls --->
	  <a class="left carousel-control" href="#carousel-instr" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-instr" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>
</section>



	<!----------------------------------------------------------------------------------------------------------------------
	MOBILE Only--------------------------------------------------------------------------------------------------------->
	
	<section class="container content-internet pad10 hidden-lg hidden-md hidden-sm mobile-equip">
	<div class="panel-group" id="mobile-videoinstr">
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-videoinstr" href="#mobile-v1">
			Как активировать услугу <strong>Call Back</strong>
			</a>
		  </h4>
		</div>
		<div id="mobile-v1" class="panel-collapse collapse">
		  <div class="panel-body">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#mobile-v2">
			 Как звонить с сим-карты <strong>Еasyroaming</strong>
			</a>
		  </h4>
		</div>
		<div id="mobile-v2" class="panel-collapse collapse">
		  <div class="panel-body">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-equip" href="#mobile-v3">
			 Проверка баланса
			</a>
		  </h4>
		</div>
		<div id="mobile-v3" class="panel-collapse collapse">
		  <div class="panel-body">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#mobile-v4">
				Как активировать услугу <strong>Call Back</strong>
			</a>
		  </h4>
		</div>
		<div id="mobile-v4" class="panel-collapse collapse">
		  <div class="panel-body">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
		  </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#mobile-equip" href="#mobile-v5">
			Как звонить с сим-карты <strong>Еasyroaming</strong>
			</a>
		  </h4>
		</div>
		<div id="mobile-v5" class="panel-collapse collapse">
		  <div class="panel-body">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
		 </div>
		</div>
	  </div>
	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#mobile-v6">
			 Проверка баланса
			</a>
		  </h4>
		</div>
		<div id="mobile-v6" class="panel-collapse collapse">
		  <div class="panel-body">
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/video-preview.png">
		  </div>
		</div>
	  </div>

	</div>
</section>

	<!----------------------------------------------------------------------------------------------------------------------
	MOBILE Only---END------------------------------------------------------------------------------------------------------>
<?php endif; ?>