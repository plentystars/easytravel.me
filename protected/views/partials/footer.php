<footer class="container pad40">
	<div class="row">
		<div class="col-sm-4 col-md-4">
			<ul class="list-unstyled">
				<li><a href="#" modal-page="news"><?php echo Yii::t('app', 'NEWS'); ?></a></li>
				<li><a href="#" modal-page="about"><?php echo Yii::t('app', 'ABOUT_COMPANY'); ?></a></li>
				<li><a class="scrollTo" href="#section-forpartners"><?php echo Yii::t('app', 'ITEM_MENU_FOR_PARTNERS'); ?></a></li>
				<!--li><a href="#" modal-page="corporate"><?php echo Yii::t('app', 'FOR_CORPORATE_CLIENTS'); ?></a></li--->
				<li><a href="#" modal-action="tariffsbycountry"><?php echo Yii::t('app', 'TARIFFS_BY_COUNTRY'); ?></a></li>
				<!-- <li><a href="#"><?php echo Yii::t('app', 'INSTRUCTIONS'); ?></a></li> -->
				<li><a href="#"  modal-page="payments"><?php echo Yii::t('app', 'PAYMENTS'); ?></a></li>
				<li><a href="#"  modal-page="delivery"><?php echo Yii::t('app', 'DELIVERY'); ?></a></li>
				<!--li><a href="#"  modal-page="webcall"><?php echo Yii::t('app', 'WEBCALL'); ?></a></li-->
				<li><a href="#"  modal-page="sms_371"><?php echo Yii::t('app', 'SEND_SMS'); ?></a></li>
				<li><a href="#" modal-action="voip"><?php echo Yii::t('app', 'VOIP'); ?></a></li>
				<li><a href="#" modal-page="legal_info"><?php echo Yii::t('app', 'LEGAL_INFO_TITLE'); ?></a></li>
				<li><a href="#" modal-page="horoshie_dela"><?php echo Yii::t('app', 'GOOD_WORK'); ?></a></li>
			</ul>
		</div>
		<div class="col-sm-4 col-md-4">

		</div>
		<div class="col-sm-4 col-md-4">
			<h4><?php echo Yii::t('app', 'FOLLOW_US'); ?></h4>
			<div id="fllow2">
				<a href="https://www.facebook.com/easyroaming" target="_blank"><div class="follow-fb"></div></a>
				<a href="https://vk.com/easyroaming"  target="_blank"><div class="follow-vk"></div></a>
				<a href="https://plus.google.com/111180215227564225972" title="Google+"  target="_blank"><div class="follow-g"></div></a>
				<a href="http://instagram.com/easytravel.me"  target="_blank"><div class="follow-insta"></div></a>
				<a href="https://www.youtube.com/channel/UCr36mMacV7vBiIopkWhv_6Q"  target="_blank"><div class="follow-yt"></div></a>
			</div>
			<div style="margin-top: 90px;">
				 <!-- Yandex.Metrika informer -->
				<a href="http://metrika.yandex.ru/stat/?id=22557637&amp;from=informer" target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/22557637/3_0_777992FF_777992FF_1_pageviews"
				style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:22557637,lang:'ru'});return false}catch(e){}"/></a>
				<!-- /Yandex.Metrika informer -->
				<!--LiveInternet counter--><script type="text/javascript"><!--
				document.write("<a href='http://www.liveinternet.ru/click' "+
				"target=_blank><img src='//counter.yadro.ru/hit?t41.10;r"+
				escape(document.referrer)+((typeof(screen)=="undefined")?"":
				";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
				screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
				";"+Math.random()+
				"' alt='' title='LiveInternet' "+
				"border='0' width='31' height='31'><\/a>")
				//--></script><!--/LiveInternet-->
			</div>
			<div class="footer_copyright" id="carousel_copyright">
				При создании сайта использовались фотографии <a href="http://www.shutterstock.com/gallery-695464p1.html?pl=edit-00&cr=00">© Denys Prykhodov</a> / <a href="http://www.shutterstock.com/?pl=edit-00&cr=00">Shutterstock.com</a>
			</div>	
			<div class="footer-trevi">
				<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/trevi.png" class="img-responsive">
				<p>Easytravel.me <?php echo date('Y'); ?></p>
			</div>
		</div>
	</div>
</footer>
<script>
	$('[modal-page]').click(function() {
		showModalPage($(this).attr('modal-page'));
		return false;
	});
	$('[modal-action]').click(function() {
		showModalAction($(this).attr('modal-action'));
		return false;
	});
</script>


<div class="back-to-top" style="display: block;"><a class="scrollTo" href="#section-header"><i class="fa fa-angle-up"></i></a></div>
