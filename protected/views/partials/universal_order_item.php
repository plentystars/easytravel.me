<div class="order-item" style="margin-top: -25px;">
	<input type="hidden" name="OrderType[equipment_id][]" />
	<input type="hidden" name="OrderType[tariff_sim_id][]" value="2" />
	<div class="container-sm-height">	
		<div class="row row-sm-height">
			<div class="col-sm-6 mcalc-balance col-sm-height">
				<?php echo Yii::t('app', 'STARTING_BALANCE'); ?>
				<p>
					<div class="btn-group OrderType2_balansId">
						  <button type="button" class="btn btn-default active" item-id="1">20 $</button>
						  <button type="button" class="btn btn-default" item-id="2">50 $</button>
						  <input type="hidden" name="OrderType[balans_id][]" value="1"/>
					</div>
				</p>
			</div>
			<div class="col-sm-6 mcalc-addbalance col-sm-height">
				<div class="form-group">
					<label for="promocode"><?php echo Yii::t('app', 'ADD_BALANCE'); ?></label>
						<div class="input-group">
						  <input type="text" name="OrderType[add_balans][]" class="form-control">
						  <div class="input-group-addon">$</div>
						</div>
				 </div>
			</div>
		</div>
	</div>
	<div class="icalc-simtype">
		<div class="form-group">
			<label for="simtype"><?php echo Yii::t('app', 'ORDER_TYPE_SIM'); ?></label>
			<select name="OrderType[type_sim_id][]" class="form-control">
			<?php foreach($data['type_sim'] as $item): ?>
				<option value="<?php echo $item->value; ?>"><?php echo $item->name; ?></option>
			<?php endforeach; ?>
			</select>
		  </div>
	</div>
</div>