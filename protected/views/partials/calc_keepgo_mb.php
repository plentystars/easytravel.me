<div class="col-md-6 icalc-right col-sm-height">
		<div class="icalc-slider-1">
			<p class="caption"><?php echo Yii::t('app', 'KEEPGO_MB_DESCRIPTION'); ?></p>
			<p>Skype <span> <?php echo Yii::t('app', 'MINUTES_DAY'); ?><span id="icalc-sl3-val" class="icalc-slider-1-val">156</span></span></p>
			<input name="skype" id="icalc-sl3" type="text" data-slider-min="0" data-slider-max="200" data-slider-step="1" data-slider-value="156">
	  </div>
		<div class="icalc-slider-1">
			<p>Email <span> <?php echo Yii::t('app', 'EMAILS_DAY'); ?><span id="icalc-sl4-val" class="icalc-slider-1-val">20</span></span></p>
			<input name="email" id="icalc-sl4" type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="20">
	  </div>
		<div class="icalc-slider-1">
			<p><?php echo Yii::t('app', 'NAVIGATOR'); ?> <span> <?php echo Yii::t('app', 'HOURS_DAY'); ?><span id="icalc-sl5-val" class="icalc-slider-1-val">2</span></span></p>
			<input name="navigator" id="icalc-sl5" type="text" data-slider-min="0" data-slider-max="5" data-slider-step="1" data-slider-value="2">
	  </div>
		<div class="icalc-slider-1">
			<p><?php echo Yii::t('app', 'SOCIAL_NETWORKS'); ?> <span> <?php echo Yii::t('app', 'MINUTES_DAY'); ?><span id="icalc-sl6-val" class="icalc-slider-1-val">100</span></span></p>
			<input name="social" id="icalc-sl6" type="text" data-slider-min="0" data-slider-max="200" data-slider-step="1" data-slider-value="100">
	  </div>
	  <p class="caption"><?php echo Yii::t('app', 'TRAFFIC_CAPTION_NONTEMPLATE'); ?></p>
	  <div class="icalc-full bottom">
		<p><?php echo Yii::t('app', 'TRAFFIC_DAY'); ?>:</p>
		<span><span id="result-mb">500</span> <?php echo Yii::t('app', 'MB'); ?></span>
	   </div>
</div>

<script>
	var cat_values			= {};

		cat_values.skype		= 1.5;
		cat_values.email		= 0.25;
		cat_values.navigator	= 20;
		cat_values.social		= 0.6;
	
	$('#icalc-sl3, #icalc-sl4, #icalc-sl5, #icalc-sl6').slider();
	$('#icalc-sl3, #icalc-sl4, #icalc-sl5, #icalc-sl6').on('slide', function(slideEvt) {
		var id = $(this).attr('id');
		$('#' + id + '-val').text(slideEvt.value);
		$(this).attr('data-slider-value', slideEvt.value);
		updateQuantityMb();
	});
	
	updateQuantityMb();
	
	function updateQuantityMb() {
		var result = 0;
		$.each($('.icalc-right input'), function(index, el) {
			var name		= $(el).attr('name');
			var quantity	= $(el).attr('data-slider-value');
			var value		= cat_values[name];

			if (value)
				result += value * quantity;
				
		});
		
		$('#result-mb').html(result.toFixed(2));
	}
</script>