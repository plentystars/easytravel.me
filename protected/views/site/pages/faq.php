<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>

<div class="container">
	<h2>Популярные вопросы</h2>
	<?php $this->renderPartial('_faq'); ?>
</div>
