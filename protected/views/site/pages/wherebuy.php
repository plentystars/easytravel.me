<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>


<div class="container">
	<div class="row padtop60" style="padding-bottom: 60px">
		<div class="span6"> 
			<h4>Интернет-магазин «Легкий роуминг» доставляет сим-карты в любой город  России и Украины.</h4>
			<p>Для заказа наших продуктов вы можете обратиться в наш контакт-центр по телефонам:</p>

			<ul class="marker">
				<li><strong>Москва</strong> +7 (495) 212-90-06</li>
				<li><strong>Санкт-Петербург</strong> +7 (812) 748-23-00</li>
				<li><strong>Нижний Новгород</strong> +7 (831) 280-83-11</li>
				<li><strong>Ростов-на-Дону</strong> +7 (863) 303-30-11</li>
				<li><strong>Самара</strong> +7 (846) 212-95-12</li>
			</ul>
		</div>
		<div class="span6"> 
			<img style="padding: 20px 0 0 0 " src="<?php echo $pathToImg; ?>/3-3.png">
		</div>
	</div>


</div>
