<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>
<div class="container" style="padding: 60px 0 80px 0">
	<div class="row">
		<div class="span12">
		
			<?php 
				$msNews = News::model()->findAll(array('order'=>'datetime DESC'));
				foreach($msNews as $mNews):
			?>
				<div>
					<h4><span class="label label-success"><?php echo Yii::app()->dateFormatter->format("dd-MM-yyyy", $mNews->datetime); ?></span> <?php echo $mNews->title; ?></h4>
					<?php echo $mNews->description; ?>
				</div>
				<hr>
			<?php endforeach; ?>				
		</div>
	</div>		
</div>