<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>

	
<div class="container" >
	<div class="row" >
		<div class="span2"></div>
		<div class="span8" style="padding: 50px 0 60px 0">
			<h2>Юридическая информация</h2>
			<table class="table table-bordered">
				<tr class="success"><td>Название магазина</td><td>Лёгкий Роуминг (EasyRoaming.ru)</td></tr>
				<tr class="success"><td>ИП</td><td>Койло Александр Эмильевич</td></tr>
				<tr><td>Юридический адрес</td><td>140083, Московская область, г. Лыткарино , квартал 2, д. 15</td></tr>
				<tr><td>Почтовый и фактический адрес</td><td>140083, Московская область, г. Лыткарино , квартал 2, д. 15</td></tr>
				<tr><td>ИНН</td><td>503501082740</td></tr>
				<tr><td>ОГРН</td><td>503501082740</td></tr>
				<tr><td>Наименование банка</td><td>ОАО КБ «Агропромкредит» г. Москва</td></tr>
				<tr><td>Расчетный счет</td><td>40802810206000000168</td></tr>
				<tr><td>Корреспондентский счет</td><td>30101810400000000710</td></tr>
				<tr><td>БИК</td><td>044579710</td></tr>
				<tr><td>Сайт</td><td>www.EasyRoaming.ru</td></tr>
				<tr><td>Телефон</td><td>+7 (495) 212-9006</td></tr>
				<tr><td>Email</td><td>assist@easyroaming.ru</td></tr>
			</table>
		</div>
		<div class="span2"></div>
	</div>

</div>
