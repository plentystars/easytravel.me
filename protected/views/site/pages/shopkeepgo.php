<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>


<style>
.product_header {
font-size: 20px;
font-family: Arial, Helvetica, sans-serif;
color: #fff;
text-align: center;
font-weight: 700;
padding: 5px 0px 0px 15px;
}
</style>


<div class="container" style="padding: 20px 0 80px 0">
	<div class="row padtop60">
		<div class="span6">
				<div style="background: #72cfd6; padding: 20px; background-image: url(http://easytravel.me/resources/img/sample/map-europe.png); background-position: right bottom"> 
				<div class="row-fluid">
				<div class="span8" >
				<div class="product_header">SIM-карта Gb2Go Europe</div>
				</div>
				<div class="span4" > 
					<div class="input-append">
					  <input class="span3"  type="text" placeholder="" value="1">
					  						  <div class="btn-group">
							<button class="btn dropdown-toggle" data-toggle="dropdown">
							  <span >шт.</span>
							</button>
						  </div>
						</div>
	
				</div>
			</div>	
			<div class="row-fluid">
				<div class="span12" >
					<div class="line-hr">
					</div>				
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="calc-text" style="font-weight: bold;">
					36 стран Евросоюза<br/>
					1Гб трафика<br/>
					Русскоязычная техподдержка<br/>
					Выгодная тарификация - округление сессий до 1 Кб
					</div>
				</div>
			</div>	
			<div class="row-fluid">
				<div class="span12" >
					<div class="line-hr">
					</div>				
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					  <button class="btn btn-large btn-warning span12" type="submit" >Заказать за 3249 руб.</button>
				</div>
			</div>	
		</div>
		<a style="text-align: center; padding: 20px 0 0 0; cursor: pointer;" id="show_countrylist">Посмотреть список стран</a>		
			<div id="countrylist" style="display: none; padding: 20px 0">
				Австрия, Азорские острова, Бельгия, Болгария, Великобритания, Ватикан, Венгрия, Гальдар/Гран-Канария, Германия, Гибралтар, Греция, Дания, Ирландия, Испания, Италия, Кипр,  Латвия, Лихтенштейн, Литва,  Люксембург, Мадейра, Мальта, Монако, Нидерланды, Норвегия, Польша,  Португалия,  Румыния, Сан-Марино, Словакия,  Словения, Финляндия, Франция, Чешская Республика, Швеция,  Швейцария, Эстония
			</div>
		</div>
		<div class="span6"> 
			<h4>SIM-карта "Гигабайт в дорогу (Gb2Go)":</h4>
			<ul>
				<li>Позволяет получить доступ в интернет с помощью любого мобильного устройства, имеющего эту функцию: смартфоны, планшетные компьютеры, модемы, роутеры, камерофоны  т.д.</li>
				<li>Сим-карта обеспечивает 1 Гигабайт  мобильного интернет-трафика на максимальной скорости в сети 3G/2G на территории Евросоюза (34 страны и 3 автономных региона)</li>
				<li>Стоимость: 0,09 $/ 1 Мб</li>
				<li>Округление сессии с точностью до 1 Kb</li>
				<li>Карта действительна в течение  30 дней с  момента первого выхода в сеть, в «спящем режиме»  (не активированная)  –  бессрочный период</li>
				<li>После регистрации карты на сайте компании Вам станет доступен контроль трафика в режиме  он-лайн</li>
				<li>Инструкция по активации карты – в упаковке</li>
				<li>По окончанию срока действия карты или трафика интернет-карта аннулируется</li>
			</ul>
		</div>		
</div>

	<div class="row padtop60">
		<div class="span6">
			<h4>SIM-карта "Гигабайт в дорогу Украина":</h4>
			<ul>
				<li>Позволяет получить доступ в интернет с помощью любого мобильного устройства, имеющего эту функцию: смартфоны, планшетные компьютеры, модемы, роутеры, камерофоны  т.д.</li>
				<li>Сим-карта обеспечивает 13 Гигабайт  мобильного интернет-трафика на максимальной скорости в сети 3G/2G на территории Украины</li>
				<li>Стоимость: 0.006 $/ 1 Мб</li>
				<li>Округление сессии с точностью до 10 Kb</li>
				<li>Карта действительна в течение  1 года с  момента первого выхода в сеть, в «спящем режиме»  (не активированная)  –бессрочный период</li>
				<li>После регистрации карты на сайте компании Вам станет доступен контроль трафика в режиме  он-лайн</li>
				<li>Инструкция по активации карты – в упаковке </li>
				<li>По окончании срока действия карты или трафика интернет-карта аннулируется</li>
			</ul>
		</div>
		<div class="span6"> 
				<div style="background: #72cfd6; padding: 20px; background-image: url(http://easytravel.me/resources/img/sample/map-ukraine.png); background-position: right bottom; "> 
				<div class="row-fluid">
				<div class="span8" >
				<div class="product_header">SIM-карта Gb2Go Ukraine</div>
				</div>
				<div class="span4" > 
					<div class="input-append">
					  <input class="span3"  type="text" placeholder="" value="1">
					  						  <div class="btn-group">
							<button class="btn dropdown-toggle" data-toggle="dropdown">
							  <span >шт.</span>
							</button>
						  </div>
						</div>
	
				</div>
			</div>	
			<div class="row-fluid">
				<div class="span12" >
					<div class="line-hr">
					</div>				
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="calc-text">
					Вся Украина <br/>
					13Гб трафика<br/>
					Русскоязычная техподдержка<br/>
					Выгодная тарификация - округление сессий до 1 Кб
					</div>
				</div>
			</div>	
			<div class="row-fluid">
				<div class="span12" >
					<div class="line-hr">
					</div>				
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					  <button class="btn btn-large btn-warning span12" type="submit" >Заказать за 2699 руб.</button>
				</div>
			</div>	
		</div>
		</div>		
</div>

	<div class="row padtop60">
		<div class="span6">
	<div style="background: #72cfd6; padding: 20px; background-image: url(http://easytravel.me/resources/img/sample/map-europe-america.png); background-position: right bottom" > 
				<div class="row-fluid">
				<div class="span8" >
				<div class="product_header">SIM-карта Gb2Go Europe+America</div>
				</div>
				<div class="span4" > 
					<div class="input-append">
					  <input class="span3"  type="text" placeholder="" value="1">
					  						  <div class="btn-group">
							<button class="btn dropdown-toggle" data-toggle="dropdown">
							  <span >шт.</span>
							</button>
						  </div>
						</div>
	
				</div>
			</div>	
			<div class="row-fluid">
				<div class="span12" >
					<div class="line-hr">
					</div>				
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="calc-text">
					Европа + Америка<br/>
					1Гб трафика<br/>
					Русскоязычная техподдержка<br/>
					Выгодная тарификация - округление сессий до 1 Кб
					</div>
				</div>
			</div>	
			<div class="row-fluid">
				<div class="span12" >
					<div class="line-hr">
					</div>				
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					  <button class="btn btn-large btn-warning span12" type="submit" >Заказать за 4949 руб.</button>
				</div>
			</div>	
		</div>	
		<a style="text-align: center; padding: 20px 0 0 0; cursor: pointer;" id="show_countrylist_am">Посмотреть список стран</a>		
			<div id="countrylist_am" style="display: none; padding: 20px 0">
				Аргентина, Австрия, Азорские о-ва, Бельгия, Бразилия, Болгария ,Великобритания, Венгрия, Ватикан, Венесуэла, Гальдар/Гран-Канария, Германия, Гибралтар, Греция, Гватемала, Дания, Ирландия, Италия, Испания, Канада, Колумбия, Коста Рика, Кипр, Латвия, Лихтенштейн, Литва, Люксембург, Мадейра, Мальта, Мексика, Монако, Нидерланды, Никарагуа, Норвегия, Панама, Перу, Польша, Португалия, Румыния, Сан-Марино, Словакия, Словения, США, Уругвай, Финляндия, Франция, Чешская Республика, Чили, Швеция, Швейцария, Эквадор, Эль Сальвадор, Эстония

			</div>
		</div>
		<div class="span6"> 
			<h4>SIM-карта "Гигабайт в дорогу Европа + Америка":</h4>
			<ul>
				<li>Позволяет получить доступ в интернет с помощью любого мобильного устройства, имеющего эту функцию: смартфоны, планшетные компьютеры, модемы, роутеры, камерофоны</li>
				<li>Сим-карта обеспечивает 1 Гигабайт  мобильного интернет-трафика на максимальной скорости в сети 3G/2G на территории Евросоюза, Южной, Центральной и Северной Америки  (51 страна и 3 автономных региона**)</li>
				<li>Стоимость: 0.14 $/1 Мб</li>
				<li>Округление сессии с точностью до 1 Kb</li>
				<li>Карта действительна в течение  30 дней с  момента первого выхода в сеть, в «спящем режиме»  (не активированная)  – бессрочный период</li>
				<li>После регистрации карты на сайте компании Вам станет доступен контроль трафика в режиме  он-лайн</li>
				<li>Инструкция по активации карты - на упаковке </li>
				<li>По окончании срока действия карты или трафика интернет-карта аннулируется</li>

			</ul>
		</div>		

		
</div>

</div>


	<script>
					//Показать/спрятать блок
				
		$('#show_countrylist').click(function() {
		if($('#countrylist').css('display') == 'none') $('#countrylist').show('slow');
		else $('#countrylist').hide('slow');
		return false;
	});	
	
		$('#show_countrylist_am').click(function() {
		if($('#countrylist_am').css('display') == 'none') $('#countrylist_am').show('slow');
		else $('#countrylist_am').hide('slow');
		return false;
	});
	
	</script>
