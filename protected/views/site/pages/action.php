<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>


	<div class="container" style="padding: 60px 0 80px 0">
	
	<style>
	
	.sample-mobile-one {
			background-image: url('<?php echo $pathToImg; ?>/sample/mobile-one.png' );
			height: 400px;
		}
	.sample-action-keepgo-sale {
			background-image: url('<?php echo $pathToImg; ?>/sample/action-keepgo-sale.png' );
			height: 550px;
		}
		
	.sample-header {
		padding: 15px 15px;
	}
	.sample-header4 {
		padding: 0px 15px;
	}
	
	.sample-text {
		padding: 15px 15px;
	}
	
	.sample-right {
		text-align: right;
		padding: 15px 15px;
	}
	
	</style>
	
		
		<div class="row">
			<div class="span1">
			</div>
			<div class="span10 brotherbox-index">
				<div class="row">
					<div class="span5 sample-action-keepgo-sale"> 
					</div>
					<div class="span5"> 
					<h3>Распродажа лучшего мобильного интернета в роуминге</h3>
						<h4>Самое большое покрытие по Европе, Северной и Южной Америке (53 страны). Подробности тут <a href="http://easytravel.me/shopkeepgo/">http://easytravel.me/shopkeepgo/</a></h4>
							<p class="sample-text">
								Всего от 0,08$ за 1 Мегабайт, тарификация по 1кб! Любые типы сим-карт - нано, микро, стандарт. При заказе введите номер сертификата <strong>334229992410</strong> для сим-карт Gb2Go Европа + Америка и получите 20% скидки!
							</p>
							<p class="sample-text">
								<strong>Внимание:</strong> срок действия акции до 30.06.2014
							</p>
							<p class="sample-right"><a class="btn btn-large btn-warning " type="button" href="http://easytravel.me/shopkeepgo/" target="_blank">Подробнее</a></p>
								
					</div>
				</div>
			</div>
			<div class="span1">
			</div>
		</div>
		<br/><br/>
				
		<div class="row">
			<div class="span1">
			</div>
			<div class="span10 brotherbox-index">
				<div class="row">
					<div class="span5 sample-mobile-one"> 
					</div>
					<div class="span5"> 
					<h3>Отличная цена на сим-карты Gb2Go (Гигабайт в дорогу)</h3>
								<h4>Только для членов сообщества Легкий Роуминг <a href="https://www.facebook.com/easyroaming">www.facebook.com/easyroaming</a></h4>
								
								<p class="sample-text">
								Cим-карты Gb2Go (Гигабайт в дорогу - 1 Гбайт мобильного интернет в 30 странах Европы в течение 30 дней с момента первого выхода в сеть) продаются по <strong>специальной цене – 90$</strong>, а также 9$ зачисляются на баланс вашей сим-карты easyroaming за счет нашей <a href="http://easytravel.me/page/loyalty_program" target="_blank">бонусной программы!</a>
								</p>
								<p class="sample-right"><a class="btn btn-large btn-warning " type="button" href="https://www.facebook.com/easytravelshop/app_251458316228" target="_blank">Заказать!</a></p>
								
					</div>
				</div>
			</div>
			<div class="span1">
			</div>
		</div>
		
		
			<div style="padding: 80px 0 5px 0" class="row">
		<div class="span2">
			<img src="<?php echo $pathToImg; ?>/shopping-cart.png">
		</div>
		<div class="span10 chat_zaz">
			<div class="row">
				<div class="span1">
				</div>
				<div class="span9 incut">
							<h4>Всегда отличные цены на мобильный интернет в роуминге.</h4><a style="float: right; margin: 0px 20px 0 0" type="button" class="btn btn-warning" href="<?php echo Yii::app()->createUrl('roaming3g'); ?>">Подробнее</a>
				</div>
		</div>
		</div>
	</div>
		
		
			
	</div>			
