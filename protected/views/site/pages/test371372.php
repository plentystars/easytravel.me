<?php
	$this->pageTitle='мобильная связь в Италии, Испании, Австрии, Великобритании, США, Турции, Египте, Украине и других странах мира';//Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>


<style>
.nav-calc-mobile span {
position: absolute;
top: 95px;
left: 8px;
height: 50px;
width: 50px;
border-radius: 50%;
background: #fbac3c;
cursor: pointer;
z-index: 1;
opacity: 1;
box-shadow: 1px 1px 1px rgba(0,0,0,0.1) inset, 1px 1px 1px rgba(255,255,255,0.1);
-webkit-transition: opacity 0.4s ease-in-out-out 0.2s;
-moz-transition: opacity 0.4s ease-in-out-out 0.2s;
-o-transition: opacity 0.4s ease-in-out-out 0.2s;
-ms-transition: opacity 0.4s ease-in-out-out 0.2s;
transition: opacity 0.4s ease-in-out-out 0.2s;
}

.nav-calc-mobile span:after {
content: '';
position: absolute;
width: 40px;
height: 40px;
top: 5px;
left: 5px;
border-radius: 50%;
box-shadow: 1px 1px 2px rgba(0,0,0,0.1);
}

.nav-done span {
	background: #6db343;
}

.nav-balance span {
top: 200px;
}
.nav-type-sim span {
top: 300px;
}
.nav-tarif span {
top: 400px;
}
.nav-krasiv span {
top: 500px;
}
.nav-plus-balance span {
top: 600px;
}
.nav-total span {
top: 700px;
}
.nav-submit span {
top: 800px;
}

</style>




<div class="container">
<div class="row padtop60">
	<div class="span1">
		<div style="height: 700px; background: #6db343; width: 7px; margin: 40px 0px 0px 30px">
			<nav class="nav-calc-mobile nav-done">
				<span></span>
			</nav>
			<nav class="nav-calc-mobile nav-balance nav-done">
				<span></span>
			</nav>
			<nav class="nav-calc-mobile nav-type-sim">
				<span></span>
			</nav>
			<nav class="nav-calc-mobile nav-tarif">
				<span></span>
			</nav>
			<nav class="nav-calc-mobile nav-krasiv">
				<span></span>
			</nav>
			<nav class="nav-calc-mobile nav-plus-balance">
				<span></span>
			</nav>
			<nav class="nav-calc-mobile nav-total">
				<span></span>
			</nav>
			<nav class="nav-calc-mobile nav-submit">
				<span></span>
			</nav>

		</div>
	</div>
	<div class="span6" style="min-width: 380px"> 
	<div id="container"> 
		<form action="<?php echo $this->getStep('delivery', 2); ?>" method="POST" name="calculator">
		<div class="row-fluid">
			<div class="span12" >
				<div class="calc-text">Выберите страну назначения:</div>
				<select name="OrderType[countries][]" class="span12">
					<option selected></option>
					<option value="Ukraine"></option>';
				</select>				
			</div>
		</div>	
			
		<div class="row-fluid">
			<div class="span12" >
				<div class="line-hr">
				</div>				
			</div>
		</div>	
			
		<div class="row-fluid">
			<div class="span5" > 
				<div class="calc-text">SIM-карта:</div>
				<div class="btn-group" id="cards">
					<button class="btn" item-id="2" disabled="disabled">+371</button>
					<button class="btn" item-id="3" disabled="disabled">+372</button>
				</div>
				<input type="hidden" name="OrderType[tariff_sim_id]" value="0" />
			</div>
			
			<div class="span7">
				<div class="calc-text">Выберите стартовый баланс</div>
				<div class="btn-group" id="balans">
				<button class="btn" item-id="1">$5</button>
				<button class="btn" item-id="1">$10</button>
				<button class="btn" item-id="1">$20</button>
				<button class="btn btn-warning" item-id="2">$50</button>
				<button class="btn" item-id="1">$100</button>
				</div>
			</div>
		</div>	
				<div class="row-fluid">
			<div class="span12" >
				<div class="line-hr">
				</div>				
			</div>
		</div>	
		
		<div class="row-fluid">
		<div class="span12">
			<div class="calc-text">Выберите тип SIM-карты</div>
			<div class="btn-group span12" id="typesim" style="text-align: center; padding: 0px 15px 15px 0px">
			<a class="btn btn-warning" type="">Адаптер+Micro Sim</a><a class="btn" type="">Адаптер+Nano Sim</a><a class="btn" type="">Nano Sim</a>
			</div>
				</div>
		</div>
		
		<div class="row-fluid">
			<div class="span12" >
				<div class="line-hr"></div>				
			</div>
		</div>
			
		<div class="row-fluid">
			<div class="span4"> 
				<div class="calc-text">Исходящие звонки</div>
				<div class="your_price" id="cost_call_out">$0</div>
			</div>
			<div class="span4"> 
				<div class="calc-text">Входящие звонки</div>
				<div class="your_price" id="cost_call_in">$0</div>
			</div>
			<div class="span4"> 
				<div class="calc-text">СМС</div>
				<div class="your_price" id="cost_sms_out">$0</div>
			</div>
		</div>
						
		<div class="row-fluid">
			<div class="span12" >
				<div class="line-hr"></div>				
			</div>
		</div>
		
	<script>
					jQuery(document).ready(function(){
					// This button will increment the value
					$('.qtyplus').click(function(e){
						// Stop acting like a button
						e.preventDefault();
						// Get the field name
						fieldName = $(this).attr('field');
						// Get its current value
						var currentVal = parseInt($('input[name='+fieldName+']').val());
						// If is not undefined
						if (!isNaN(currentVal)) {
							// Increment
							$('input[name='+fieldName+']').val(currentVal + 1);
						} else {
							// Otherwise put a 0 there
							$('input[name='+fieldName+']').val(0);
						}
					});
					// This button will decrement the value till 0
					$(".qtyminus").click(function(e) {
						// Stop acting like a button
						e.preventDefault();
						// Get the field name
						fieldName = $(this).attr('field');
						// Get its current value
						var currentVal = parseInt($('input[name='+fieldName+']').val());
						// If it isn't undefined or its greater than 0
						if (!isNaN(currentVal) && currentVal > 0) {
							// Decrement one
							$('input[name='+fieldName+']').val(currentVal - 1);
						} else {
							// Otherwise put a 0 there
							$('input[name='+fieldName+']').val(0);
						}
					});
					
				// Checkbox
				// Отслеживаем событие клика по диву с классом check
				$('.checkboxes').find('.check').click(function(){
			
					// Пишем условие: если вложенный в див чекбокс отмечен
					if( $(this).find('input').is(':checked') ) {
						// то снимаем активность с дива
						$(this).removeClass('active');
						// и удаляем атрибут checked (делаем чекбокс не отмеченным)
						$(this).find('input').removeAttr('checked');
				 
					// если же чекбокс не отмечен, то
					} else {
						// добавляем класс активности диву
						$(this).addClass('active');
						// добавляем атрибут checked чекбоксу
						$(this).find('input').attr('checked', true);
					}
				});
				// RadioButton
				$('.radioblock').find('.radio').each(function(){
					$(this).click(function(){
						// Заносим текст из нажатого дива в переменную
						var valueRadio = $(this).html();
						// Находим любой активный переключатель и убираем активность
						$(this).parent().find('.radio').removeClass('active');
						// Нажатому диву добавляем активность
						$(this).addClass('active');
						// Заносим значение объявленной переменной в атрибут скрытого инпута
						$(this).parent().find('input').val(valueRadio);
					});
				});
			});
				



	
	</script>
	
		<style>
		.qty {
    width: 40px;
    height: 25px;
    text-align: center;
	font-size: 12px;
	font-weight: bold;
	color: #737373;
}

input.qty {	 
	-webkit-border-radius: 3px;
    -moz-border-radius: 3px;
     border-radius: 3px; 
	 background: #f5f5f5;
	 font-size: 12px;
}

input.qtyplus { width:14px; height:14px;}
input.qtyminus { width:14px; height:14px;}

.qtyplus {
	background: url('http://easytravel.me/resources/img/plusminus.png');
	background-position: 0px 0px;
	background-repeat: no-repeat;
	width: 14px;
	height: 14px;	
	cursor: pointer;
}


.qtyminus {
	background: url('http://easytravel.me/resources/img/plusminus.png');
	background-position: 0px -14px;
	background-repeat: no-repeat;
	width: 14px;
	height: 14px;	
	cursor: pointer;
}


#block_numbers > div.row-fluid{
border: 4px solid #6db343;
padding: 10px 0px 0px 10px;
color: #fff;
}
		
#block_numbers > div.row-fluid:hover {
border: 4px solid #f89509;
cursor: pointer;
}		
#block_numbers > div.row-fluid:hover:first-child {
border: 4px solid #6db343;
cursor: pointer;
}

#block_numbers > div.active {
background: #f89509;
}	

#block_balance {
color: #fff;
}
		
		</style>
		
		
		
		<div class="row-fluid">
			<div class="span6" style="padding: 10px 0 ">
				<div class="calc-text">Количество SIM-карт</div>
				 <div style="height: 30px; float: right; padding: 0px 0 0 0; margin: -40px 0 0 0">
								<div style="width: 15px;float: left; padding: 0 0 0 10px">
								<input type='text' name='quantity' value='1' class='qty' />
							</div>
							<div class='qtyplus' field='quantity' style="margin: 2px 0 0 76px;"></div>
							<div class='qtyminus' field='quantity' style="margin: 1px 0 0 76px;"></div>
						</div>	
			</div>
			<div class="span6" style="padding: 7px 0 0 10px;">
				<a class="btn btn-warning"  id="show_block_numbers">Выбрать красивые номера</a>				
			</div>
			<div class="span12" id="block_numbers" style="display: none; padding-right: 30px;">
			
				<div class="row-fluid">
					<div class="span6">
						<select>
						  <option>Международный</option>
						  <option>Локальный</option>
						</select>
					</div>
					<div class="span3" >
					 	<select class="span12">
						  <option>Все</option>
						  <option>Подороже</option>
						  <option>Подешевле</option>
						</select>
					</div>
					<div class="span3">
					<select class="span12">
						  <option>Все</option>
						  <option>Красивый</option>
						  <option>Бронзовый</option>
						  <option>Серебряный</option>
						  <option>Золотой</option>
						  <option>Платиновый</option>
						  <option>VIP</option>
						</select></div>
				</div>
				<div class="row-fluid active">
					<div class="span6">+371 912-16-10</div>
					<div class="span3">199 руб.</div>
					<div class="span3">красивый</div>
				</div>
				<div class="row-fluid">
					<div class="span6">+371 912-16-11</div>
					<div class="span3">199 руб.</div>
					<div class="span3">красивый</div>
				</div>
				<div class="row-fluid">
					<div class="span6">+371 912-16-12</div>
					<div class="span3">199 руб.</div>
					<div class="span3">красивый</div>
				</div>
			
			</div>
		</div>	
								
		<div class="row-fluid">
			<div class="span12" >
				<div class="line-hr"></div>				
			</div>
		</div>		
		<div class="row-fluid">

		</div>	

		
		
		<div class="row-fluid">
			<div class="span12" style="padding: 10px 0 ">
				<a class="btn " style="text-align: center; margin: 0 0 0 160px" id="show_block_balance">Управление балансом карты</a>		
							<div id="block_balance" style="display: none; padding: 20px 0">
					<div class="row-fluid">
					<div class="span6">+371 912-16-10</div>
					<div class="span3 btn-group" data-toggle="buttons-radio">
						<label class="radio"> <input type="radio" value="">195 руб.</label>
						<label class="radio"> <input type="radio" value="">600 руб.</label>
						<label class="radio"> <input type="radio" value="">1500 руб.</label>
						<label class="radio"> <input type="radio" value="">2000 руб.</label>
						<label class="radio"> <input type="radio" value="">2800 руб.</label>
					</div>
					<div class="span3">красивый</div>
				</div>
						<div class="row-fluid">
			<div class="span12" >
				<div class="line-hr"></div>				
			</div>
		</div>		
				<div class="row-fluid">
					<div class="span6">+371 912-16-11</div>
					<div class="span3 btn-group" data-toggle="buttons-radio">
						<label class="radio"> <input type="radio" value="">195 руб.</label>
						<label class="radio"> <input type="radio" value="">600 руб.</label>
						<label class="radio"> <input type="radio" value="">1500 руб.</label>
						<label class="radio"> <input type="radio" value="">2000 руб.</label>
						<label class="radio"> <input type="radio" value="">2800 руб.</label>
					</div>
					<div class="span3">красивый</div>
				</div>
						<div class="row-fluid">
			<div class="span12" >
				<div class="line-hr"></div>				
			</div>
		</div>		
				<div class="row-fluid">
					<div class="span6">+371 912-16-12</div>
					<div class="span3 btn-group" data-toggle="buttons-radio">
						<label class="radio"> <input type="radio" value="">195 руб.</label>
						<label class="radio"> <input type="radio" value="">600 руб.</label>
						<label class="radio"> <input type="radio" value="">1500 руб.</label>
						<label class="radio"> <input type="radio" value="">2000 руб.</label>
						<label class="radio"> <input type="radio" value="">2800 руб.</label>
					</div>
					<div class="span3">красивый</div>
				</div>
			</div>		
				
			</div>
		</div>	

		
		
						
		<div class="row-fluid">
			<div class="span12" >
				<div class="line-hr"></div>				
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span4" style="padding: 30px 0 0 0">
				<div class="calc-text">Сумма заказа всего</div>
			</div>
			
			<div class="span4"  style="padding: 28px 0 0 0">
				<div class="your_price" id="cost">$0</div>
				<input type="hidden" name="OrderType[cost]" />
			</div>
				
			<div class="span4"> 
				<div class="calc-text" style="padding: 0px 0px 8px 30px">Ваш бонус</div>
				<div class="your_bonus" id="bonus">+0</div>
				<input type="hidden" name="OrderType[bonus]" />
			</div>
		</div>	

		<div class="row-fluid">
			<div class="span12" >
				<div class="line-hr"></div>				
			</div>
		</div>
				
		<div class="row-fluid">
			<div class="span12">
				<input type="hidden" name="Orders[type_id]" value="2" />
				<button type="submit" class="btn btn-large btn-warning span12" disabled="disabled">Заказать</button>
			</div>
		</div>
		</form>
	</div>
	</div>
	
	<div class="span5" id="calculatortop"> 
		<h3>Международная мобильная связь</h3>
			<p>Мобильная связь в роуминге вещь довольно дорогая и ваши домашние мобильные операторы, которые предоставляют вам этот сервис зарубежом стараются скрыть за маркетинговыми сложными для понимания простого пользователя акциями саму суть – связь за границей дотирует низкие внутренние тарифы в стране, ведь конкуренция жестокая и нужно постоянно снижать цены в борьбе за абонента. А значит, единственное место, где операторы могут компенсировать низкую маржу – это международный роуминг. И как они не стараются заманить нас «нулями» и «вторыми минутами», если внимательно прочесть то, что написано мелким шрифтом и прочими «ограничениями», «условиями предоставления» и т.д. и т.п. то получается, что включенный международный роуминг в телефоне – это источник риска испортить себе настроение по возвращению из поездки, открыв счет за услуги в роуминге. Блоги и форумы пестрят историями о таких испорченных поездках.<br/>Но выход есть – это международные prepaid SIM-карты <strong>easyroaming@371</strong> и <strong>easyroaming@372</strong></p>
	</div>
</div>



	</div>

	
	<script>
					//Показать/спрятать блок
				
		$('#show_block_balance').click(function() {
		if($('#block_balance').css('display') == 'none') $('#block_balance').show('slow');
		else $('#block_balance').hide('slow');
		return false;
	});	
	
		$('#show_block_numbers').click(function() {
		if($('#block_numbers').css('display') == 'none') $('#block_numbers').show('slow');
		else $('#block_numbers').hide('slow');
		return false;
	});
	
	</script>
