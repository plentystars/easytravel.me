<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>


		<div class="container" style="padding: 60px 0 0 0">
			<div class="row">
				<div class="span6">
					<h2 style="background: #f89c1a; padding: 20px; color: #fff">easyroaming@371</h2>
		</div>
		<div class="span6"> 
				<h2 style="background: #f89c1a; padding: 20px; color: #fff">easyroaming@372</h2>
		</div>
	</div>			
	
	<div class="row">
		<div class="span6">
			<div class="accordion" id="371accord">
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-1">
					Отправить бесплатное SMS-сообщение
				  </a>
				</div>
				<div id="371-1" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Вы можете отправить бесплатно сообщение абоненту easyroaming@ 371, который находится за границей. Стоимость за входящее сообщение отсутствует!</p>
					<p>Единственное ограничение  - отправка SMS возможна с периодичностью не более 1 сообщения в течение 5 минут, но не более 10 сообщений на один и тот же номер в сутки.</p>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-2">
					Услуга «Интернет звонки»
				  </a>
				</div>
				<div id="371-2" class="accordion-body collapse">
				  <div class="accordion-inner">
						
					<div class="accordion" id="371accord-1">
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord-1" href="#371-1-0">
							Описание услуги
						  </a>
						</div>
						<div id="371-1-0" class="accordion-body collapse">
						  <div class="accordion-inner">
							<p>Представляем Вам новую услугу WEB звонок , которая позволит Вам здесь и сейчас, а главное БЕСПЛАТНО, звонить на сим-карту easyroaming@ 371 с любого компьютера. Услуга осуществляется с помощью Технологии WebRTC (коммуникации в реальном времени). Звонки проходят через сеть Интернет, без установки дополнительного программного обеспечения на компьютер пользователя совершающего звонок. На данный момент услуга работает только через браузер Google Chrome.</p>
							<p>Для успешного звонка вам потребуются:</p>
							<ul>
								<li>компьютер (планшет и мобильный телефон не подходит);</li>
								<li>наушники и микрофон (заранее настроенные на компьютере);</li>
								<li>веб браузер Google Chrome (на других браузерах: Mozilla, Internet Explorer услуга не работает).</li>
							</ul>
						  </div>
						</div>
					  </div>
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord-1" href="#371-1-1">
							Как это работает?
						  </a>
						</div>
						<div id="371-1-1" class="accordion-body collapse">
						  <div class="accordion-inner">
							<ul>
								<li>Введите номер на который хотите позвонить. <strong> Пример: 37122222222</strong></li>
								<li>Введите любой 4-х значный цифровой код. <strong>Пример: 0000</strong></li>
								<li>Нажмите кнопку Call.</li>
								<li>В окне Веб браузера появиться всплывающее окно.</li>
								<li>Нажмите кнопку Разрешить (Allow).</li>
								<li>В наушниках услышите сигнал вызова.</li>
								<li>У вызываемого абонента высветится номер  + 371 2190 0000 (0000 – соответствует 4-х значному цифровому коду, который вы ввели ранее).</li>
							</ul>
						  </div>
						</div>
					  </div>
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord-1" href="#371-1-2">
							Стоимость
						  </a>
						</div>
						<div id="371-1-2" class="accordion-body collapse">
						  <div class="accordion-inner">
							<ul>
								<li>Для Вас исходящий звонок является бесплатным.</li>
								<li>Для Абонента, которому вы звоните, ответ на Ваш звонок является платной услугой. Стоимость входящего звонка см. в таблице.</li>
								<li>Вызываемый Абонент, всегда может определить Ваш номер, у которого первые 4 цифры (не включая международный код) будут неизменными + 371 2190, а последние 4 цифры  будут соответствовать коду, который Вы вносите в нижнюю строку (Enter Code) формы для выполнения звонка.</li>
							</ul>
						  </div>
						</div>
					  </div>
					 </div>	
						
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-3">
					Услуга «Перезвони мне»
				  </a>
				</div>
				<div id="371-3" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Если у Вас закончился баланс на карте, услуга «Перезвони мне» предоставляет Вам возможность отправить сообщение абоненту любого мобильного оператора с просьбой перезвонить Вам.</p>
					<p>Услуга предоставляется бесплатно и не требует подключения или дополнительных настроек.</p>
					<p>Для того, чтобы попросить абонента перезвонить Вам, просто наберите у себя в телефоне команду *146*90*00 код страны номер телефона# и нажмите «Вызов». Вы получите информацию на экран Вашего телефона о том, что сообщение отправлено.</p>
					<p>Абонент, которого вы просите позвонить Вам, получит от Вашего имени SMS с текстом «У этого абонента недостаточно средств для звонка. Пожалуйста, перезвоните.»</p>
					<p>Для удобства добавьте данную команду *146*90*00 код страны номер телефона # в телефонный справочник. При необходимости просто выберите нужную запись из адресной книги.</p>
					<p><strong> Услугой «Перезвони мне» можно воспользоваться не более 5-ти раз в сутки</strong> (за сутки принимается временной интервал в 24 часа).</p>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-4">
					Управление "черным" и "белым" списком звонков абонента
				  </a>
				</div>
				<div id="371-4" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Данная услуга подразумевает возможность избавления от нежелательных звонков и контроля денежных средств на балансе сим-карты easyroaming@ 371. Данная услуга на данный момент подключается в ручном режиме. Данная услуга бесплатная.</p>
					<p>Для подключения услуги обращайтесь в справочную службу или пишите на нашу почту .</p>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-5">
					Услуга АнтиОпределитель Номера.
				  </a>
				</div>
				<div id="371-5" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Данная услуга предоставляет возможность скрыть Ваш номер телефона при исходящем вызове.</p>
					<p> Для активации/деактивации услуги наберите на Вашем телефоне (с роуминг картой)  <strong>код 900</strong> (если телефон не поддерживает прямой набор, то <strong>*146*900#</strong>) и нажмите на клавишу <strong>«Call»</strong>. Сервис активирован.</p>
					<p>Абонентам туристической сим-карты easyroaming@ 371 данная услуга предоставляется бесплатно.</p>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-6">
					Перевод баланса с карты на карту
				  </a>
				</div>
				<div id="371-6" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Перевод баланса с карты на карту возможен при условии приобретения у одного Партнера.</p>
					<p>Для переноса баланса (или его части) наберите код *146*89*371хххххххх*ХХХ# (ХХХ-сумма которую вы хотите перечислить), нажмите клавишу «Call».</p>
					<p>При успешном переводе суммы на экране отобразится сообщение о том, что перевод завершен. «Принимающий» абонент НЕ получит сообщение о том, что его баланс пополнен.</p>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-7">
					Уведомление по почте о достижении лимита
				  </a>
				</div>
				<div id="371-7" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Уведомление о достижении лимита карты: лимит, при достижении которого высылается уведомление на почту, устанавливается в параметрах карты нашими специалистами.</p>
					<p>Данная услуга бесплатная как для физических, так и для юридических лиц.</p>
					<p>Для подключения услуги обращайтесь в справочную службу или пишите на нашу почту.</p>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-8">
					Сохранение российского номера за границей
				  </a>
				</div>
				<div id="371-8" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Услуга сохранения своего номера при выезде за границу очень удобна и даже необходима. </p>
					<p>Например, если Вы едете по делам, то совершенно очевидно, что нельзя сообщить свой новый номер всем деловым партнерам и коллегам. С услугой сохранения российского по  сим-карте easyroaming@371Вы всегда будете на связи. Вы сможете принимать все входящие звонки, поступающие на Ваш российский номер. А при исходящих у Ваших родных и коллег будет определяться привычный номер русского мобильного оператора.</p>
					<p>Безлимитные тарифы с сохранением номера становятся настоящим чудом для деловых людей и туристов. Вы сможете забыть о том, что находитесь вдали от дома, и привычно общаться, не боясь счета за услуги связи. Сим-карта easyroaming@371обеспечивает уверенный прием в 123 странах и гарантирует бесплатные входящие. </p>
					<p>Подключить услугу «Сохранение российского номера» Вы сможете самостоятельно благодаря подробной инструкции на этой странице сайта. Подключение производится <strong>абсолютно бесплатно</strong></p>
				  </div>
				</div>
			  </div>
			  
			  
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-9">
					Отправка факса, черный и белый списки входящих номеров,  круглосуточная бесплатная техподдержка и многие другие.
				  </a>
				</div>
				<div id="371-9" class="accordion-body collapse">
				  <div class="accordion-inner">
						
					<div class="accordion" id="371accord-2">
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord-2" href="#371-2-0">
							Стоимость
						  </a>
						</div>
						<div id="371-2-0" class="accordion-body collapse">
						  <div class="accordion-inner">
							<ul>
								<li>Стоимость входящего при звонке на основной номер сим-карты <strong>Согласно тарифам easyroaming@371</strong></li>
								<li>Стоимость входящего при звонке на российский номер <strong>Тариф в стране пребывания + 0,15 у.е./мин.</strong></li>
								<li>Плата за соединение <strong>Нет</strong></li>
								<li>Абонентская плата <strong>Нет</strong></li>
								<li>Стоимость подключения <strong>Бесплатно</strong></li>
								<li>Стоимость переадресации с российского номера <strong>0 — 3,50 руб./мин.**</strong></li>
							</ul>
						  </div>
						</div>
					  </div>
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord-2" href="#371-2-1">
							Подключение услуги
						  </a>
						</div>
						<div id="371-2-1" class="accordion-body collapse">
						  <div class="accordion-inner">
							<ul>
								<li><strong>Регистрация российского номера в системе Еasyroaming.</strong> Выполняется один раз, то есть, перед следующей поездкой данное действие совершать не нужно. На телефоне с сим-картой easyroaming@371наберите комбинацию: <strong>* 146 * 088 * 7XXXXXXXXXX #</strong> <br/>
								Где ХХХХХХХХХХ — ваш российский номер в 10-значном формате. После набора комбинации вам поступит сообщение об удачной регистрации в системе.</li>
								<li><strong>Подтверждение регистрации.</strong> На ваш российский номер поступит SMS с кодом подтверждения регистрации в системе  Еasyroaming.   Код нужно активировать в течение 10 минут, набрав на телефоне с сим-картой easyroaming@371 комбинацию: <strong>*146 * 088 * 7XXXXXXXXXX * ZZZZ #</strong> <br/>
								где ХХХХХХХХХХ — 10 цифр вашего российского мобильного телефона, который вы сохраняете, а ZZZZ — код авторизации, поступивший по SMS </li>
								<li><strong>Переадресация с российского номера.</strong> Переадресацию необходимо установить перед вылетом за границу, российскую сим-карту во время пребывания за границей включать нельзя, поскольку в этом случае переадресация будет тарифицироваться как исходящий звонок в роуминге. Для того, чтобы установить переадресацию наберите комбинацию на телефоне с российской сим-картой: <strong> * * 21 * СЕРВИСНЫЙ НОМЕР #</strong> <br/>
								</li>
							</ul>
							<p>Сервисный номер:</p>
							<ul>
								<li>Для абонентов МТС <strong>+78005558077, +74997000202</strong></li>
								<li>Для абонентов Билайн, Мегафон и др. <strong>+78005558077</strong></li>
							</ul>
							<p>Услуга временно не доступна абонентам Мегафон, номера которых начинаются с +7 (921), +7 (928), +7 (929) 1...... и +7 (902) 1...... В этом случае рекомендуем привязать к сим-карте easyroaming@371 номер другой российской сим-карты, на которую вы сможете установить переадресацию.</p>
							<ul>
								<li><strong>Отключение переадресации.</strong> После возвращения в Россию на телефоне с российской сим-картой наберите комбинацию: <strong># # 21 #</strong></li>
							</ul>
						  </div>
						</div>
					  </div>
					 </div>	
				
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-10">
					Постоплата / предоплата
				  </a>
				</div>
				<div id="371-10" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Система расчетов может быть как предоплатная (prePaid) - абонент расходует уже проплаченную сумму, которая на балансе сим-карты, так и постоплатная (postPaid) - абонент расходует предоставленный лимит. Лимит, предоставленный по расчету postPaid, не зависит от prePaid баланса сим-карты.</p>
					<p>Пример: Вы являетесь сотрудником компании, которая предоставила Вам туристическую <strong>сим-карту easyroaming@371</strong> с определенным лимитом postPaid на служебные цели. Для Ваших личных нужд Вы можете использовать баланс prePaid, не расходуя средства Вашей компании.</p>
					<p>Для смены системы расчетов наберите на Вашем телефоне <strong>код 904</strong> (если телефон не поддерживает прямой набор, то <strong>*146*904#</strong>) и нажмите на клавишу <strong>«Call»</strong>.</p>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-11">
					Приём и отправка факсимильных сообщений
				  </a>
				</div>
				<div id="371-11" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p><strong>Прием</strong></p>
					<ul>
						<li>Зарегистрируйте личный кабинет на сайте.</li>
						<li>Добавьте в свой кабинет туристическую сим-карту easyroaming@371</li>
						<li>Обратитесь в справочную службу для подключения данной услуги.</li>
						<li>Теперь, при получении факса на Ваш адрес электронной почты, к Вам на телефон будет приходить SMS с сообщением об этом.</li>
					</ul>
					<p><strong>Отправка</strong></p>
					<ul>
						<li>Отправка факсимильного сообщения происходит в обычном режиме.</li>
						<li>Наберите телефонный номер, на который Вы желаете отправить информацию (003712ххххххх) и нажмите кнопку «Пуск».</li>
						<li>Абоненту поступит сообщение на телефон о том, что факс принят на его e-mail.</li>
					</ul>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-12">
					Пропущенный звонок
				  </a>
				</div>
				<div id="371-12" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Данная услуга дает возможность получать информацию о пропущенных звонках на свой номер, в том случае, если в момент поступления звонков телефон был отключен или находился вне зоны доступа сети.</p>
					<p>Информация о пропущенных звонках приходит в виде СМС сообщения в момент включения телефона или его подключения к сети.</p>
					<p>Примечание: СМС может быть не доставлено в случае подключения абонента к базовой станции (вышке) или сети оператора, используемых до отключения телефона. При смене базовой станции (вышки) или сети оператора, СМС будет доставлено.</p>
					<p>Принимая во внимание возможность частой смены часовых поясов абонентом сим-карты, дата звонка, к сожалению, в сообщении не указывается.Срок хранения в системе информации о каждом пропущенном звонке – одна неделя. Таким образом, в случае, если телефон был отключен или находился вне зоны доступа сети дольше одной недели, Вы получите информацию о пропущенных звонках за последние 7 дней.</p>
					<p>Услуга добавлена на все номера по умолчанию.</p>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-13">
					Определение местоположения своих СИМ карт
				  </a>
				</div>
				<div id="371-13" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Услуга “Определение местоположения своих сим-карт” предоставляет возможность владельцу сим-карты определить местоположение своей сим-карты в данный конкретный  момент времени. Услуга доступна при включенном и подключенном к сети мобильного  оператора телефоне. Если телефон отключен, услуга недоступна.</p>
					<p>Точность определения местоположения составляет от 200 метров до 2 километров, в зависимости от количества базовых станций оператора мобильной связи, к которому подключен абонент в момент запроса услуги. Для определения местоположения сим-карты Оператор использует базу данных Google, содержащую список базовых станций. </p>
					<p>Оператор не несет ответственности за неточности определения местоположения, связанные с отсутствием в указанной базе данных какой-либо из базовых станций. </p>
					<p>Ограничение: 20 запросов в день.</p>
					<p><strong>Стоимость для абонента: 1,40 USD за 30 календарных дней.</strong></p>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-14">
					СИМ трекинг
				  </a>
				</div>
				<div id="371-14" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p>Услуга “СИМ трекинг” предоставляет следующие возможности:</p>
					<ul>
						<li>Автоматическое отображение на географической карте суточного маршрута сим-карты за указанную дату.</li>
						<li>Ручное (принудительно) определение местоположения сим-карты в данный конкретный момент времени и отображения обновленного маршрута за текущие сутки. При этом, вручную запрашиваемая точка также добавляется в маршрут автоматического определения местоположения.</li>
						<li>Просмотр местоположения всех отслеживаемых сим-карт сервиса, находящихся в радиусе 10 километров от выбранной сим-карты.</li>
					</ul>
					<p>Услуга доступна при включенном и подключенном к сети мобильного оператора телефоне. Если телефон отключен, услуга недоступна.</p>
					<p>При каждом автоматическом или ручном (принудительном) запросе, на телефон с отслеживаемой сим-картой приходит USSD сообщение с текстом «updlocation».Точность определения местоположения составляет от 200 метров до 2 километров, в зависимости от количества базовых станций оператора мобильной связи, к которому подключен абонент в момент запроса услуги. Для определения местоположения сим-карты Оператор использует базу данных Google, содержащую список базовых станций.</p>
					<p>Оператор не несет ответственности за неточности определения местоположения, связанные с отсутствием в указанной базе данных какой-либо из базовых станций.</p>
					<p><strong>Стоимость пакетов:</strong></p>
					<ul>
						<li>Пакет включает в себя 1000 автоматических + 300 ручных (принудительных) определений местоположения. Автоматические определения местоположения осуществляются через каждые 45 минут. Срок действия пакета 30 дней. <strong>Стоимость пакета - 8 USD.</strong></li>
						<li>Пакет включает в себя 500 автоматических + 150 ручных (принудительных) определений местоположения. Автоматические определения местоположения осуществляются через каждые 90 минут. Срок действия пакета 30 дней. <strong> Стоимость пакета - 5 USD.</strong></li>
						<li>Пакет включает в себя 150 ручных (принудительных) определений местоположения. Срок действия пакета 30 дней. <strong>Стоимость пакета– 1,50 USD.</strong></li>
						</ul>
				  </div>
				</div>
			  </div>
			  <div class="accordion-group">
				<div class="accordion-heading">
				  <a class="accordion-toggle" data-toggle="collapse" data-parent="#371accord" href="#371-15">
					M2M
				  </a>
				</div>
				<div id="371-15" class="accordion-body collapse">
				  <div class="accordion-inner">
					<p><strong>Назначение услуги:</strong> Machine-to-Machine (M2M) — общее название технологий, которые позволяют машинам обмениваться информацией друг с другом, или же передавать её в одностороннем порядке. Это могут быть проводные и беспроводные системы мониторинга датчиков или каких-либо параметров устройств (температура, уровень запасов, местоположение и т.д.). M2M также активно используется в системах безопасности и охраны, вендинге,  системах здравоохранения, промышленных телеметрических системах, а также в системах  позиционирования подвижных объектов.</p>
					<p><strong>Краткое описание услуги:</strong> Возможность передачи данных между устройствами с помощью USSD сообщений.</p>
					<p><strong>Необходимое требование:</strong> Оборудование должно поддерживать USSD.</p>
					<p><strong>Стоимость:</strong> Для каждого запроса формируется индивидуальное коммерческое предложение. Для получения стоимости услуги в USD, обращайтесь на е-mail.</p>
				  </div>
				</div>
			  </div>
			  
			  
			  
			  

			</div>	
		</div>
		
		
		
		<div class="span6"> 
				<div class="accordion" id="372accord">
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord" href="#372-1">
							Бесплатная связь с абонентом
					  </a>
					</div>
					<div id="372-1" class="accordion-body collapse">
					  <div class="accordion-inner">
						<div class="accordion" id="372accord-2">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-2" href="#372-1-1">
								Отправить бесплатное SMS-сообщение
							  </a>
							</div>
							<div id="372-1-1" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Вы можете отправить бесплатно сообщение абоненту easyroaming@372, который находится за границей. Стоимость за входящее сообщение отсутствует!</p>
								<p>Единственное ограничение — периодичность  отправки SMS с нашего сайта должна быть не менее 3-х минут.</p>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-2" href="#372-1-2">
								Бесплатный web-звонок на easyroaming@372 
							  </a>
							</div>
							<div id="372-1-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Звоните бесплатно на номер абонента  easyroaming@ 372 с помощью услуги «Web-звонок» прямо с нашего сайта.</p>
								<p>Все, что вам нужно  - устройство, которое поддерживает функцию web-звонков (микрофон и динамики на ноутбуке или гарнитура).</p>
								<p>Стоимость: к стоимости входящего звонка, поступающего через наш сервис, добавится 0,20 у.е./мин. Если в стране, в которой находится абонент, входящие звонки бесплатные, то стоимость разговора составит 0,20 у.е./мин.</p>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-2" href="#372-1-3">
								Бесплатный звонок из Skype на easyroaming@372 
							  </a>
							</div>
							<div id="372-1-3" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Помимо звонка из Skype на номер easyroaming@372 по тарифам международной связи, теперь вы можете позвонить абоненту совершенно бесплатно.</p>
								<p>При совершении звонка наберите номер абонента в формате +372 (800) XX-XX-XX-XX.</p>
							  </div>
								<p>Стоимость: к стоимости входящего звонка, поступающего через наш сервис, добавится 0,20 у.е./мин. Если в стране, в которой находится абонент, входящие звонки бесплатные, то стоимость разговора составит 0,20 у.е./мин.</p>
							</div>
						  </div>
						</div>
				</div>
					</div>
				  </div>
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#372-2">
						SMS-сообщения абоненту easyroaming@372
					  </a>
					</div>
					<div id="372-2" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-2-2">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-2" href="#372-2-0">
								Правила отправки SMS
							  </a>
							</div>
							<div id="372-2-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Номер, на который вы отправляете SMS, должен быть указан в международном формате:
						<p><strong>+ КОД СТРАНЫ, КОД ОПЕРАТОРА, НОМЕР АБОНЕНТА</strong></p>
						<p>Например: +7 903 1234567</p>
						<p>Отправлять SMS-сообщения абоненту  easyroaming@372 можно только на основной номер SIM-карты в формате: +372 5Х ХХ ХХ ХХ</p>
						<p>В том числе и при использовании услуги «Сохранение российского номера».</p>
						<p>Вы можете бесплатно отправить SMS абоненту  easyroaming@372 с нашего сайта.</p>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-2" href="#372-2-1">
								Льготные SMS
							  </a>
							</div>
							<div id="372-2-1" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Абоненты easyroaming@372 могут отправлять SMS-сообщения по льготным ценам</p>
									<ul>
										<li>Льготные SMS из Европы	<strong>0,15 у.е./сообщение</strong></li>
										<li>Льготные SMS из Турции	<strong>0,25 у.е./сообщение</strong></li>
										<li>Льготные SMS из Египта	<strong>0,25 у.е./сообщение</strong></li>
										<li>SMS через SIM-меню <strong>0,10 у.е./сообщение</strong></li>
									</ul>
										<p>Если вам не удается отправить SMS:</p>
											<ul>
												<li>Проверьте номер центра коротких сообщений в настройках SMS в вашем телефоне. Там должен быть указан номер +372 50 99 000</li>
												<li>Попробуйте поменять оператора в настройках сети в вашем телефоне</li>
												<li>Отправьте SMS через SIM-меню</li>
											</ul>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-2-2" href="#372-2-2">
								Льготные SMS из Европы 
							  </a>
							</div>
							<div id="372-2-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Подключите пакет «Льготные SMS из Европы» и отправляйте SMS за 0,15 у.е. вместо 0,40 у.е.</p>
									<ul>
										<li>Стоимость одного SMS-сообщения	<strong>0,15 у.е.</strong></li>
										<li>Подключение пакета	<strong>* 146 * 921 * 2 #</strong></li>
										<li>Стоимость подключения	<strong>0,99 у.е.</strong></li>
										<li>Срок действия пакета	<strong>100 дней</strong></li>
										<li>Абонентская плата	<strong>НЕТ</strong></li>
										<li>Проверка статуса и срока действия услуги	<strong>* 146 * 922 * 2 #</strong></li>
										<li>Способ отправки сообщений	<strong>Классический</strong></li>
									</ul>
								<p><strong>Пакет «Льготные SMS из Европы» действует в следующих странах:</strong></p>
								<p>Австрия Бельгия Болгария Ватикан Великобритания Венгрия Гваделупа Германия Гибралтар Греция Дания Ирландия Испания Италия Кипр Латвия Литва Лихтенштейн Люксембург Мальта Мартиника Нидерланды Норвегия Польша Португалия Реюньон  остров Румыния	Сан-Марино Словакия Словения Финляндия Франция Французская Гвиана Чехия Швеция Эстония</p>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-2-2" href="#372-2-3">
								Льготные SMS из Турции
							  </a>
							</div>
							<div id="372-2-3" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Подключите пакет «Льготные SMS из Турции» и отправляйте SMS за 0,25 у.е. вместо 0,40 у.е.</p>
									<ul>
										<li>Стоимость одного SMS-сообщения	<strong>0,25 у.е.</strong></li>
										<li>Подключение пакета	<strong>* 146 * 921 #</strong></li>
										<li>Стоимость подключения	<strong>0,99 у.е.</strong></li>
										<li>Срок действия пакета	<strong>100 дней</strong></li>
										<li>Абонентская плата	<strong>НЕТ</strong></li>
										<li>Проверка статуса и срока действия услуги<strong>	* 146 * 922 #</strong></li>
										<li>Способ отправки сообщений	<strong>Классический</strong></li>
									</ul>
							</div>
						  </div>
						</div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-2-2" href="#372-2-4">
								Льготные SMS из Египта
							  </a>
							</div>
							<div id="372-2-4" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Подключите пакет «Льготные SMS из Египта» и отправляйте SMS за 0,25 у.е. вместо 0,40 у.е.</p>
									<ul>
										<li>Стоимость одного SMS-сообщения	<strong>0,25 у.е.</strong></li>
										<li>Подключение пакета	<strong>* 146 * 921 * 3 #</strong></li>
										<li>Стоимость подключения	<strong>0,99 у.е.</strong></li>
										<li>Срок действия пакета	<strong>100 дней</strong></li>
										<li>Абонентская плата	<strong>НЕТ</strong></li>
										<li>Проверка статуса и срока действия услуги	<strong>* 146 * 922 * 3 #</strong></li>
										<li>Способ отправки сообщений	<strong>Классический</strong></li>
									</ul>

							</div>
						  </div>
						</div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-2-2" href="#372-2-5">
								SMS через SIM-меню из любой страны мира
							  </a>
							</div>
							<div id="372-2-5" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Вы можете отправлять SMS-сообщения через SIM-меню карты easyroaming@372 из любой страны за 0,10 у.е.</p>
								<p>Найдите в меню вашего телефона SIM-меню easyroaming@372</p>
								<p>В SIM-меню найдите пункт Send SMS.</p>
								<p>В поле «To:» введите номер абонента, которому вы отправляете SMS в международном формате.</p>
								<p>В поле «Message:» введите текст сообщения.</p>
								<p><strong>Важно:</strong> можно писать только латинскими буквами.</p>								
							</div>
						  </div>
						</div>
					</div>
					</div>
				  </div>
				</div>
				
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#372-3">
						MMS-сообщения
					  </a>
					</div>
					<div id="372-3" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-3">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-3" href="#372-3-0">
								Описание услуги
							  </a>
							</div>
							<div id="372-3-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Абоненты Еasyroaming 372   могут получать и отправлять MMS-сообщения. В настоящий момент существует возможность отправлять сообщения только на номера других абонентов Еasyroaming 372   и на электронную почту.</p>
								<p>В ближайшее время станет возможным отправлять MMS на номера абонентов других сотовых операторов.</p>
								<p>Услуга является бесплатной – вы оплачиваете только объем переданной и полученной в процессе передачи сообщения информации по тарифам GPRS в стране пребывания. Средняя стоимость одного MMS составляет 0,15 у.е.</p>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-3" href="#372-3-1">
								Подробнее об услуге
							  </a>
							</div>
							<div id="372-3-1" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
										<li>MMS-сообщение на номер абонента easyroaming@372	<strong>Бесплатно (оплачивается только трафик)</strong></li>
										<li>MMS-сообщение на адрес электронной почты	<strong>Бесплатно (оплачивается только трафик)</strong></li>
										<li>Максимальный размер сообщения	<strong>300 Кб</strong></li>
									</ul>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-3" href="#372-3-2">
								Настройки
							  </a>
							</div>
							<div id="372-3-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>MMS-сообщение можно отправить только на один номер easyroaming@372 или электронный адрес.</p>
								<p><strong>APN:</strong> send.ee</p>
								<p><strong>Адрес сервера:</strong> http://mms.emt.ee/servlets/mms</p>
								<p><strong>Proxy адрес:</strong> 217.71.32.82 port 8080</p>
								<p>Убедитесь, что вы находитесь в сети оператора, предоставляющего услуги GPRS.</p>
								<p>Сменить оператора в настройках сети в вашем телефоне.</p>
							  </div>
							</div>
						  </div>

					  </div>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion4" href="#372-4">
						GPRS/EDGE/3G
					  </a>
					</div>
					<div id="372-4" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-4">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-4" href="#372-4-0">
								Описание услуги
							  </a>
							</div>
							<div id="372-4-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Услуга GPRS/EDGE/3G позволяет:</p>
									<ul>
										<li>Получить доступ к Интернету с мобильного телефона и через GPRS-модем;</li>
										<li>Получить доступ к электронной почте;</li>
										<li>Общаться онлайн с помощью приложений и в социальных сетях;</li>
										<li>Оплачивать услуги связи онлайн с помощью платежных систем.</li>
									</ul>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-4" href="#372-4-1">
								Стоимость услуги
							  </a>
							</div>
							<div id="372-4-1" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Стоимость услуги зависит от страны пребывания (от 0,19 у.е./МБ).</p>
								<p>Округление трафика производится по 10 или 100 Килобайт (зависит от страны пребывания).</p>
								<p>Без абонентской платы.</p>
								<p>Без стоимости подключения.</p>
								<p>Подробнее о стоимости услуги – в разделе Тарифы.</p>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-4" href="#372-4-2">
								Настройки GPRS/EDGE/3G
							  </a>
							</div>
							<div id="372-4-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Настроить вручную:</p>
								<ul>
									<li><strong>APN (точка доступа):</strong> send.ee</li>
									<li><strong>Имя пользователя (Username):</strong> номер SIM-карты Еasyroaming 372  в формате 372 5Х ХХ ХХ ХХ</li>
									<li><strong>Пароль (Password):</strong> оставьте пустым</li>
								</ul>
								<p>Настроить в качестве модема:</p>
								<p><strong>В установках почты SMTP прописать сервер:</strong> gprsmail.send.ee</p>
								<p><strong>Кстати:</strong>Ранее установленные настройки Интернета других операторов связи сохраняются. По возвращении в Россию вы можете переключить их вручную в настройках конфигурации.</p>
							  </div>
							</div>
						  </div>

					  </div>
					</div>
				  </div>
				</div>
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion5" href="#372-5">
						GPRS-Пакет «Европа Онлайн»
					  </a>
					</div>
					<div id="372-5" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-5">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-5" href="#372-5-0">
								Описание
							  </a>
							</div>
							<div id="372-5-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>УИспользование GPRS-пакетов "Европа Оналайн" позволяет сократить расходы на интернет в роуминге в большинстве европейских стран. Выберите наиболее подходящий вариант для вас и оставайтесь онлайн.</p>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-5" href="#372-5-1">
								Европа Онлайн 10
							  </a>
							</div>
							<div id="372-5-1" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>Стоимость подключения	1,90 у.е.</strong></li>
									<li>Срок действия	<strong>1 день</strong></li>
									<li>Объем трафика	<strong>10 Мб</strong></li>
									<li>Подключение услуги	<strong>*146*941*1#</strong></li>
									<li>Отключение услуги	<strong>*146*940*1#</strong></li>
									<li>Проверка статуса	<strong>*146*942*1#</strong></li>
								</ul>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-5" href="#372-5-2">
								Европа Онлайн 50
							  </a>
							</div>
							<div id="372-5-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>Стоимость подключения	9,50 у.е.</strong></li>
									<li>Срок действия	<strong>1 день</strong></li>
									<li>Объем трафика	<strong>50 Мб</strong></li>
									<li>Подключение услуги	<strong>*146*941*2#</strong></li>
									<li>Отключение услуги	<strong>*146*940*2#</strong></li>
									<li>Проверка статуса	<strong>*146*942*2#</strong></li>
								</ul>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-5" href="#372-5-3">
								Европа Онлайн 100
							  </a>
							</div>
							<div id="372-5-3" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>Стоимость подключения	19 у.е.</strong></li>
									<li>Срок действия	<strong>7 дней</strong></li>
									<li>Объем трафика	<strong>100 Мб</strong></li>
									<li>Подключение услуги	<strong>*146*941*3#</strong></li>
									<li>Отключение услуги	<strong>*146*940*3#</strong></li>
									<li>Проверка статуса	<strong>*146*942*3#</strong></li>
								</ul>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-5" href="#372-5-4">
								Европа Онлайн 250
							  </a>
							</div>
							<div id="372-5-4" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>Стоимость подключения	47,50 у.е.</strong></li>
									<li>Срок действия	<strong>30 дней</strong></li>
									<li>Объем трафика	<strong>250 Мб</strong></li>
									<li>Подключение услуги	<strong>*146*941*4#</strong></li>
									<li>Отключение услуги	<strong>*146*940*4#</strong></li>
									<li>Проверка статуса	<strong>*146*942*4#</strong></li>
								</ul>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-5" href="#372-5-5">
								Страны, где действуют пакеты
							  </a>
							</div>
							<div id="372-5-5" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul class="inline">
									<li>Австрия</li><li>	Дания</li><li>	Люксембург</li><li>	Словакия</li><li>Бельгия</li><li>	Ирландия</li><li>	Мальта</li><li>	Словения</li><li>
									Болгария</li><li>	Исландия</li><li>	Мартинике</li><li>	Финляндия</li><li>Великобритания</li><li>	Испания</li><li>	Нидерланды</li><li>	Франция</li><li>
									Венгрия</li><li>	Италия</li><li>	Норвегия</li><li>	Чешская </li><li>Республика</li><li>Германия</li><li>	Кипр</li><li>	Люксембург</li><li>	Швеция</li><li>Гваделупа</li><li>	Латвия</li><li>	Польша</li><li>	Эстония</li><li>Греция</li><li>	Литва</li><li>	Португалия</li>
								</ul>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-5" href="#372-5-6">
								Важно
							  </a>
							</div>
							<div id="372-5-6" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>Нельзя использовать более одного пакета одновременно;</li>
									<li>Стоимость пакета списывается единовременно, в момент подключения;</li>
									<li>Завершение действия пакета происходит по окончании срока действия или при полном использовании трафика, включенного в стоимость пакета;</li>
									<li>Заказ нового пакета станет доступен только при окончании действия предыдущего пакета;</li>
									<li>Неизрасходованный трафик сгорает по окончании срока действия пакета;</li>
									<li>При наличии остатка "Бесплатного трафика 5 Мб в Европе и Турции", сначала используется бесплатный трафик, и только за тем трафик, включенный в стоимость пакета "Европа Онлайн".</li>
								</ul>
							  </div>
							</div>
						  </div>

					  </div>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion6" href="#372-6">
						Пакет «Европейский»
					  </a>
					</div>
					<div id="372-6" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-6">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-6" href="#372-6-0">
								Описание
							  </a>
							</div>
							<div id="372-6-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Пакет является базовым и подключен по умолчанию на всех сим-картах easyroaming@372, активированных с 23 декабря 2011 года.</p>
								<p>Часто бываете в странах Евросоюза? В таком случае вы можете еще больше сократить расходы на сотовую связь в роуминге. Данная функция позволяет сократить расходы как на звонки в Россию, так и внутри страны пребывания и на номера операторов связи других стран.</p>
								<p>Международная sim-карта для Европы – это альтернатива европейскому роумингу сотовых операторов, а также покупке местных сим-карт в странах Европы. Наши сим карты для Европы созданы с учётом потребностей туристов. Они просты в использовании. Роуминг включается автоматически при пересечении границы и, что важно, стоимость звонков едина в разных странах Европы. Кроме того, мы предлагаем высокое качество роуминга, ведь сотрудничая со многими местными операторами, настраиваем роуминг автоматически, выбирая в конкретном месте наилучший по качеству сигнала источник связи.</p>
								<p>У наших сим-карт нет абонентской платы и неограничен срок действия. Вернувшись из путешествия, Вы сможете взять её с собой в следующую поездку! Низкие по цене смс и ммс в роуминге, а также доступный интернет – всё это удобно и выгодно с картой easyroaming@372 </p>
							  </div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-6" href="#372-6-1">
								Подключение
							  </a>
							</div>
							<div id="372-6-1" class="accordion-body collapse">
							  <div class="accordion-inner">
							  <p>Для абонентов архивного тарифа "Весь мир" данную опцию необходимо подключить с помощью сервисной команды или обратиться в службу поддержки абонентов easyroaming .</p>
								<ul>
									<li>Подключение услуги	<strong>* 146 * 931 * 2 #</strong></li>
									<li>Стоимость подключения	<strong>3 у.е.</strong></li>
									<li>Срок действия	<strong>360 дней</strong></li>
									<li>Проверка статуса и срока действия	<strong>* 146 * 932 * 2 #</strong></li>
									<li>Подробнее о стоимости звонков – в разделе Тарифы.</strong></li>
								</ul>
							  </div>
							</div>
						  </div>						
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-6" href="#372-6-2">
								Страны в которых действует пакет «Европейский»
							  </a>
							</div>
							<div id="372-6-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul class="inline">
									<li>Австрия</li><li> Бельгия</li><li> Великобритания</li><li> Венгрия</li><li> Германия</li><li> Греция</li><li>Дания</li><li> Испания</li><li> Италия</li><li> Кипр</li><li> Латвия</li><li> Литва</li><li>Нидерланды</li><li> Норвегия</li><li> Польша</li><li> Португалия</li><li> Турция</li><li>Финляндия</li><li> Франция</li><li> Чехия</li><li> Швеция</li><li> Эстония</li>
								</ul>
							  </div>
							</div>
						  </div>
						 
					  </div>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion7" href="#372-7">
						Сохранение российского номера
					  </a>
					</div>
					<div id="372-7" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-7">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-7" href="#372-7-0">
								Описание
							  </a>
							</div>
							<div id="372-7-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Если вам важно, чтобы знакомые, партнеры или клиенты могли связаться с вами во время вашего пребывания за рубежом – подключайте услугу «Сохранение российского номера»! Данная услуга актуальна не только для Москвы и Санкт-Петербурга, но и для абонентов, easyroaming проживающих на территории всей России!  easyroaming позволяет сохранить российский номер, как при входящих, так и при исходящих вызовах. У вызываемого абонента будет определяться ваш российский номер.</p>
								</div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-7" href="#372-7-1">
								Подключение
							  </a>
							</div>
							<div id="372-7-1" class="accordion-body collapse">
							  <div class="accordion-inner">
							  <ul>
									<li>Регистрация – привязка российского номера к международной туристической сим-карте. Производится один раз и не имеет срока действия.</li> 
									<li>Наберите комбинацию: <strong>* 146 * 098116 * ХХХ ХХХ ХХ ХХ #</strong> и нажмите кнопку Вызов.</li>
									<li>ХХХ ХХХ ХХ ХХ – 10 цифр вашего российского номера (без 8-ки), который вы собираетесь привязать к карте easyroaming@372.</li>
									<li>Временно не регистрируются мобильные номера с кодом 921, 928, 902-1 и 929-1</li>
									</ul>
							  </div>
							</div>
						  </div>						
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-7" href="#372-7-2">
								Переадресация
							  </a>
							</div>
							<div id="372-7-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Переадресацию следует установить до того, как вы покинете пределы РФ. Каждый раз, покидая пределы России, устанавливайте переадресацию с вашего российского номера на бесплатный сервисный номер: + 7 800 333-19-00, и все входящие звонки будут поступать на вашу туристическую сим-карту easyroaming@372.</p>
								<p>Переадресации с российского мобильного номера на единый сервисный номер +7 800 333-19-00 бесплатная.</p>
								<p>Типовая команда для переадресации с российского мобильного: <strong>* * 21 * +78003331900 # </strong>и кнопка «Вызова».</p>
								<p>Также Вы можете сделать переадресацию из меню вашего российского мобильного телефона.</p>
							  </div>
							</div>
						  </div>					
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-7" href="#372-7-3">
								Стоимость
							  </a>
							</div>
							<div id="372-7-3" class="accordion-body collapse">
							  <div class="accordion-inner">
								 <ul>
									<li>Входящий звонок при использовании услуги	<strong>Стоимость входящего звонка в зависимости от страны пребывания + 0,15 у.е./мин.</strong></li>
									<li>Входящий звонок на основной номер SIM-карты	<strong>Стоимость не меняется</strong></li>
									<li>Стоимость переадресации с российского мобильного на единый сервисный номер +7 800 333-19-00	<strong>Бесплатно</strong></li>
									<li>Плата за соединение	<strong>Бесплатно</strong></li>
									<li>Абонентская плата	<strong>НЕТ</strong></li>
									<li>Срок действия услуги	<strong>Не ограничено</strong></li>
								</ul>
							  </div>
							</div>
						  </div>				
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-7" href="#372-7-4">
								Важно
							  </a>
							</div>
							<div id="372-7-4" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Если после подключения услуги у вас изменился номер вашего российского телефона или номер туристической SIM-карты , easyroaming@372 вам необходимо с помощью службы поддержки абонентов, отключить привязку и пройти Регистрацию заново.</p>
								<p>К одной карте  easyroaming@372 можно привязать только один российский номер. Если вы хотите принимать звонки с нескольких российских номеров – вам необходимо установить с них переадресацию на номер, который вы привяжете к карте easyroaming@372 </p>
								<p>Сервисный номер +7 800 333-19-00 предполагает бесплатную переадресацию звонков, установленную на него. Однако, в виду индивидуальной ценовой политики российских сотовых операторов, звонок может оказаться платным по условиям вашего тарифного плана. Уточняйте в службе поддержки вашего оператора. Например, обладатели некоторых тарифных планов Билайна платят за переадресацию на бесплатные сервисные номера 3,50 руб./мин.</p>
								<p>Переадресацию следует установить до того как вы покинете Россию. Российскую сим-карту за границей включать нельзя: в этом случае переадресация на номер +7 800 333-19-00 будет тарифицирована вашим российским оператором как исходящий звонок в роуминге.</p>
								<p>Абоненты Билайна, зарегистрированные в качестве физических лиц подключают переадресацию путем звонка в абонентскую службу оператора.</p>
							  </div>
							</div>
						  </div>
						 
					  </div>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion8" href="#372-8">
						Американский номер
					  </a>
					</div>
					<div id="372-8" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-8">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-8" href="#372-8-0">
								Описание
							  </a>
							</div>
							<div id="372-8-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Услуга позволяет получить местный номер с кодом +1 при нахождении в США, Канаде, Мексике или Пуэрто-Рико. "Американский номер" доступен только для абонентов с easyroaming@372 тарифом "Америка".</p>
								<p>Если вы хотите отключить данную услугу, позвоните в службу поддержки easyroaming +7 (499) 500-90-23</p>
								</div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-8" href="#372-8-1">
								Подключение
							  </a>
							</div>
							<div id="372-8-1" class="accordion-body collapse">
							  <div class="accordion-inner">
							  <p>При нахождении в вышеуказанных странах с сим-картой тариф "Америка" вам присваивается местный номер с телефонным кодом +1.</p>
							<p>После присвоения номера вы получите SMS с текстом "Your number is +1ХХХХХХХХХХ".</p>
							<p>После отъезда из выше указанных стран номер будет активен в течение 60 дней. После истечения срока вам поступит SMS о том, что номер +1ХХХХХХХХХХ более не активен.</p>
							<p>В случае более позднего возвращения в США, Канаду, Мексику и Пуэрто-Рико вам будет присвоен новый номер.</p>
							  </div>
							</div>
						  </div>						
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-8" href="#372-8-2">
								Управление услугой
							  </a>
							</div>
							<div id="372-8-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>Отключение	<strong>*146*320#</strong></li>
									<li>Подключение	<strong>*146*321#</strong></li>
									<li>Проверка статуса	<strong>*146*322#</strong></li>
								</ul>
							  </div>
							</div>
						  </div>					
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-8" href="#372-8-3">
								Входящие вызовы
							  </a>
							</div>
							<div id="372-8-3" class="accordion-body collapse">
							  <div class="accordion-inner">
								 <ul>
									<li>+1ХХХХХХХХХХ (Американский номер)	<strong>0,20 у.е./мин.</strong></li>
									<li>+372ХХХХХХХХ (Основной номер сим-карты)	<strong>Без доп. стоимости</strong></li>
									<li>+7ХХХХХХХХХХ (Российский номер, если подключена услуга «Сохранение номера»)<strong>0,15 у.е/мин.</strong></li>
								</ul>
							  </div>
							</div>
						  </div>				
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-8" href="#372-8-4">
								Входящие SMS
							  </a>
							</div>
							<div id="372-8-4" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>+1ХХХХХХХХХХ (Американский номер)	<strong>Без доп. стоимости</strong></li>
									<li>+372ХХХХХХХХ (Основной номер сим-карты)	<strong>Без доп. стоимости</strong></li>
								</ul>
							  </div>
							</div>
						  </div>		
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-8" href="#372-8-5">
								Исходящие вызовы
							  </a>
							</div>
							<div id="372-8-5" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>+1ХХХХХХХХХХ – если звоните на местные номера	<strong>Без доп. стоимости</strong></li>
									<li>+372ХХХХХХХХ – при звонке на номера других стран, в том числе в РФ<strong>Без доп. стоимости</strong></li>
									<li>+7ХХХХХХХХХХ – при звонке на номера других стран, в том числе РФ, если подключена услуга «Сохранение номера»<strong>Без доп. стоимости</strong></li>
								</ul>
							  </div>
							</div>
						  </div>
						  						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-8" href="#372-8-6">
								Исходящие SMS
							  </a>
							</div>
							<div id="372-8-6" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>+1ХХХХХХХХХХ – если звоните на местные номера	<strong>Без доп. стоимости</strong></li>
									<li>+372ХХХХХХХХ – при звонке на номера других стран, в том числе в РФ<strong>Без доп. стоимости</strong></li>
								</ul>
							  </div>
							</div>
						  </div>		
						 
					  </div>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion9" href="#372-9">
						Бесплатные звонки из  Skype
					  </a>
					</div>
					<div id="372-9" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-9">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-9" href="#372-9-0">
								Описание
							  </a>
							</div>
							<div id="372-9-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Теперь из любой точки планеты при подключении услуги «Бесплатные звонки из Skype» ваши родные и близкие смогут звонить на вашу сим-карту Еasyroaming 372    совершенно бесплатно.</p>
								<p>Для этого нужно всего лишь подключить услугу «Бесплатные звонки из Skype»:</p>
								<p>Стоимость входящего звонка, поступившего по Skype, для абонента easyroaming@372 складывается из стоимости входящего звонка в стране пребывания плюс 0,20 у.е. за минуту. То есть, в 135 странах с бесплатными входящими минута разговора обойдётся туристу всего в 7 рублей, а звонящему по Skype не будет стоить ни копейки.</p>
							</div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-9" href="#372-9-1">
								Подключение
							  </a>
							</div>
							<div id="372-9-1" class="accordion-body collapse">
							  <div class="accordion-inner">
							  <ul>
								<li>Стоимость подключения услуги	<strong>Бесплатно</strong></li>
								<li>Абонентская плата	<strong>Нет</strong></li>
								<li>Дополнительная стоимость при входящих звонках	<strong>0,20 у.е./мин.</strong></li>
								<li>Формат набора номера из Skype	<strong>+372 (800) ХХ-ХХ-ХХ-ХХ <br/> ХХ-ХХ-ХХ-ХХ – номер Еasyroaming 372   <strong>без +372</strong></li>
								<li>Формат номера, который определяется при входящем звонке из Skype	<strong>+372 (800) ХХ-ХХ-ХХ-ХХ</strong></li>
								<li>Подключение услуги	*146*098141#</strong></li>
								<li>Отключение услуги	*146*098142#</strong></li>
								<li>Проверка состояния	*146*098140#</strong></li>
							</ul>
							  </div>
							</div>
						  </div>						
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-9" href="#372-9-2">
								Пример использования
							  </a>
							</div>
							<div id="372-9-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Звоним бесплатно из Skype на номер абонента easyroaming@372  +37253204388, абонент находится в Италии:</p>
								<p>Набираем в Skype +37280053204388.</p>
								<p>Стоимость для того, кто звонит - Бесплатно</p>
								<p>Стоимость для того, кто принимает звонок - 7 руб.</p>
							  </div>
							</div>
						  </div>					
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-9" href="#372-9-3">
								Рекомендации по пользованию услугой
							  </a>
							</div>
							<div id="372-9-3" class="accordion-body collapse">
							  <div class="accordion-inner">
								 <ul>
									<li>Используйте новейшую версию Skype, так как она включает специальные обновления для Еasyroaming 372   при использовании более ранних версий (релизы до 13 июля 2012 г.) могут возникать проблемы при наборе номеров формата +372800XXXXXXXX.</li>
									<li>Входящий вызов при звонке из Skype как правило приходит на Еasyroaming 372   с номера, начинающегося на +372800. Тем не менее, существует небольшая вероятность некорректного определения номера.</li>
									<li>Если при наборе номера в Skype пользователь уже выбрал эстонский код из списка, следует набрать лишь 800XXXXXXXX, так как префикс +372 будет добавлен автоматически.</li>
									<li>Если пользователь набирает номер Еasyroaming 372 в Skype без 800 (например, +37253204388), то звонок тарифицируется как звонок на эстонский мобильный номер, при этом пользователь  платит только за входящий звонок, если он платный.</li>
									<li>Если услуга «Бесплатные звонки из Skype» не активирована для конкретной SIM-карты, при наборе из Skype прозвучит автоматическое сообщение: «The mobile phone is either switched off or out of coverage area».</li>
								</ul>
							  </div>
							</div>
						  </div>				

					  </div>
					</div>
				  </div>
				</div>
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion10" href="#372-10">
						<strong>Личный помощник</strong>
					  </a>
					</div>
					<div id="372-10" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-10">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-10" href="#372-10-0">
								Описание
							  </a>
							</div>
							<div id="372-10-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Личный помощник – специальная услуга для абонентов easyroaming@372. 24 часа в сутки, 7 дней в неделю, 365 дней в году наши специалисты на связи с Вами и готовы помочь в любой ситуации . Ваш «Личный помощник» поможет Вам чувствовать за границей в полной безопасности и оперативно решать возникающие вопросы.</p>
								<p>Для того чтобы воспользоваться услугой просто позвоните по номеру +372 992.</p>
							</div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-10" href="#372-10-1">
								Стоимость услуги
							  </a>
							</div>
							<div id="372-10-1" class="accordion-body collapse">
							  <div class="accordion-inner">
							  <p>До 01 января 2014 года услуга предоставляется бесплатно. Вы платите только за звонок, согласно тарифам в стране пребывания.</p>
							  </div>
							</div>
						  </div>						
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-10" href="#372-10-2">
								Онлайн переводчик
							  </a>
							</div>
							<div id="372-10-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>«Личный помощник» поможет Клиенту перевести информацию на любой из основных языков. Используя ресурсы 26 call-центров по всему миру, доступна языковая поддержка на более чем 90 языках.</li>
									<li>Возможна организация конференц-звонка, в котором участвуют 3 линии: Абонент, лицо, речь которого необходимо перевести («Личный помощник» свяжется с ним и добавит в конференц-звонок), «Личный помощник». Если лицо, речь которого необходимо перевести, находится в одном помещении с Абонентом, возможно использование громкой связи.</li>
								</ul>
							  </div>
							</div>
						  </div>					
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-10" href="#372-10-3">
								Юридическая помощь
							  </a>
							</div>
							<div id="372-10-3" class="accordion-body collapse">
							  <div class="accordion-inner">
								 <ul>
									<li>Справка об особенностях в законодательстве конкретной страны</li>
									<li>Помощь адвоката</li>
									<li>Общение с представителями правопорядка</li>
									<li>Нарушение ваших прав</li>
								</ul>
							  </div>
							</div>
						  </div>				
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-10" href="#372-10-4">
								Медицинская помощь
							  </a>
							</div>
							<div id="372-10-4" class="accordion-body collapse">
							  <div class="accordion-inner">
								 <ul>
									<li>Онлайн консультация врача</li>
									<li>Вызов скорой помощи</li>
									<li>Поиск и запись к врачу</li>
									<li>Амбулаторное и стационарное лечение</li>
								</ul>
							  </div>
							</div>
						  </div>				
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-10" href="#372-10-5">
								Проблемы в поездке
							  </a>
							</div>
							<div id="372-10-5" class="accordion-body collapse">
							  <div class="accordion-inner">
								 <ul>
									<li>Проблемы при заселении в отель</li>
									<li>Поиск и бронирование отеля</li>
									<li>Поиск и покупка билетов на все виды транспорта</li>
									<li>Аренда автомобиля</li>
									<li>Заказ такси, трансфера</li>
									<li>Экстренные ситуации</li>
								</ul>
							  </div>
							</div>
						  </div>				
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-10" href="#372-10-6">
								Досуг (в стране пребывания)
							  </a>
							</div>
							<div id="372-10-6" class="accordion-body collapse">
							  <div class="accordion-inner">
								 <ul>
									<li>Рестораны, клубы, бары</li>
									<li>Театры, кино</li>
									<li>Экскурсии</li>
									<li>Шопинг и пр.</li>
								</ul>
								<p>В этих и многих других вопросах вам помогут разобраться наши специалисты, готовые прийти на помощь в любой точке мира.</p>
							  </div>
							</div>
						  </div>				
					  </div>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion11" href="#372-11">
						Конференц-связь
					  </a>
					</div>
					<div id="372-11" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-11">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-11" href="#372-11-0">
								Описание
							  </a>
							</div>
							<div id="372-11-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Услуга «Конференция» позволяет общаться с несколькими абонентами одновременно в режиме конференц-связи. Абонент Еasyroaming 372 может инициировать конференцию между 10 абонентами (включая абонента Еasyroaming 372)</p>
							</div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-11" href="#372-11-1">
								Стоимость услуги
							  </a>
							</div>
							<div id="372-11-1" class="accordion-body collapse">
							  <div class="accordion-inner">
							  <p>Услуга тарифицируется исходя из стоимости исходящих звонков в стране пребывания. При этом стоимость рассчитывается следующим образом:</p>
								<p>Общая продолжительность конференции (начинается со звонка на сервисный номер +372-993)</p>
							  </div>
							</div>
						  </div>						
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-11" href="#372-11-2">
								Организация конференции
							  </a>
							</div>
							<div id="372-11-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>Абонент  Еasyroaming 372   звонит по номеру +372-993</li>
									<li>После ответа автоинформатора нужно нажать * и ввести номер абонента, которого нужно подключить к конференции в международном формате (74951234567) и нажать #.</li>
									<li>Чтобы подключить еще одного участника конференции нужно нажать *, затем еще раз * и ввести номер абонента в международном формате и нажать #. После ответа нового участника конференции нужно нажать *, чтобы объединить всех участников.</li>
									<li>Максимальное количество участников - 10, включая абонента. Еasyroaming 372</li>
								</ul>
							  </div>
							</div>
						  </div>					
					  </div>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion12" href="#372-12">
						Голосовая почта и факс
					  </a>
					</div>
					<div id="372-12" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-12">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-12" href="#372-12-0">
								Стоимость
							  </a>
							</div>
							<div id="372-12-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>С помощью международной сим-карты Еasyroaming 372  вы можете принимать голосовые сообщения и факсы.<p>
								<p>Прием и прослушивание голосовых сообщений.</p>
								<ul>
									<li>Запись сообщений <strong>Бесплатно</strong></li>
									<li>Прослушивание оставленных сообщений <strong>Равна стоимости исходящего звонка в стране пребывания</strong></li>
									<li>Абонентская плата <strong>Нет</strong></li>
									<li>Стоимость подключения <strong>Бесплатно</strong></li>
								</ul>
							</div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-12" href="#372-12-1">
								Сервисные команды:
							  </a>
							</div>
							<div id="372-12-1" class="accordion-body collapse">
							  <div class="accordion-inner">
							  <ul>
									<li>Подключение услуги <strong>* 146 * 091 #</strong></li>
									<li>Отключение услуги <strong>* 146 * 090 #</strong></li>
									<li>Проверка сообщений <strong>* 146 * 094 #</strong></li>
									<li>Прослушать сообщения <strong>* 146 * 095 #, после ответа добавочный 1</strong></li>
								</ul>
							  </div>
							</div>
						  </div>						
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-12" href="#372-12-2">
								Голосовое меню
							  </a>
							</div>
							<div id="372-12-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>Вход в голосовое меню <strong>* 146 * 095 #</strong></li>
									<li>Помощь <strong>*</strong></li>
									<li>Функция почтового ящика <strong>0</strong></li>
									<li>Прослушивание голосовых сообщений <strong>1</strong></li>
									<li>Смена директории <strong>2</strong></li>
									<li>Дополнительные функции <strong>3</strong></li>
									<li>Прослушать сообщение повторно <strong>5</strong></li>
									<li>Прослушать следующее сообщение <strong>6</strong></li>
									<li>Удалить сообщение<br/>Следует регулярно удалять сообщения <strong>7</strong></li>
									<li>Выход <strong>#</strong></li>
								</ul>
							  </div>
							</div>
						  </div>					
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-12" href="#372-12-3">
								Прием факсов
							  </a>
							</div>
							<div id="372-12-3" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>1. Отправьте письмо с темой: «Факс», по адресу: assist@easyroaming.ru</p>
								<p>2. Укажите в тексте письма свой номер easyroaming@372  и адрес электронной почты, на который Вы хотите получать факсимильные сообщения.</p>
								<p>Теперь при отправке контрагентом факса на ваш номер easyroaming@372 вы будете получать документ по почте в виде вложения в формате .tiff. Многостраничный факс приходит в виде нескольких писем. Будьте внимательны: прием факса инициируется в тех же случаях, в которых срабатывает голосовая почта:</p>
								<ul>
									<li>Если абонент находится вне сети или отключен;</li>
									<li>Если входящий звонок отклонен;</li>
									<li>Если на звонок не отвечают в течение минуты (зависит от настроек факса отправителя).</li>
								</ul>
								<p>Таким образом, вы можете выбирать между функцией голосовых сообщений и приемом факса.</p>
								 </div>
							</div>
						  </div>					
					  </div>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion13" href="#372-13">
						Переадресация вызовов
					  </a>
					</div>
					<div id="372-13" class="accordion-body collapse">
					  <div class="accordion-inner">
						
							<div class="accordion" id="372accord-13">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-13" href="#372-13-0">
								Описание услуги
							  </a>
							</div>
							<div id="372-13-0" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Переадресация вызовов – услуга, которая позволяет вам оставаться на связи для тех людей, которым известен только номер вашей туристической SIM-карты. Самое важное, что переадресация вызовов с номера easyroaming@372 является абсолютно БЕСПЛАТНОЙ, если вы устанавливаете переадресацию на российский номер телефона!<p>
							</div>
							</div>
						  </div>
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-13" href="#372-13-1">
								Стоимость
							  </a>
							</div>
							<div id="372-13-1" class="accordion-body collapse">
							  <div class="accordion-inner">
							  <ul>
									<li>Подключение (место нахождения значения не имеет) <strong>Бесплатно</strong></li>
									<li>Абонентская плата Нет</strong></li>
									<li>Стоимость переадресованного вызова на номера: России, Украины, Турции, Египта, США, Чешской Республики <strong>Бесплатно</strong></li>
									<li>Румынии <strong>0,25 у.е./мин.</strong></li>
								</ul>
							  </div>
							</div>
						  </div>						
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-13" href="#372-13-2">
								Подключение услуги
							  </a>
							</div>
							<div id="372-13-2" class="accordion-body collapse">
							  <div class="accordion-inner">
								<ul>
									<li>Включение <strong>* 146 * 081 * 00, КОД СТРАНЫ, КОД ГОРОДА или ОПЕРАТОРА, НОМЕР ТЕЛЕФОНА #</strong></li>
									<li>Отключение <strong>* 146 * 080 # </strong></li>
								</ul>
							  </div>
							</div>
						  </div>					
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#372accord-13" href="#372-13-3">
								Важно
							  </a>
							</div>
							<div id="372-13-3" class="accordion-body collapse">
							  <div class="accordion-inner">
								<p>Существует возможность установить только безусловную переадресацию. Переадресация устанавливается ТОЛЬКО приведенным выше способом. Установить переадресацию можно в том числе и на территории России</p>
								 </div>
							</div>
						  </div>					
					  </div>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion14" href="#372-14">
						Переадресация вызовов
					  </a>
					</div>
					<div id="372-14" class="accordion-body collapse">
					  <div class="accordion-inner">
						<p>Услуга «Позвони мне» позволяет бесплатно отправить SMS-сообщение с просьбой перезвонить вам абонентам easyroaming@372.</p>
						<p>Услуга является бесплатной и не требует подключения.</p>
						<p>Наберите с карты  easyroaming@372 команду: <strong> * 146 * 098113 * 372ХXXXXXXX #</strong></p>
						<p>На номер, указанный вами в запросе, поступит SMS с текстом: <strong>«Абонент просит, чтобы вы перезвонили по номеру +372ХХХХХХХХ.»</strong></p>
						<p>Услугой можно воспользоваться не более 5 раз в течение суток.</p>
					</div>
				  </div>
				</div>	
				  <div class="accordion-group">
					<div class="accordion-heading">
					  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion15" href="#372-15">
						«Ожидание (удержание) вызова»
					  </a>
					</div>
					<div id="372-15" class="accordion-body collapse">
					  <div class="accordion-inner">
						<p>Услуга «Ожидание вызова» предоставляет возможность абонентам туристических SIM-карт easyroaming@372 поставить текущий разговор в режим ожидания и принять второй входящий звонок.</p>
						<p>Услуга предоставляется бесплатно, не требует подключения.</p>
						<p><strong>Управление услугой:</strong> Телефонный аппарат должен поддерживать функцию «Удержание вызова». Отключение/подключение осуществляется из настроек телефонного аппарата.</p>
					</div>
				  </div>
				</div>	
				
		</div>
	</div>
	
	
	

				
				
</div>				
</div>