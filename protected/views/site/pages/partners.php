<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>

	
<div class="container">
		<div class="row" style="padding: 60px 0 0 0">
			<div class="span6"> 
			<img src="<?php echo $pathToImg; ?>/keepgo-tree.png">
		</div>
		<div class="span6"> 
		<h3>Наше предложение</h3>
			<p>Российский рынок международного роуминга превышает 1 миллиард USD. И рынок мобильных устройств (планшетов, смартфонов и мобильных телефонов) растет в геометрической прогрессии ежегодно. Все это создает предпосылки появлению новых услуг и сервисов для выезжающих зарубеж туристов и деловых людей.</p>
			<p><strong>Сим-карты KeepGo</strong> - реальная альтернатива для туристов и деловых людей из России, Украины и других стран СНГ получить лучший доступ в интернет в международном роуминге. Причем экономия средств абонента может доходить до 10 раз по сравнению с международным роумингом от российских сотовых компаний.</p>
			<p>Очевидно, что потенциал рынка услуг доступа в Интернет велик. В связи с этим мы заинтересованы в развитии партнерской сети во всех регионах России и стран СНГ.</p>
		</div>
	</div>
	<div class="row" style="padding: 80px 0">
		<div class="span2">
			<img src="<?php echo $pathToImg; ?>/camera.png">
		</div>
		<div class="span10 chat_zaz">
			<div class="row">
				<div class="span1">
				</div>
				<div class="span9 incut">
							<h4>Трафик интернет на картах KeepGo стоит настолько мало, что наши клиенты могут просматривать видеокамеры своих объектов и проводить видеоконференции из роуминга.</h4><a class="btn btn-warning" type="button" style="float: right; margin: 0px 20px 0 0" href="<?php echo Yii::app()->createUrl('roaming3g'); ?>">Заказать!</a>
				</div>
		</div>
		</div>
	</div>
			<div class="row">
				<div class="span6"> 
				<h3>Идеальный вариант</h3>
				<p>Продажа <strong>Сим-карт KeepGo</strong> и контрактов на длительное обслуживание мобильного интернета <strong>KeepGo</strong> в роуминге может быть как отдельным бизнесом, так  и дополнительным направлением деятельности Вашей компании. Так, например, это идеально подходит для:</p>
				<ul class="marker">
					<li>Туристических операторов и агентств.</li>
					<li>Салонов сотовой связи.</li>
					<li>Транспортных компаний.</li>
					<li>Авиа-компаний.</li>
					<li>Авиа- и железнодорожных билетных касс.</li>
					<li>Точек продаж в аэропортах и ж/д вокзалах.</li>
				</ul>
		</div>
		<div class="span6"> 
			<img src="<?php echo $pathToImg; ?>/keepgo.png">
		</div>
	</div>
	<div class="row" style="padding: 80px 0">
		<div class="span2">
			<img src="<?php echo $pathToImg; ?>/envelope.png">
		</div>
		<div class="span10 chat_zaz">
			<div class="row">
				<div class="span1">
				</div>
				<div class="span9 incut">
					<h4>Если Вам интересно стать частью успешного международного проекта, обращайтесь к менеджеру по развитию партнёрской сети:</h4>
					<hr/>
					<h4>Ярослав Соколов</h4>
					<h4>Тел: +7 495 212-90-06</h4>
					<h4>Написать <a class="btn btn-warning" type="button" href="mailto:yar@easyroaming.ru">yar@easyroaming.ru</a></h4>
				</div>
		</div>
		</div>
	</div>
</div>
