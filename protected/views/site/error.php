<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>

<div class="container">
    <div class="row">
        <div class="span12">
			<?php if($error !== NULL): ?>
		        <?php if($error == 0): ?>
		        <h2 style="background: #6db343; padding: 20px; color: #fff">Ваш заказ оплачен.</h2>
		        <?php else: ?>
		        <h2 style="background: #6db343; padding: 20px; color: #fff">Ошибка</h2>
		        <?php endif; ?>
			<?php else: ?>
	            <h2 style="background: #6db343; padding: 20px; color: #fff">Страница не найдена</h2>
			<?php endif; ?>
		</div>
	</div>
</div>
