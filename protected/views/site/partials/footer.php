<footer class="footer">
  <div class="container">
	<div class="row">
	
	
	<!---- FACEBOOK --->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!---- END FB---->
	
		<div style="min-width: 270px" class="span3">
			<h5>Следуйте за нами</h5>
				<div style="background: #fff; width: 275px;">
					<div class="fb-like-box" data-href="https://www.facebook.com/easyroaming" data-width="275" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
			</div></div>			
		<div class="span3">
			<h5>Способы оплаты</h5>

			  <ul class="footer-links li-bordered">
			  <li style="margin-right: 45px;"><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/visa.png"></a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/master-card.png"></a></li>					  
			  <li style="margin-right: 45px;"><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/paypal.png"></a></li>				  
			  <li><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/yandex-money.png"></a></li>				  
			  <li style="margin-right: 45px;"><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/liqpay.png"></a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/robokassa.png"></a></li>
			  <!--<li><a href="#"><img src="<?php echo $path; ?>/webmoney.png"></a></li>
			  <li style="margin-right: 45px;"><a href="#"><img src="<?php echo $path; ?>/cash.png"></a></li> -->				 
			  <li><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/bank.png"></a></li>
			  
			  </ul>
		</div>		
		<div class="span3">
			<h5>Доставка</h5>
			  <ul class="footer-links li-bordered">
			  <li style="margin-right: 45px;"><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/im-logistics.png"></a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/dhl.png"></a></li>
			  <li style="margin-right: 45px;"><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/ems.png"></a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/pony-express.png"></a></li>
			  <li style="margin-right: 45px;"><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>"><img src="<?php echo $path; ?>/russian-post.png"></a></li>
			  </ul>
			  
			    <p style="padding-top: 160px; float: right;">
			  			  <!-- Yandex.Metrika informer -->
						<a href="http://metrika.yandex.ru/stat/?id=22557637&amp;from=informer" target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/22557637/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
						style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:22557637,lang:'ru'});return false}catch(e){}"/></a>
						<!-- /Yandex.Metrika informer -->
						<!--LiveInternet counter--><script type="text/javascript"><!--
						document.write("<a href='http://www.liveinternet.ru/click' "+
						"target=_blank><img src='//counter.yadro.ru/hit?t44.15;r"+
						escape(document.referrer)+((typeof(screen)=="undefined")?"":
						";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
						screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
						";"+Math.random()+
						"' alt='' title='LiveInternet' "+
						"border='0' width='31' height='31'><\/a>")
						//--></script><!--/LiveInternet-->
				</p>
		</div>			
		<div class="span3">
			<h5>Важные ссылки</h5>
			  <ul class="footer-links">
			  <li><a href="<?php echo Yii::app()->createUrl('page/about'); ?>">О нас</a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/action'); ?>">Скидки</a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/payntake'); ?>">Доставка и оплата</a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/faq'); ?>">Популярные вопросы</a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('user/login'); ?>">История заказов</a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/legalinformation'); ?>">Юридическая информация</a></li>
			  
			  </ul>
			<h5>Лучшее предложение</h5>
			  <div class="subscribe">
				<form>
					<input type="email" placeholder="Введите ваш email" name="email">
					<button type="button" class="btn btn-warning">Подписаться</button>
				</form>
			  
			  </div>
			  <p style="padding-top: 80px; float: right;">&copy; easyroaming.ru 2013

</p>
		</div>
	</div>
  </div> 
</footer>
