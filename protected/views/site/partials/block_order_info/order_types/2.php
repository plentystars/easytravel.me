﻿<h3>Ваш заказ:</h3>
<div class="calc-text" style="background: #6db343; line-height: 3; padding: 10px 10px">
	<div>
		<strong>Страна: </strong>
		<span><?php echo $html_values['countries']; ?></span>
	</div>
	<div>
		<strong>SIM-карта: </strong>
		<span><?php echo $html_values['type']; ?></span>
	</div>
	<div>
		<strong>Баланс: </strong>
		<span><?php echo $html_values['balans']; ?></span>
	</div>
	<div>
		<strong>Добавленный баланс: </strong>
		<span><?php echo $html_values['add_balans']; ?></span>
	</div>
	<div>
		<strong>Бонус: </strong>
		<span><?php echo $html_values['bonus']; ?></span>
	</div>
</div>
<div>
	<h4>
		Сумма заказа:
		<span id="cost">
			$<?php echo $html_values['cost']; ?> / <small class="costcolor">
			<?php echo ceil(($html_values['cost'])*$this->getRate()); ?> руб. </small>
		</span>
	</h4>
</div>