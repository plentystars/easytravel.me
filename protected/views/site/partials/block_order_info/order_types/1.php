﻿<?php
	$html_values['countries']				= (isset($html_values['countries']) ? $html_values['countries'] : '');
	$html_values['date']					= (isset($html_values['date']) ? $html_values['date'] : '');
	$html_values['tariff']					= (isset($html_values['tariff']) ? $html_values['tariff'] : '');
	$html_values['equipment']				= (isset($html_values['equipment']) ? $html_values['equipment'] : '');
	$html_values['equipment_cost']			= (isset($html_values['equipment_cost']) ? $html_values['equipment_cost'] : '');
	$html_values['equipment_deposit_cost']	= (isset($html_values['equipment_deposit_cost']) ? $html_values['equipment_deposit_cost'] : '');
	$html_values['cost']					= (isset($html_values['cost']) ? $html_values['cost'] : '');
	$html_values['type_sim']				= (isset($html_values['type_sim']) ? $html_values['type_sim'] : '');
    $html_values['cost_sim']                = (isset($html_values['cost_sim']) ? $html_values['cost_sim'] : '');
	$hmtl_values['prefix']					= (isset($html_values['prefix']) ? $html_values['prefix'] : '');
	$hmtl_values['number']					= (isset($html_values['number']) ? $html_values['number'] : '');
	
	$display['equipment'] = $html_values['equipment'] == '' ? 'none' : 'block';
?>
<h3 style="padding-top: 10px;">Ваш заказ:</h3>
<div class="calc-text" style="background: #6db343; line-height: 3; padding: 10px 10px">
	<div>
		<strong>Страна: </strong>
		<span id="countries"><?php echo $html_values['countries']; ?></span>
	</div>
	<div>
		<strong>Дата: </strong>
		<span id="date"><?php echo $html_values['date']; ?></span>
	</div>
	<div>
		<strong>Тариф: </strong>
		<span id="tariff"><?php echo $html_values['tariff']; ?></span>
	</div>
    <!-- additional cost -->
    <?php if($html_values['settings']): ?>
        <?php foreach($html_values['settings']->add_cost as $add_cost): ?>
    <div>
        <strong><?php echo $add_cost->name; ?> : </strong>
        <span><?php echo $add_cost->lsign . $add_cost->cost; ?></span>
    </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <!-- /additional cost -->
	<div style="display: <?php echo $display['equipment']; ?>">
		<strong>Оборудование: </strong>
		<span id="equipment_name"><?php echo $html_values['equipment']; ?></span>
		<a href="#" style="float: right;" title="Удалить" alt="Удалить" onclick="getOrderInfo(0); $('button.equipment').attr('disabled', false); return false;"><i class="icon-trash icon-white"></i></a>
		
		<strong>Залоговая стоимость: </strong>
		<span id="pledge">$<span id="equipment_deposit_cost"><?php echo $html_values['equipment_deposit_cost']; ?></span></span>
		<a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Залоговая стоимость возвращается после возврата оборудования в исходном исправном состоянии." style="float: right;"><i class="icon-info-sign icon-white"></i></a>
	</div>
	<div>
	<script>
    jQuery(function ($) {
        $("a").tooltip()
    });
	</script>
	</div>
</div>
<div>
	<h4>
		Сумма заказа:
		<span id="cost">
			$<?php echo $html_values['cost']; ?> / <small class="costcolor">
			<?php echo ceil(($html_values['cost'])*$this->getRate()); ?> руб. </small>
		</span>
	</h4>
</div>