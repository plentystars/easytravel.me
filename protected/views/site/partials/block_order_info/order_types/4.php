﻿<h3>Ваш заказ:</h3>
<div class="calc-text" style="background: #6db343; line-height: 3; padding: 10px 10px">
	<div>
		<strong>SIM-карта: </strong>
		<span><?php echo $html_values['tariff']; ?></span>
	</div>
	<div>
		<strong>Количество: </strong>
		<span><?php echo $html_values['quantity']; ?></span>
	</div>
</div>
<div class="costcolor">
	<h4>
		Сумма заказа:
		<span>
			$<?php echo $html_values['cost']; ?> / <small class="costcolor">
			<?php echo ceil($html_values['cost']*$this->getRate()); ?> руб. </small>
		</span>
	</h4>
</div>