Номер заказа: <?php echo $order->id . "\n"; ?>
Страны: <?php echo $order->countries . "\n"; ?>
Количество дней: <?php echo $order->days . "\n"; ?>
Дата активации: <?php echo $order->date_activation . "\n"; ?>
Тариф: <?php echo $order->tariff . "\n"; ?>
Тип сим-карты: <?php echo $order->type_sim . "\n"; ?>
<?php if(isset($order->equipment)): ?>
Оборудование: <?php echo $order->equipment . "\n"; ?>
<?php endif; ?>
Стоимость: <?php echo $order->cost . "\n"; ?>
Платежная система: <?php echo $order->payment_system . "\n"; ?>

<?php $this->renderPartial('partials/email/order_types/delivery', array('delivery' => $delivery)); ?>