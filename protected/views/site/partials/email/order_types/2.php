Номер заказа: <?php echo $order->id . "\n"; ?>
Дата: <?php echo $order->datetime . "\n"; ?>
Сим карта: <?php echo $order->tariff_sim . "\n"; ?>
Тип сим-карты: <?php echo $order->type_sim . "\n"; ?>
Страна: <?php echo $order->countries . "\n"; ?>
Стоимость: <?php echo $order->cost . "\n"; ?>
Платежная система: <?php echo $order->payment_system . "\n"; ?>

<?php $this->renderPartial('partials/email/order_types/delivery', array('delivery' => $delivery)); ?>
