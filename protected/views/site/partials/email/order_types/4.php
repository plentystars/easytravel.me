Номер заказа: <?php echo $order->id . "\n"; ?>
Дата: <?php echo $order->datetime . "\n"; ?>
Название: <?php echo $order->name . "\n"; ?>
Количество: <?php echo $order->quantity . "\n"; ?>
Стоимость: <?php echo $order->cost . "\n"; ?>
Платежная система: <?php echo $order->payment_system . "\n"; ?>

<?php $this->renderPartial('partials/email/order_types/delivery', array('delivery' => $delivery)); ?>
