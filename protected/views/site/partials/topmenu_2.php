<div class="container" style="padding: 100px 0 0 0">
	<div class="row">
		<div class="span6">
			<style>
			.nav-pills-big > li > a {
				padding-top: 14px;
				padding-bottom: 14px;
				font-size: 14px;

			}
			.nav-pills-small, .nav-pills-big {
				margin-bottom: 0px;
			}
			ul.nav.nav-pills.nav-pills-big li.active ul {display: block;}
			ul.nav.nav-pills.nav-pills-big li ul {display: none;}
		</style>
		<?php
		$currentUrl = Yii::app()->request->url;
		/* вот это
		//для статических страниц
		$objectAction = Yii::app()->controller->action;
		$requestedView = NULL;
		if($objectAction->id == 'page')
			$requestedView = $objectAction->getRequestedView();
		//
			
		$this->widget('zii.widgets.CMenu',array(
			'activateParents'=>true,
			'htmlOptions'=>array('class'=>'nav nav-pills nav-pills-big', 'style'=>'font-size: 12px;'),
			'items'=>array(
				array('label'=>'Частным лицам', 'url'=>array('site/index'),
					'submenuOptions'=>array('class'=>'nav nav-pills nav-pills-small'),
					'items'=>array(
						array('label'=>'Пополнить баланс', 'url'=>array('site/addcash371372')),
						array('label'=>'Новости', 'url'=>array('page/news'), 'active'=>$requestedView=='news'),
						array('label'=>'Инструкции', 'url'=>array('page/instructions'), 'active'=>$requestedView=='instructions'),
						array('label'=>'Акции', 'url'=>array('/page/action'), 'active'=>$requestedView=='action'),
						array('label'=>'Где купить', 'url'=>array('/page/wherebuy'), 'active'=>$requestedView=='wherebuy'),
					)
				),
				array('label'=>'Корпоративным клиентам', 'url'=>array('page/corporate'), 'active'=>$requestedView=='corporate'),
				array('label'=>'Партнерам', 'url'=>array('page/partners'), 'active'=>$requestedView=='partners')
			),
		));
		*/
		?>
		<?php
			$menu_top = array(
				array('href'=>Yii::app()->createUrl('/#'), 'text'=>'Частным лицам'),
				array('href'=>Yii::app()->createUrl('page/corporate'), 'text'=>'Корпоративным клиентам'),
				array('href'=>Yii::app()->createUrl('page/partners'), 'text'=>'Партнерам'),
				array('href'=>Yii::app()->createUrl('page/about'), 'text'=>'О нас')
			);

			$menu = array(
				array('href'=>Yii::app()->createUrl('site/addcash371372'), 'text'=>'Пополнить баланс'),
				array('href'=>Yii::app()->createUrl('page/news'), 'text'=>'Новости'),
				array('href'=>Yii::app()->createUrl('page/instructions'), 'text'=>'Инструкции'),
				array('href'=>Yii::app()->createUrl('page/action'), 'text'=>'Акции'),
				array('href'=>Yii::app()->createUrl('page/wherebuy'), 'text'=>'Где купить')
			);

			$currentUrl = Yii::app()->request->url;
		?>

			<ul class="nav nav-pills nav-pills-big" style="font-size: 12px;">
			  <!-- <li class="active"><a href="#">Частным лицам</a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/corporate'); ?>">Корпоративным клиентам</a></li>
			  <li><a href="<?php echo Yii::app()->createUrl('page/partners'); ?>">Партнерам</a></li> -->
				<?php
					foreach($menu_top as $item) {
						$hrefLast = end(explode('/', $item['href']));
						//var_dump($hrefLast, $currentUrl);
						if($currentUrl == '/' && ($hrefLast == '#' || $hrefLast == ''))
							echo '<li class="active">' . '<a href="' . $item['href'] . '">' . $item['text'] . '</a>' . '</li>';
						else {
							if(stripos($currentUrl, $hrefLast) === FALSE)
								echo '<li>' . '<a href="' . $item['href'] . '">' . $item['text'] . '</a>' . '</li>';
							else
								echo '<li class="active">' . '<a href="' . $item['href'] . '">' . $item['text'] . '</a>' . '</li>';
						}
					}
				?>
			</ul>
			<ul class="nav nav-pills nav-pills-small" style="font-size: 12px;">
				<?php
					foreach($menu as $item) {
						$hrefLast = end(explode('/', $item['href']));
						if(stripos($currentUrl, $hrefLast) === FALSE)
							echo '<li>' . '<a href="' . $item['href'] . '">' . $item['text'] . '</a>' . '</li>';
						else
							echo '<li class="active">' . '<a href="' . $item['href'] . '">' . $item['text'] . '</a>' . '</li>';
					}
				?>
			</ul>
		</div>
		<div class="span6">
			<div class="socialicons">
			<a href="https://siteheart.com/webconsultation/629677" target="_blank" class="chat">Chat with Easyroaming</a>
			<a href="mailto:assist@easyroaming.ru" class="mail">Mail to Easyroaming</a>
			<a href="https://www.facebook.com/easyroaming" target="_blank" class="facebook">Like Easyroaming on Facebook</a>
			<a href="https://twitter.com/easytravelme" target="_blank" class="twitter">Follow @easyroaming on Twitter</a>
			</div>	
		</div>
	</div>	
</div>
