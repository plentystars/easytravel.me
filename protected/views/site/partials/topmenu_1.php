<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="bg_topmenu">
		<div class="container">
			<div class="row" id="topmenu">
			  <div class="span2 padtop10"><a href="<?php echo Yii::app()->homeUrl; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/logo.png"></a></div>
			  <div class="span3">
				  <ul class="menu-links" >
				  <li><a href="<?php echo Yii::app()->createUrl('site/roaming3g'); ?>">Интернет в роуминге</a></li>
				  <li><a href="<?php echo Yii::app()->createUrl('site/shopkeepgo'); ?>">Sim-карты Gb2Go</a></li>
				  </ul>
				</div>
				 <div class="span3">
				  <ul class="menu-links">
				  <li><a href="<?php echo Yii::app()->createUrl('site/calculatorprepaid'); ?>">Мобильная связь</a></li>
				  <li><a href="<?php echo Yii::app()->createUrl('page/loyalty_program'); ?>">Программа лояльности</a></li>
				  </ul>
				</div>
					 <div class="span2 padtop15" >
					 <span style="font-size: 15px;">Горячая линия</span>  

						 <style>
						 .btn-link {
							color: #2d2d2d;
							font-weight: bold;
														padding: 0px 4px;
						 }
						.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .dropdown-submenu:hover > a, .dropdown-submenu:focus > a {
						color: #ffffff;
						background-color: #C90A0A;
						background-image: -moz-linear-gradient(top, #53a753, #2d832d);
						background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#53a753), to(#2d832d));
						background-image: -webkit-linear-gradient(top, #53a753, #2d832d);
						background-image: -o-linear-gradient(top, #53a753, #2d832d);
						background-image: linear-gradient(to bottom, #53a753, #2d832d);
						}
						 </style>
						 
							<div class="btn-group">
							  <a class="btn dropdown-toggle btn-link" data-toggle="dropdown" href="#" id="dropdown-current-phone">
								(495) 212-90-06
								<span class="caret"></span>
							  </a>
							  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" id="dropdown-phones">
								  <li><a tabindex="-1" href="#">Москва +7 <span>(495) 212-90-06</a></li>
								  <li class="divider"></li>
								  <li><a tabindex="-1" href="#">Санкт-Петербург +7 <span>(812) 748-23-00</span></a></li>
								  <li class="divider"></li>
								  <li><a tabindex="-1" href="#">Нижний Новгород +7 <span>(831) 280-83-11</span></a></li>
								  <li class="divider"></li>
								  <li><a tabindex="-1" href="#">Ростов-на-Дону +7 <span>(863) 303-30-11</span></a></li>
								  <li class="divider"></li>
								  <li><a tabindex="-1" href="#">Самара +7 <span>(846) 212-95-12</span></a></li>
								</ul>
							</div>
							<script type="text/javascript">
								$('#dropdown-phones a').click(function() {
									$('#dropdown-current-phone').html($(this).find('span').text());
								});
							</script>
				</div>
				 <div class="span2 padtop25">
					<?php if(Yii::app()->user->isGuest): ?>
						<a class="btn btn-small btn-success" style="margin: -5px 0 0 0;" href="<?php echo Yii::app()->createUrl('user/login'); ?>">Войти</a>&nbsp;
						<a href="<?php echo Yii::app()->createUrl('user/registration'); ?>"><small>Регистрация</small></a>
					<?php else: ?>
						<a class="btn btn-small btn-success" style="margin: -5px 0 0 0;" href="<?php echo Yii::app()->createUrl('user/profile');; ?>">Профиль</a>&nbsp;
						<a href="<?php echo Yii::app()->createUrl('user/logout'); ?>"><small>Выйти</small></a>
					<?php endif; ?>
				</div>
				
			</div>
		</div>	
	</div>	
</div>
