<div class="container" style="text-align: left;">
<div class="row-fluid">
	<a href="#" class="btn btn-large btn-warning span12" style="margin: 10px 0;">Получить деньги</a>
</div>
<?php if($items): ?>
<table class="table table-hover">
	<tr>
		<th>Сертификат</th>
		<th>Статус</th>
	</tr>
	<?php foreach($items as $item): ?>
	<tr>
		<td><?php echo $item['certificate']; ?></td>
		<td><?php echo $item['status'] == 0 ? '<span style="color: #AA0000;">не использован</span>' : '<span style="color: #00AA00;">использован</span>'; ?></td>
	</tr>
	<?php endforeach; ?>
</table>
<?php endif; ?>
</div>
