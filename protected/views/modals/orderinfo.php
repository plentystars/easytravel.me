<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">Оформление заказа</h4>
</div>
<form class="form-horizontal" role="form">
	<div class="row">
		<div class="col-md-6 order-info">
			<div class="icalc-simtype ">
					<h2>Данные Вашего заказа</h2>
					<p class="caption">Сим-карта: <a href="#">easyroaming</a></p>
					<p class="caption">Страна: <a href="#">Италия</a></p>
					<p class="caption">Дата активации карты: <a href="#">15.09.2014</a></p>
					<p class="caption">Тарифный план: <a href="#">100 Мб/сутки</a></p>
					<p class="caption">Тип сим-карты: <a href="#">nanoSIM</a></p>
					<p class="caption">Промокод: <a href="#">-7%</a></p>
					<div class="addmoresim">
						<a href="#"><i class="fa fa-plus-square-o " style="padding-left: 10px;"></i> Добавить к заказу оборудование (роутер/модем)</a>
					</div>
					<div class="hidden-xs" style="height: 88px"></div>
					<div class="mcalc-final">
						<div class="row">
							<div class="col-sm-4">
								Сумма заказа:
							</div>
							<div class="col-sm-8 mcalc-sum">
								1285.44 $ / 48847 руб.
							</div>
						</div>
					</div>

				</div>
		</div>
		<div class="col-md-6 sms371">
				<div class="icalc-simtype orderpad">
					<h2>Заполните форму</h2>
					<div class="form-group">
						<label for="orderinfo-fio">ФИО</label>
						<input type="text" class="form-control easy-form-input" id="orderinfo-fio" placeholder="">
					 </div>
					<div class="form-group">
						<label for="orderinfo-email">Email</label>
						<input type="email" class="form-control easy-form-input" id="orderinfo-email" placeholder="">
					 </div>
					<div class="form-group">
						<label for="orderinfo-phone">Телефон</label>
						<input type="text" class="form-control easy-form-input" id="orderinfo-phone" placeholder="">
					 </div>
					<div class="form-group">
						<label for="orderinfo-city">Город</label>
						<input type="text" class="form-control easy-form-input" id="orderinfo-city" placeholder="">
					 </div>
					<div class="form-group">
						<label for="orderinfo-adress">Адрес доставки</label>
						<input type="text" class="form-control easy-form-input" id="orderinfo-adress" placeholder="">
					 </div>
					<div class="call371 sms371">
						<button type="button" class="btn btn-default">Перейти к оплате</button>
					</div>
				</div>
		</div>
	</div>
</form>