		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Тарифы по странам</h4>
		</div>
	<form class="form-horizontal" role="form">
		<div class="row">
			<div class="col-sm-6 mcalc-balance">
				Выберите сим-карту
				<p>
					<div class="btn-group">
						  <button type="button" class="btn btn-default">easyroaming</button>
						  <button type="button" class="btn btn-default">keepgo</button>
					</div>
				</p>
			</div>
			<div class="col-md-6 modal-tariffs-cchose">
				<div class="dropdown">
				  <button class="btn btn-default dropdown-toggle bt-clr-ddown" type="button" id="dropdownMenu1" data-toggle="dropdown">
					Выберите страну <i class="fa fa-chevron-down" style="padding-left: 10px;"></i> 
				  </button>
				  <ul class="dropdown-menu bt-clr-ddown" role="menu" aria-labelledby="dropdownMenu1">
					<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ангола</a></li>
					<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Бангладеш</a></li>
					<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ирак</a></li>
				  </ul>
				</div>
			</div>
		</div>
		<div class="row mcalc-tariffs padtop40">
			<div class="col-sm-1">
			</div>
			<div class="col-sm-2">
				Исх.<br/>звонок
				<p>2 $</p>
			</div>
			<div class="col-sm-2">
				VoIP<br/>звонок
				<p>1 $</p>
			</div>
			<div class="col-sm-2">
				СМС &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
				<p>1 $</p>
			</div>
			<div class="col-sm-2">
				Вход.<br/>звонок
				<p>0 $</p>
			</div>
			<div class="col-sm-2">
				Мб <br/> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
				<p>0,09 $</p>
			</div>
			<div class="col-sm-1">
			</div>
		</div>
	</form>
