<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">Доставка</h4>
</div>
<div>
	<p><strong>Москва</strong></p>
	<p>Доставка по Москве осуществляется на следующий день после оплаты заказа.</p>
	<p><strong>Стоимость доставки:</strong> 380 рублей.</p>
	<hr/>
	<p><strong>Другие регионы</strong></p>
	<p>Доставка в другие регионы осуществляется двумя способами:</p>
	<ul>
		<li>курьером</li>
		<li>почтой России</li>
	</ul>
	<p>Стоимость доставки в другие регионы уточняйте у наших операторов по телефону: <br/><strong>8 (495) 212-90-06</strong></p>

</div>