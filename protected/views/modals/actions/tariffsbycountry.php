		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Тарифы по странам</h4>
		</div>
	<form class="form-horizontal" role="form">
		<div class="row">
			<div class="col-sm-6 mcalc-balance">
				Выберите сим-карту
				<p>
					<div class="btn-group" id="chooseType">
						  <button type="button" class="btn btn-default" item-id="1">easyroaming</button>
						  <button type="button" class="btn btn-default" item-id="2">keepgo</button>
					</div>
					<input type="hidden" name="type_sim" />
				</p>
			</div>
			<div class="col-md-6 modal-tariffs-cchose">
				<input type="text" name="country_id" style="width: 100%;" disabled="disabled" placeholder="<?php echo Yii::t('app', 'CHOOSE_COUNTRY'); ?>" />
			</div>
			<script>
				var htmlCountryId	= 'input[name="country_id"]';
				var htmlTypeSim		= 'input[name="type_sim"]';
				var dataItems;
			
				$(htmlCountryId)
					.select2({data: []})
					.on('change', function() {
						
						var id = $(this).val();
						$.each(dataItems, function(index, el) {
							if (el.id == id) {
								$.each(el.tariff, function(i, e) {
									console.info(i, e);
									$('#modal-' + i).html(e);
								});
							}
						});
						//getData({id: $(htmlCountryId).val(), type: $(htmlTypeSim).val()}, '/TariffsByCountry');
					});
				
				$('#chooseType button').click(function() {
					$(this).addClass('active').siblings().removeClass('active');
					var typeSim = $(this).attr('item-id');
					$(htmlTypeSim).val(typeSim);
					$(htmlCountryId).attr('disabled', false);
					var data = getData({type: $(htmlTypeSim).val()}, '/CountriesList');
					
					if (data.success) {
						$(htmlCountryId).select2({data:data.items});
						dataItems = data.items;
					}

					return false;
				});
			</script>
		</div>
		<div class="row mcalc-tariffs padtop40">
			<div class="col-sm-1">
			</div>
			<div class="col-sm-2">
				Исх.<br/>звонок
				<p><span id="modal-call_out"></span></p>
			</div>
			<div class="col-sm-2">
				VoIP<br/>звонок
				<p><span id="modal-voip"></span></p>
			</div>
			<div class="col-sm-2">
				СМС &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
				<p><span id="modal-sms"></span></p>
			</div>
			<div class="col-sm-2">
				Вход.<br/>звонок
				<p><span id="modal-call_in"></span></p>
			</div>
			<div class="col-sm-2">
				Мб <br/> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
				<p><span id="modal-mb"></span></p>
			</div>
			<div class="col-sm-1">
			</div>
		</div>
	</form>
