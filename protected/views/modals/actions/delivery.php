<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">Оформление заказа</h4>
</div>
<div class="form-horizontal">
	<div class="row">
		<div class="col-md-6 order-info">
			<?php echo $htmlOrderInfo; ?>
			<?php //$this->renderPartial('//modals/actions/order_info', array('order'=>$order)); ?>
		</div>
		<div class="col-md-6 sms371">
				<div class="icalc-simtype orderpad">
					<h2>Заполните форму</h2>
					<?php
						$form = $this->beginWidget('CActiveForm', array(
							'enableAjaxValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
								'validateOnChange'=>false,
								'validationUrl'=>Yii::app()->createUrl('site/delivery'),
								'afterValidate'=>'js:function(form, error) {
									if ($.isEmptyObject(error)) {
										showModalAction("choosepayment");
										return false;
									}
								}'
							),
						));  
					?>
					<div class="form-group">
						<?php echo $form->label($model, 'name'); ?>
						<?php echo $form->textField($model, 'name', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'name'); ?>
					</div>
					<div class="form-group">
						<?php echo $form->label($model, 'email'); ?>
						<?php echo $form->textField($model, 'email', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'email'); ?>
					</div>
					<div class="form-group">
						<?php echo $form->label($model, 'phone_number'); ?>
						<?php echo $form->textField($model, 'phone_number', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'phone_number'); ?>
					</div>
					<div class="form-group">
						<?php echo $form->label($model, 'city'); ?>
						<?php echo $form->textField($model, 'city', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'city'); ?>
					</div>
					<div class="form-group">
						<?php echo $form->label($model, 'address'); ?>
						<?php echo $form->textField($model, 'address', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'address'); ?>
					</div>
					<div class="call371 sms371">
						<?php echo CHtml::htmlButton(Yii::t('app', 'Перейти к оплате'), array('type'=>'submit', 'class'=>'btn btn-default')); ?>
					</div>
					
					<?php $this->endWidget(); ?>
				</div>
		</div>
	</div>
</div>

<script>
	var dataOrder = getData({}, app_settings.url.getsessionorder);
	if (dataOrder.delivery) {
		$.each(dataOrder.delivery, function(key, value) {
			$('[name="Delivery[' + key + ']"]').val(value);
		});
	}
</script>