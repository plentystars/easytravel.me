<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">Оформление заказа</h4>
</div>
<div class="form-horizontal">
	<div class="row">
		<div class="col-md-6 order-info">
			<div class="icalc-simtype ">
					<h2>Данные Вашего заказа</h2>
					<p class="caption">Сим-карта: <a href="#">easyroaming</a></p>
					<p class="caption">Страна: <a href="#">Италия</a></p>
					<p class="caption">Дата активации карты: <a href="#">15.09.2014</a></p>
					<p class="caption">Тарифный план: <a href="#">100 Мб/сутки</a></p>
					<p class="caption">Тип сим-карты: <a href="#">nanoSIM</a></p>
					<p class="caption">Промокод: <a href="#">-7%</a></p>
					<!-- <div class="addmoresim">
						<a href="#"><i class="fa fa-plus-square-o " style="padding-left: 10px;"></i> Добавить к заказу оборудование (роутер/модем)</a>
					</div> -->
					<div class="hidden-xs" style="height: 88px"></div>
					<div class="mcalc-final">
						<div class="row">
							<div class="col-sm-4">
								Сумма заказа:
							</div>
							<div class="col-sm-8 mcalc-sum">
								1285.44 $ / 48847 руб.
							</div>
						</div>
					</div>

				</div>
		</div>
		<div class="col-md-6 sms371">
				<div class="icalc-simtype orderpad">
					<h2>Заполните форму</h2>
					<?php
						$form = $this->beginWidget('CActiveForm', array(
							'enableAjaxValidation'=>true,
							'clientOptions'=>array(
								'validateOnSubmit'=>true,
								'validateOnChange'=>false,
								'validationUrl'=>Yii::app()->createUrl('site/delivery'),
								'afterValidate'=>'js:function(form, error) {
									//console.info(error.length);
									return false;
								}'
							),
						));  
					?>
					<div class="form-group">
						<?php echo $form->label($model, 'name'); ?>
						<?php echo $form->textField($model, 'name', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'name'); ?>
					</div>
					<div class="form-group">
						<?php echo $form->label($model, 'email'); ?>
						<?php echo $form->textField($model, 'email', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'email'); ?>
					</div>
					<div class="form-group">
						<?php echo $form->label($model, 'phone_number'); ?>
						<?php echo $form->textField($model, 'phone_number', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'phone_number'); ?>
					</div>
					<div class="form-group">
						<?php echo $form->label($model, 'city'); ?>
						<?php echo $form->textField($model, 'city', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'city'); ?>
					</div>
					<div class="form-group">
						<?php echo $form->label($model, 'address'); ?>
						<?php echo $form->textField($model, 'address', array('class'=>'form-control easy-form-input')); ?>
						<?php echo $form->error($model, 'address'); ?>
					</div>
					<div class="call371 sms371">
						<?php echo CHtml::htmlButton(Yii::t('app', 'Перейти к оплате'), array('type'=>'submit', 'class'=>'btn btn-default')); ?>
					</div>
					
					<?php $this->endWidget(); ?>
				</div>
		</div>
	</div>
</div>