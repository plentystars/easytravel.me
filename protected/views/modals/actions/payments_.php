<div class="modal fade bs-example-modal-lg modal-payments" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Оплата</h4>
		</div>
		<div>
			<p><strong>Оплатить нашу продукцию вы можете следующими способами:</strong></p>
			<div class="row pad40">
				<div class="col-sm-6 col-md-6">
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/payments-vma.png">
				</div>
				<div class="col-sm-6 col-md-6">
					Банковской картой
				</div>
			</div>
			<div class="row pad40">
				<div class="col-sm-6 col-md-6">
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/payments-sber.png">
				</div>
				<div class="col-sm-6 col-md-6">
					По квитанции через Сбербанк
				</div>
			</div>
			<div class="row pad40">
				<div class="col-sm-6 col-md-6">
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/payments-pya.png">
				</div>
				<div class="col-sm-6 col-md-6">
					Электронными деньгами
				</div>
			</div>
			<div class="row pad40">
				<div class="col-sm-6 col-md-6">
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/payments-seq.png">
				</div>
				<div class="col-sm-6 col-md-6">
					В розничных сетях и терминалах мгновенной оплаты
				</div>
			</div>
		</div>
	</div>
  </div>
</div>