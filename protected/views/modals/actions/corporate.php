<?php 
	$form=$this->beginWidget('UActiveForm', array(
		'id'=>'login-form',
		'enableAjaxValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'afterValidate'=>'js:function(form, error) {
				if ($.isEmptyObject(error)) {
					if (returnHash != "")
						showModalAction(returnHash);
					else
						location.reload(true);
				}
					
			}',
		),
		
		//'htmlOptions' => array('enctype'=>'multipart/form-data'),
	)); 
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel"><?php echo Yii::t('app', 'FOR_CORPORATE_CLIENTS'); ?></h4>
</div>
<div class="corporate">
	<?php echo Yii::t('app', 'FOR_CORPORATE_CLIENTS_CONTENT'); ?>
		<form role="form" class="corp-form">
			<div class="row">
				<div class="col-sm-6">
          <?php echo $form->textField($model,'name', array('placeholder'=>Yii::t('app', 'DELIVERY_NAME'), 'class'=>'form-control easy-form-input')); ?>
          <?php echo $form->error($model,'name'); ?>
					
          <?php echo $form->textField($model,'email', array('placeholder'=>'Email', 'class'=>'form-control easy-form-input')); ?>
          <?php echo $form->error($model,'email'); ?>
				</div>
				<div class="col-sm-6">
          <?php echo $form->textField($model,'phone', array('placeholder'=>Yii::t('app', 'DELIVERY_PHONE_NUMBER'), 'class'=>'form-control easy-form-input')); ?>
          <?php echo $form->error($model,'phone'); ?>
          
          <?php echo CHtml::htmlButton(Yii::t('app', 'CORPORATE_APPLY'), array('type'=>'submit', 'class'=>'btn btn-default')); ?>
				</div>
			</div>
		</form>
</div>

<?php $this->endWidget(); ?>

