<div class="icalc-simtype ">

	<h2><?php echo Yii::t('app', 'ORDER_OUR_ORDER_DATA'); ?></h2>
	<p class="caption"><?php echo Yii::t('app', 'ORDER_SIM_CARD'); ?>: <a href="#">easyroaming</a></p>
	<p class="caption"><?php echo Yii::t('app', 'ORDER_COUNTRIES'); ?>: <a href="#"><?php echo $order['countries']; ?></a></p>
	<p class="caption"><?php echo Yii::t('app', 'ORDER_DATE_ACTIVATION'); ?>: <a href="#"><?php echo $order['date_activation']; ?></a></p>
	<p class="caption"><?php echo Yii::t('app', 'ORDER_TARIFF'); ?>: <a href="#"><?php echo $order['tariff']; ?></a></p>
	<p class="caption"><?php echo Yii::t('app', 'ORDER_TYPE_SIM'); ?>: <a href="#"><?php echo $order['type_sim']; ?> </a></p>
	<?php if ($order['certificate'] !== NULL): ?>
	<p class="caption"><?php echo Yii::t('app', 'ORDER_CERTIFICATE'); ?>: <a href="#"><?php echo $order['certificate']; ?></a></p>
	<?php endif; ?>
	
	<div class="hidden-xs" style="height: 88px"></div>
	<div class="mcalc-final">
		<div class="row">
			<div class="col-sm-4">
				<?php echo Yii::t('app', 'ORDER_COST'); ?>:
			</div>
			<div class="col-sm-8 mcalc-sum">
				<?php echo $order['cost_usd']; ?> $ / <?php echo $order['cost_rub']; ?> <?php echo Yii::t('app', 'ORDER_COST_RUB_SIGN'); ?>.
			</div>
		</div>
	</div>

</div>