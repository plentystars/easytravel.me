<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">Выберите способ оплаты</h4>
</div>
<div>
	<ul id="payment" class="nav nav-pills">
		<li><a payment-id="1" href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/payments/robokassa.png"></a></li>
		<!-- <li><a payment-id="2" href="#"><img src="/order/liqpay.png"></a></li>
		<li><a payment-id="3" href="#"><img src="/order/paypal.png"></a></li>
		<li><a payment-id="4" href="#"><img src="/order/yandexmoney.png"></a></li> -->
		<li><a payment-id="5" href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/payments/platezhka.png"></a></li>
	</ul>

	<div id="payment-form"></div>	
</div>
<script type="text/javascript">
	$('#payment a').click(function() {
		var id = $(this).attr('payment-id');
		var li = $(this).parent();

		li.siblings().removeClass('active');
		li.addClass('active');

		$.ajax({
			type: "POST",
			url: app_settings.url.getformpayment + '/' + id,
			dataType: 'json',
			async: false,
			data: {id: id},
			success: function(json) {
				$('#payment-form').html(json.content);
			}
		});

		return false;
	});
</script>