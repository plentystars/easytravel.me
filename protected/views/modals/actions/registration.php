<div class="icalc-simtype call371 sms371">
	<h2><?php echo Yii::t('app', 'REGISTRATION_NEW_USER'); ?></h2>

	<?php 
		$form=$this->beginWidget('UActiveForm', array(
			'id'=>'registration-form',
			'action'=>'/user/registration',
			'enableAjaxValidation'=>true,
			//'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
				'validateOnChange'=>false,
				'afterValidate'=>'js:function(form, error) {
					if ($.isEmptyObject(error)) {
						showModalPage("registration_success");
						return false;
					}
				}',
			),
			
			'htmlOptions' => array('enctype'=>'multipart/form-data'),
		)); 
	?>
	
	<?php 
		$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
				echo '<div class="form-group">';
				echo $form->label($model, $field->varname);
				
				if ($widgetEdit = $field->widgetEdit($profile)) {
					echo $widgetEdit;
				} elseif ($field->range) {
					echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
				} elseif ($field->field_type=="TEXT") {
					echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
				} else {
					echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255), 'placeholder'=>'', 'class'=>'form-control easy-form-input'));
				}

				echo $form->error($profile, $field->varname);
				echo '</div>';
			}
		}
	?>

	<div class="form-group">
		<?php echo $form->label($model, 'email'); ?>
		<?php echo $form->textField($model,'email', array('placeholder'=>'', 'class'=>'form-control easy-form-input')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->label($model, 'password'); ?>
		<?php echo $form->passwordField($model, 'password', array('placeholder'=>'', 'class'=>'form-control easy-form-input')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->label($model, 'verifyPassword'); ?>
		<?php echo $form->passwordField($model,'verifyPassword', array('placeholder'=>'', 'class'=>'form-control easy-form-input')); ?>
		<?php echo $form->error($model,'verifyPassword'); ?>
	</div>
	<div class="call371 sms371">
		<?php echo CHtml::htmlButton(Yii::t('app', 'REGISTRATION'), array('type'=>'submit', 'class'=>'btn btn-default')); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>