<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $model->name; ?></h4>
</div>

<style>

.equip-img {
	background: #fff;
	text-align: center;
}
</style>

<div>
	<div class="equip-img"><img src="<?php echo Yii::app()->request->baseUrl . $model->img; ?>"></div>
	<p><?php echo $model->description; ?></p>
<div class="row">
	<div class="col-sm-4 mcalc-capt">
		<?php echo Yii::t('app', 'COST'); ?>:  
	</div>
	<div class="col-sm-4 mcalc-sum">
		<span><?php echo $model->cost; ?></span> $
	</div>
	<div class="col-sm-4">
		<div class="btn-addequip">
			<button type="submit" class="btn btn-default" item-id="<?php echo $model->id; ?>"><?php echo Yii::t('app', 'ADD_TO_ORDER'); ?></button>
		</div>
	</div>
</div>
	

</div>

<script>
	$('#modal button[type="submit"]').bind('click', function() {
        var equipment_id = $(this).attr('item-id');
        addEquipment(equipment_id);
		$('#modal button.close').trigger('click');
		$(this).unbind('click');
        return false;
	});

</script>


