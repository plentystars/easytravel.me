<h2>Авторизация</h2>

<?php 
	$form=$this->beginWidget('UActiveForm', array(
		'id'=>'login-form',
		'enableAjaxValidation'=>true,
		//'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'afterValidate'=>'js:function(form, error) {
				if ($.isEmptyObject(error)) {
					if (returnHash != "")
						showModalAction(returnHash);
					else
						location.reload(true);
				}
					
			}',
		),
		
		'htmlOptions' => array('enctype'=>'multipart/form-data'),
	)); 
?>

<div class="form-group">
	<?php echo $form->label($model, 'username'); ?>
	<?php echo $form->textField($model,'username', array('placeholder'=>'', 'class'=>'form-control easy-form-input')); ?>
	<?php echo $form->error($model,'username'); ?>
</div>
<div class="form-group">
	<?php echo $form->label($model, 'password'); ?>
	<?php echo $form->passwordField($model, 'password', array('placeholder'=>'', 'class'=>'form-control easy-form-input')); ?>
	<?php echo $form->error($model,'password'); ?>
</div>
<div class="call371 sms371">
	<?php echo CHtml::htmlButton(UserModule::t("Login"), array('type'=>'submit', 'class'=>'btn btn-default')); ?>
</div>

<?php $this->endWidget(); ?>