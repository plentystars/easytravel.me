<div class="modal fade bs-example-modal-lg modal-loginform" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Зарегистрируйтесь или Войдите </h4>
		</div>
	<form class="form-horizontal" role="form">
		<div class="row">
			<div class="col-md-6">
					<div class="icalc-simtype call371 sms371">
						<h2>Регистрация нового пользователя</h2>
						<div class="form-group">
							<label for="reg-fio">ФИО</label>
							<input type="text" class="form-control easy-form-input" id="reg-fio" placeholder="">
						 </div>
						<div class="form-group">
							<label for="reg-email">Email</label>
							<input type="email" class="form-control easy-form-input" id="reg-email" placeholder="">
						 </div>
						<div class="form-group">
							<label for="reg-pass">Пароль</label>
							<input type="password" class="form-control easy-form-input" id="reg-pass" placeholder="">
						 </div>
						<div class="form-group">
							<label for="reg-pass-con">Повторите пароль</label>
							<input type="password" class="form-control easy-form-input" id="reg-pass-con" placeholder="">
						 </div>
						<div class="call371 sms371">
							<button type="button" class="btn btn-default">Зарегистрироваться</button>
						</div>
					</div>
			</div>
			<div class="col-md-6 sms371">
					<div class="icalc-simtype call371 sms371">
						<h2>Войти через социальные сети</h2>
							<a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/social-login-vk.png" style="padding: 12px;"></a>
							<a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/social-login-fb.png" style="padding: 12px;"></a>
						<h2>Авторизация</h2>
						<div class="form-group">
							<label for="auth-email">Email</label>
							<input type="email" class="form-control easy-form-input" id="auth-email" placeholder="">
						 </div>
						<div class="form-group">
							<label for="auth-pass">Пароль</label>
							<input type="password" class="form-control easy-form-input" id="auth-pass" placeholder="">
						 </div>
						<div class="call371 sms371">
							<button type="button" class="btn btn-default">Войти</button>
						</div>
					</div>
			</div>
		</div>

	</form>
	</div>
  </div>
</div>