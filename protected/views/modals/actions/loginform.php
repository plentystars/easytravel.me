<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel"><?php echo Yii::t('app', 'REGISTRATION_LOGIN'); ?></h4>
</div>
<div class="form-horizontal">
	<div class="row">
		<div class="col-md-6">
				<?php $this->renderPartial('//modals/actions/registration', array('model'=>$model_registration, 'profile'=>$profile)); ?>
		</div>
		<div class="col-md-6 sms371">
				<div class="icalc-simtype call371 sms371">
				<!--
					<h2>Войти через социальные сети</h2>
						<a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/social-login-vk.png" style="padding: 12px;"></a>
						<a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/social-login-fb.png" style="padding: 12px;"></a>
				-->
					<?php $this->renderPartial('//modals/actions/login', array('model'=>$userLogin)); ?>
				</div>
		</div>
	</div>
</div>