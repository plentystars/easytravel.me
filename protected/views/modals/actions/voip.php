		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Стоимость VoIP - звонка</h4>
		</div>
	<form class="form-horizontal form-voip" role="form">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label for="simtype">Выберите страну*</label>
					<select name="voip_country_id" class="form-control2">
						<!-- <option disabled="disabled" selected><?php echo Yii::t('app', 'CHOOSE_COUNTRY'); ?></option> -->
						<?php
							foreach($models as $model)
								echo '<option value="' . $model->id . '">' . $model->country_en . '</option>';
						?>
					</select>
				</div>
			</div>
		</div>
	</form>
	
	<div class="voiptable" style="display: none;">
			<div class="row voipbold">
			<div class="col-md-5">
				Страна/Оператор
			</div>
			<div class="col-md-5">
				Код
			</div>
			<div class="col-md-2">
				Цена / USD
			</div>
		</div>
		<div id="voip-tariffs"></div>
	</div>
	
		<div class="row">
			<div class="col-md-12">
				<?php echo Yii::t('app', 'VOIP_MODAL_CAPTION', array('{path}'=>Yii::app()->request->baseUrl)); ?>
			</div>
		</div>
	
	<script>
		$('[name="voip_country_id"]')
			.select2()
			.on('change', function() {
				var data = getData({id: $(this).val()}, app_settings.url.getvoiptariffs);
				if (data.success) {
					$('#voip-tariffs').html('');
					$('.voiptable').show();
					$.each(data.items, function(index, el) {
						$('#voip-tariffs').append('<div class="row"><div class="col-md-5">' + el.country_en + '</div><div class="col-md-5">' + el.code + '</div><div class="col-md-2">' + el.cost + '</div></div>');
					});
				}
			});
	</script>