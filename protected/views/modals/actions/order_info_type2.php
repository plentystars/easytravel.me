<?php //var_dump($orders); die(); ?>
<div class="icalc-simtype ">

	<h2><?php echo Yii::t('app', 'ORDER_YOUR_ORDER_DATA'); ?></h2>
	
	<p class="caption"><?php echo Yii::t('app', 'ORDER_SIM_CARD'); ?>: <a href="#">easyroaming</a></p>
	<?php foreach($orders['items'] as $order): ?>
	<div style="border: 1px #eee solid; margin: 5px; padding: 3px;">
		<p class="caption"><?php echo Yii::t('app', 'ORDER_TYPE_SIM'); ?>: <a href="#"><?php echo $order['type_sim']; ?></a></p>
		<p class="caption"><?php echo Yii::t('app', 'STARTING_BALANCE'); ?>: <a href="#"><?php echo $order['start_balans']; ?></a></p>
		<?php if ($order['added_balans'] !== ''): ?>
		<p class="caption"><?php echo Yii::t('app', 'ADD_BALANCE'); ?>: <a href="#"><?php echo $order['added_balans']; ?> $</a></p>
		<?php endif; ?>
	</div>
	<?php endforeach; ?>
	
	<?php if ($orders['certificate'] !== NULL): ?>
		<p class="caption"><?php echo Yii::t('app', 'ORDER_CERTIFICATE'); ?>: <a href="#"><?php echo $order['certificate']; ?></a></p>
	<?php endif; ?>
	
	<div class="hidden-xs" style="height: 88px"></div>
	<div class="mcalc-final">
		<div class="row">
			<div class="col-sm-4">
				<?php echo Yii::t('app', 'ORDER_COST'); ?>:
			</div>
			<div class="col-sm-8 mcalc-sum">
				<?php echo $orders['cost_usd']; ?> $ / <?php echo $orders['cost_rub']; ?> <?php echo Yii::t('app', 'ORDER_COST_RUB_SIGN'); ?>.
			</div>
		</div>
	</div>

</div>