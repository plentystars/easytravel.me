		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Стоимость VoIP - звонка</h4>
		</div>
	<form class="form-horizontal" role="form">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label for="simtype">Выберите страну*</label>
						<select name="countries" class="form-control">
						<option value="1">Греция</option>
						<option value="2">Италия</option>
						<option value="3">Япония</option>	
					</select>		  
				</div>
			</div>
		</div>
	</form>
	
	<div class="voiptable">
			<div class="row voipbold">
			<div class="col-md-5">
				Страна/Оператор
			</div>
			<div class="col-md-5">
				Код
			</div>
			<div class="col-md-2">
				Цена / USD
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				Italy
			</div>
			<div class="col-md-5">
				39
			</div>
			<div class="col-md-2">
				0,04
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				Italy
			</div>
			<div class="col-md-5">
				39
			</div>
			<div class="col-md-2">
				0,04
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				Italy
			</div>
			<div class="col-md-5">
				39
			</div>
			<div class="col-md-2">
				0,04
			</div>
		</div>
	</div>
	
		<div class="row">
			<div class="col-md-12">
				<p>*Выберите страну, в которую вы собираетесь совершить звонок.</p>
				<p>Стоимость VoIP - звонка не зависит от местоположения сим-карты, а зависит от направления совершаемого звонка.</p>
				<p>С более подробной информацию вы можете ознакомиться скачав <a href="#">файл</a> или позвонив по телефону 8 (495) 212-90-06</p>
			</div>
		</div>	