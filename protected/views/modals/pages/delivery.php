<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel"><?php echo Yii::t('app', 'DELIVERY'); ?></h4>
</div>
<div>
	<?php echo Yii::t('app', 'DELIVERY_CONTENT', array('{cost}'=>10 * $this->getRate())); ?>
</div>