<!-- <div class="modal fade bs-example-modal-lg modal-call371" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"> -->
  <!-- <div class="modal-dialog modal-lg"> -->
	<!-- <div class="modal-content"> -->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Веб-звонок</h4>
		</div>
	<!-- <form class="form-horizontal" role="form">
		<div class="row">
			<div class="col-md-6">
					<div class="icalc-simtype call371">
						<div class="form-group">
							<label for="callfrom371">Номер телефона</label>
							<input type="text" class="form-control easy-form-input" id="callfrom371" placeholder="+3712">
						 </div>
					</div>
					<p>Только для карт Easyroaming</p>
			</div>
			<div class="col-md-6">
				<div class="call371">
					<button type="button" class="btn btn-default">Позвонить</button>
				</div>
			</div>
		</div>
	</form> -->
	<iframe src="http://manager.camelmobile.com/webcalls " scrolling="no" frameborder="0" style="width: 230px; height: 222px; overflow: hidden;"></iframe>
	<!-- </div>
  </div>
</div> -->

<p>Предоставляем Вам новую услугу Beб-звонок, которая позволяет Вам здесь и сейчас, а главное БЕСПЛАТНО, звонить на сим-карту easyroaming с любого телефона.</p>
<p>Услуга осуществляется с помощью Технологии WebRTC (коммуникации реального времени). Звонки проходят через сеть Интернет без установки дополнительного программного обеспечения на компьютер пользователя, совершающего звонок. <u>Услуга работает только через браузер Google Chrome</u>.</p>
<p>Для абонента, которому Вы звоните, ответ на Ваш звонок является платной услугой. Со стоимостью звонков Вы можете ознакомиться скачав <a href="<?php echo Yii::app()->request->baseUrl; ?>/resources/files/webcall_rates.pdf" target="_blank">файл</a></p>

<p>EN</p>
<p>Introducing a new service Beb-Up, which allows you to here and now, and most importantly FREE, call the easyroaming sim card from any phone.</p>
<p>The service is carried out with the help of Technology WebRTC (real time communication). Calls pass through the Internet without having to install additional software on the user's computer. <u>The service is available only through the browser Google Chrome.</u></p>
<p>For the person you are calling, your call is a paid service. With the cost of calls, you can read by downloading a <a href="<?php echo Yii::app()->request->baseUrl; ?>/resources/files/webcall_rates.pdf" target="_blank">file</a>