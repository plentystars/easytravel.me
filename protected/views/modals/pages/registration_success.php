<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel">Регистрация</h4>
</div>
<div>
	<p>Вы успешно зарегистрировались.</p>
	<p>Пожалуйста проверьте e-mail, который Вы указали при регистрации</p>
</div>