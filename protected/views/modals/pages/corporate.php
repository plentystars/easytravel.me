<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel"><?php echo Yii::t('app', 'FOR_CORPORATE_CLIENTS'); ?></h4>
</div>
<div class="corporate">
	<?php echo Yii::t('app', 'FOR_CORPORATE_CLIENTS_CONTENT'); ?>
		<form role="form" class="corp-form">
			<div class="row">
				<div class="col-sm-6">
					<input type="text" class="form-control easy-form-input" id="name" placeholder="<?php echo Yii::t('app', 'DELIVERY_NAME'); ?>">
					<input type="email" class="form-control easy-form-input" id="email" placeholder="Email">
				</div>
				<div class="col-sm-6">
					<input type="text" class="form-control easy-form-input" id="phone" placeholder="<?php echo Yii::t('app', 'DELIVERY_PHONE_NUMBER'); ?>">
					<button type="submit" class="btn btn-default" href="#" data-toggle="modal"><?php echo Yii::t('app', 'CORPORATE_APPLY'); ?></button>
				</div>
			</div>
		</form>
</div>

