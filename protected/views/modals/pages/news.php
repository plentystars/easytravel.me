<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	<h4 class="modal-title" id="myModalLabel"><?php echo Yii::t('app', 'NEWS', array('{path}' => Yii::app()->request->baseUrl)); ?></h4>
</div>
<div>
	<?php 
		$msNews = News::model()->findAll(array('order'=>'datetime DESC'));
		foreach($msNews as $mNews):
	?>
		<div>
			<h4><span class="label label-success"><?php echo Yii::app()->dateFormatter->format("dd-MM-yyyy", $mNews->datetime); ?></span> <?php echo $mNews->title; ?></h4>
			<?php echo $mNews->description; ?>
		</div>
		<hr>
	<?php endforeach; ?>
</div>