		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title" id="myModalLabel">Отправить СМС</h4>
		</div>
	<form class="form-horizontal" role="form">
		<div class="row">
			<div class="col-md-6">
					<div class="icalc-simtype call371 sms371">
						<div class="form-group">
							<label for="callfrom371">Номер телефона</label>
							<input type="text" class="form-control easy-form-input" id="callfrom371" placeholder="+3712">
						 </div>
					</div>
			</div>
			<div class="col-md-6 sms371">
				<p class="psms371">Текст сообщения только латинскими буквами</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
					<div class="icalc-simtype call371">
						<textarea class="form-control" rows="4"></textarea>
					</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 sms371">
				<div class="radio">
				  <label>
					<input type="radio" name="translit" id="translit" value="option1">
						Преобразовать в транслит
				  </label>
				</div>
			</div>
			<div class="col-md-6">
				<div class="call371 sms371">
					<button type="button" class="btn btn-default">Отправить</button>
				</div>
			</div>
		</div>
	</form>