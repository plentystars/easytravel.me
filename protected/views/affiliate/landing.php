<?php $this->renderPartial('//partials/header'); ?>
 
<section class="container content-internet pad40">
	<div class="row">
		<div class="col-md-4"> 
			<img src="<?php echo Yii::app()->request->baseUrl; ?>/resources/img/badge-discount-7.png" class="img-responsive">
		</div>
		<div class="col-md-8"> 
			<h1><span class="ProximaNova-Bold color-orange">Поздравляем!</span></h1>
			<h2><span class="ProximaNova-Bold color-green">Мы рады приветсвовать вас на нашем сайте</span></h2>
			<p><strong>Вы перешли к нам с нашего сайта-партнера</strong> и благодаря этому получите лучшие цены за счет партнерской программы с сайтом<strong> - 7% скидки от суммы вашего заказа интернета и мобильной связи в роуминге.</strong></p>
			<hr>
			<p>Для получения скидки, при заказе, <u>введите этот код</u>: <span class="label label-warning"><?php echo $promo; ?></span></p>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="icalc-orderbtn font20">
						 <a href="<?php echo Yii::app()->createUrl('/#section-keepgo'); ?>" class="btn btn-default">Заказать Интернет</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="icalc-orderbtn font20">
						 <a href="<?php echo Yii::app()->createUrl('/#section-universal'); ?>" class="btn btn-default">Заказать мобильную связь</a>
					</div>
				</div>
			</div>					
		</div>
	</div>
</section>
	
<?php //$this->renderPartial('//partials/footer'); ?>