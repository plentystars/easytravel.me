<?php
	$this->pageTitle=Yii::app()->name;
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>

<div class="container">

		<div class="row" style="padding: 60px 0">
			<div class="span4"> 
			<img src="<?php echo $pathToImg; ?>/badge-discount-7.png">
		</div>
		<div class="span8"> 
		<h3 style="text-align: center; font-weight: bold; font-size: 30px; color: #6c9e4f;">Поздравляем!</h3>
		<h4>Мы рады привествовать вас на нашем сайте</h4>
			<p><strong>Вы перешли к нам с нашего сайта-партнера</strong> и благодаря этому получите лучшие цены за счет пратнерской программы с сайтом<strong> - 7% скидки от суммы вашего заказа интернета и мобильной связи в роуминге.</strong></p>
			<hr/>
			<p>Для получения скидки, при заказе, <u>введите этот код</u>: <span class="label label-warning" ><?php echo $promo; ?></span></p>
			<div>
				<hr/>

				<style>
					
					.green-btn-rounded {
						background-color: #6c9e4f;
						color: white;
						font-size: 115%;
						display: block;
						height: 40px;
						line-height: 40px;
						text-decoration: none;
						width: 94%;
						text-align: center;
						  -webkit-border-radius: 6px;
						 -moz-border-radius: 6px;
						border-radius: 6px;	
						padding: 20px 10px;
					}
					
					a.green-btn-rounded:hover {
						background-color: #51a351;
						color: white;
						text-decoration: none;
					}
					
					
				</style>
				
				
		<div class="row">
			<div class="span4"> 
				<a class="green-btn-rounded" href="<?php echo Yii::app()->createUrl('roaming3g'); ?>">Заказать Интернет</a>
			</div>
			<div class="span4"> 
				<a class="green-btn-rounded" href="<?php echo Yii::app()->createUrl('calculatorprepaid'); ?>">Заказать мобильную связь</a>
			</div>
		</div>

				

			</div>
		</div>
	</div>


</div>