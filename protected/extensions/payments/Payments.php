<?php
	class Payments extends CController {

		private static $alias	= 'ext.payments';
		private $settings		= NULL;
		public $order_params	= array();
		
		function __construct(){
			parent::__construct($this->id, $this->module);
		}
		
		private static function getSettings($id = NULL, $type = 'production') {
			$settings = NULL;
			
			if($id !== NULL) {
				$mPaymentSystem = PaymentSystem::model()->findByPk($id);
				if($mPaymentSystem) {
					$settings = CJSON::decode($mPaymentSystem->settings, false);
					if(isset($settings->$type))
						$settings = $settings->$type;
				}
			}

			return $settings;
		}

		public function getForm($id = NULL) {
			$name_method	= 'getForm' . $id;
			$json			= new stdClass;
			$json->success	= false;
			$json->content	= '';

			if($id !== NULL)
				$this->settings = self::getSettings($id);

			if(method_exists($this, $name_method)) {
				$json->success	= true;
				$json->content	= $this->$name_method($this->order_params);
			}

			echo CJSON::encode($json);
		}
		
		public static function getResponse($payment_id = NULL, $request = array()) {
			$name_method	= 'getResponse' . $payment_id;
			$settings		= self::getSettings($payment_id);
			$response		= false;

			if(method_exists(get_class(), $name_method))
				$response = self::$name_method($settings, $request);

			return $response;			
		}
		
		//robokassa
		private static function getResponse1($settings = NULL, $request = array()) {
			$mrh_pass2	= isset($settings->mrh_pass2) ? $settings->mrh_pass2 : NULL;
			$out_summ	= isset($request["OutSum"]) ? $request["OutSum"] : NULL;
			$inv_id		= isset($request["InvId"]) ? $request["InvId"] : NULL;
			$crc		= isset($request["SignatureValue"]) ? $request["SignatureValue"] : NULL;
			$crc		= strtoupper($crc);
			$my_crc		= strtoupper(md5("$out_summ:$inv_id:$mrh_pass2"));
			
			if($my_crc == $crc)
				return $inv_id;
			else
				return false;
		}
		
		//liqpay
		private static function getResponse2($settings = NULL, $request = array()) {
			if(isset($request['order_id'])) {
				Yii::import(self::$alias . '.libs.LiqPay');
				$liqpay = new LiqPay($settings->public_key, $settings->private_key);
				$result = $liqpay->api('payment/status', array('order_id'=>$request['order_id']));

				if(($result->status == 'success') || ($result->status == 'sandbox'))
					return $result->order_id;
				else
					return false;
			} else
				return false;
		}
		
		//paypal
		private static function getResponse3($settings = NULL, $request = array()) {
            if(isset($request['item_number'])) {
                Yii::import(self::$alias . '.libs.PayPal');
                $paypal = new PayPal();
                $result = $paypal->notify_validate($settings->action,$request);
                if ($result=="VERIFIED")
                    return $request['item_number'];
                else
                    return false;
            } else
                return false;
        }
		
        //yandexmoney
		private static function getResponse4($settings = NULL, $request = array()) {
            $result     = false;
            $order_id   = Yii::app()->request->cookies['id_ym'];
            if($order_id)
                unset(Yii::app()->request->cookies['id_ym']);
            
            Yii::setPathOfAlias('YandexMoney', Yii::getPathOfAlias(self::$alias . '.libs.YandexMoney'));
            YandexMoney\Settings::setSettings($this->settings);

            $ym = new YandexMoney\Client(YandexMoney\Settings::$APP_CLIENT_ID);
            if(isset($request['code'])) {
                $receiveTokenResp = $ym->receiveOAuthToken($request['code'], YandexMoney\Settings::$APP_REDIRECT_URI);
                
                if($receiveTokenResp->isSuccess()) {
                    $token = $receiveTokenResp->getAccessToken();
                    //p2p transfer
                    $mOrder     = Order::model()->findByPk($order_id);
                    $resp       = $ym->requestPaymentP2P($token, YandexMoney\Settings::$WALLET_STORE, $mOrder->cost);
                    $requestId  = $resp->getRequestId();
                    $resp       = $ym->processPaymentByWallet($token, $requestId);
            
                    if($resp->isSuccess())
                        $result = $mOrder->id;
                }
            }
            
            return $result;
		}
        
		//robokassa
		private function getForm1($params) {
			$values['action']		= $this->settings->action;
			$values['mrh_login']	= $this->settings->mrh_login;
			$values['mrh_pass1']	= $this->settings->mrh_pass1;
			// номер заказа
			$values['inv_id']		= $params['order_id'];
			// описание заказа
			$values['inv_desc']		= $params['description'];
			// сумма заказа
			$values['out_summ']		= $params['cost'];
			// предлагаемая валюта платежа
			$values['in_curr']		= '';//$params['currency'];
			// язык
			$values['culture']		= "ru";
			// формирование подписи
			$values['crc']  = md5("$values[mrh_login]:$values[out_summ]:$values[inv_id]:$values[mrh_pass1]");

			return $this->renderPartial(self::$alias . '.views.1', array('values'=>(object)$values), true);
		}
		
		//liqpay
		private function getForm2($params) {
			Yii::import(self::$alias . '.libs.LiqPay');
			$liqpay = new LiqPay($this->settings->public_key, $this->settings->private_key);
			return $liqpay->getForm(
				array(
					'order_id'		=> $params['order_id'],
					'amount'		=> $params['cost'],
					'currency'		=> $params['currency'],
					'description'	=> $params['description'],
					'result_url'	=> Yii::app()->createAbsoluteUrl('success'),
					'server_url'	=> Yii::app()->createAbsoluteUrl('paymentnotification/2'),
					'sandbox'		=> $this->settings->sandbox
				), '<input type="submit" style="float: right; margin: 5px 5px 5px 0" class="btn btn-warning" value="Перейти к оплате" />'
			);			
		}
		
        //paypal
        private function getForm3($params) {
            if($params['currency'] == 'RUR') $params['currency'] = 'RUB';
			$values['action']	    	= $this->settings->action;
			$values['business']		    = $this->settings->business;
			$values['amount']	    	= $params['cost'];
			$values['item_name']	    = $params['description'];
			$values['item_number']	    = $params['order_id'];
            $values['currency']	    	= $params['currency'];

			$values['notify_url']		= Yii::app()->createAbsoluteUrl('paymentnotification/3');
			$values['return']			= Yii::app()->createAbsoluteUrl('success');

			$values['currency_code']	= $params['currency'];
			return $this->renderPartial(self::$alias . '.views.3', array('values'=>(object)$values), true);
		}

		//yandexmoney
		private function getForm4($params) {
            Yii::setPathOfAlias('YandexMoney', Yii::getPathOfAlias(self::$alias . '.libs.YandexMoney'));
            YandexMoney\Settings::setSettings($this->settings);
            $scope = "payment.to-account(\"".YandexMoney\Settings::$WALLET_STORE."\",\"account\").limit(,".$params['cost'].") " .
                     "money-source(\"wallet\") ";

            $values['url'] = YandexMoney\Client::authorizeUri(YandexMoney\Settings::$APP_CLIENT_ID,YandexMoney\Settings::$APP_REDIRECT_URI , $scope);
            Yii::app()->request->cookies['id_ym'] = new CHttpCookie('id_ym', $params['order_id']);

            return $this->renderPartial(self::$alias . '.views.4', array('values'=>(object)$values), true);
		}
		
		//platezhka
		private function getForm5($params) {
			$values['amount']	= $params['cost'];
			$values['order_id']	= $params['order_id'];
			
			return $this->renderPartial(self::$alias . '.views.5', array('values'=>(object)$values), true);
		}
	}
?>
