<?php
namespace YandexMoney;

class Settings {

    static public  $APP_CLIENT_ID;
    static public  $APP_REDIRECT_URI;
    static public  $WALLET_STORE;
    static public $URI_API;
    static public $URI_AUTH;
    static public $URI_TOKEN;

static public function setSettings($settings){
    self::$APP_CLIENT_ID=$settings->app_client_id;
    self::$APP_REDIRECT_URI=$settings->app_redirect_uri;
    self::$WALLET_STORE=$settings->wallet_store;
    self::$URI_API=$settings->uri_api;
    self::$URI_AUTH=$settings->uri_auth;
    self::$URI_TOKEN=$settings->uri_token;

}


}

?>