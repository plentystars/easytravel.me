<?php

/**
 * Class PayPal
 */
class PayPal
{

    /**
     * @param       $url
     * @param array $params
     *
     * @return string
     */
    public function notify_validate($url, $params = array())
    {
        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($params as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        if( !($res = curl_exec($ch)) ) {

            curl_close($ch);
            exit;
        }
        curl_close($ch);


        if (strcmp ($res, "VERIFIED") == 0) {
            // The IPN is verified, process it
            return "VERIFIED";
        } else if (strcmp ($res, "INVALID") == 0) {
            // IPN invalid, log for manual investigation
            return "INVALID";
        }
    }


}
?>