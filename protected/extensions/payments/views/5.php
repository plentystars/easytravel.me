<p><a href="#" onclick="window.print(); return false;">Распечатайте</a> данное платежное поручение и предъявите его в кассе любого банка для оплаты заказа</p>
<table class="table table-bordered">
	<tbody>
	<tr class="success"><td>Название магазина</td><td>Лёгкий Роуминг (EasyRoaming.ru)</td></tr>
	<tr><td>Почтовый и фактический адрес:</td><td>140082, Московская область, г. Лыткарино, ул. Парковая, д. 4Б</td></tr>
	<tr><td>ИНН</td><td>503501082740</td></tr>
	<tr><td>Получатель платежа</td><td>ИП Койло Александр Эмильевич</td></tr>
	<!-- <tr><td>Наименование банка</td><td>ОАО КБ «Агропромкредит» г. Москва</td></tr> -->
	<tr><td>Расчетный счет</td><td>40802810206000000168 в ОАО КБ «АГРОПРОМКРЕДИТ»</td></tr>
	<tr><td>Корреспондентский счет</td><td>30101810400000000710 в Отделении № 4 Московского ГТУ Банка России</td></tr>
	<tr><td>БИК</td><td>044579710</td></tr>
	<tr><td>ОГРНИП</td><td>305503502000018</td></tr>
	<tr><td>Руководитель</td><td>Койло А.Э.</td></tr>
	<tr><td>Сайт</td><td>www.EasyRoaming.ru</td></tr>
	<tr><td>Телефон</td><td>+7 (495) 212-9006</td></tr>
	<tr><td>Email</td><td>easytalk@easyroaming.ru</td></tr>
	<tr class="success"><td>Заказ №</td><td><?php echo $values->order_id; ?></td></tr>
	<tr class="success"><td>Сумма</td><td><?php echo ceil($values->amount); ?> руб.</td></tr>
	</tbody>
</table>
<button type="submit" style="float: right; margin: 5px 5px 5px 0" class="btn btn-warning" onclick="window.print(); return false;">Распечатать</button>
