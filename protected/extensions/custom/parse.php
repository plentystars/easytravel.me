<?php
    ini_set("memory_limit", "256M");
	class Parse {
		public $filename = NULL;
        public $startRow = NULL;
        public $endRow = NULL;
        public $highestColumn = 'F';
        
		function updateZones() {
			$inputFileName = $this->filename;
			Yii::import('ext.phpexcel.XPHPExcel');
			
			$objPHPExcel	= XPHPExcel::createPHPExcel();
			$inputFileType	= PHPExcel_IOFactory::identify($inputFileName);
			$objReader		= PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel	= $objReader->load($inputFileName);
			$sheet			= $objPHPExcel->getSheet(1);
			$highestColumn	= $this->highestColumn;
			$startRow = $this->startRow; $endRow = $this->endRow;
            $wCountries = array();
			
			for($row = $startRow; $row <= $endRow; $row++) {
				$modelCountry = NULL;
				$data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, true, false);
				if($data) {
					$country = trim($data[0][0]);
					$zones = array(
						'zone_1' => (int)$data[0][1] > 0 ? 1 : 0,
						'zone_2' => (int)$data[0][2] > 0 ? 1 : 0,
						'zone_3' => (int)$data[0][3] > 0 ? 1 : 0,
						'zone_4' => (int)$data[0][4] > 0 ? 1 : 0,
						'zone_5' => (int)$data[0][5] > 0 ? 1 : 0
					);
					
					$modelCountry = Countries::model()->findByAttributes(array('name_ru'=>$country));
					if(!$modelCountry) {
						$wCountries[] = $country;
					} else {
						foreach($zones as $key => $value)
							$modelCountry->$key = $value;
						$modelCountry->update();
					}
				}
			}
			var_dump($wCountries);
		}
        
        function compare() {
        $inputFileName = $this->filename;
			Yii::import('ext.phpexcel.XPHPExcel');
			
			$objPHPExcel	= XPHPExcel::createPHPExcel();
			$inputFileType	= PHPExcel_IOFactory::identify($inputFileName);
			$objReader		= PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel	= $objReader->load($inputFileName);
			$sheet			= $objPHPExcel->getSheet(1);
			$highestColumn	= $this->highestColumn;
			$startRow = $this->startRow; $endRow = $this->endRow;
			$wCountries = array();
            
			for($row = $startRow; $row <= $endRow; $row++) {
				$modelCountry = NULL;
				$data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, true, false);
				if($data) {
					$country = trim($data[0][0]);

					$modelCountry = Countries::model()->findByAttributes(array('name_ru'=>$country));
					if($modelCountry === NULL)
						$wCountries[] = $country;
				}
			}
			var_dump($wCountries);
        }
/*
format xlsx file:
������		50MB/����	100MB/����	����������� 3G
����������	9,79		13,99		25,19
�������		5,59		8,39		16,79
�������		9,79		13,99		25,19
*/

		
		function importTariffsRoaming3G() {
			$inputFileName = $this->filename;
			Yii::import('ext.phpexcel.XPHPExcel');
			
			$objPHPExcel	= XPHPExcel::createPHPExcel();
			$inputFileType	= PHPExcel_IOFactory::identify($inputFileName);
			$objReader		= PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel	= $objReader->load($inputFileName);
			$sheet			= $objPHPExcel->getSheet(0);
			$highestColumn	= $this->highestColumn;
			$startRow = $this->startRow; $endRow = $this->endRow;

			for($row = $startRow; $row <= $endRow; $row++) {
				$data = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, true, false);
				$country	= trim($data[0][0]);
				$tariffs	= array(
					'cost_tariff_1'	=> $data[0][1],
					'cost_tariff_2'	=> $data[0][2],
					'cost_tariff_3'	=> $data[0][3]
				);
				$mCountries = Countries::model()->findByAttributes(array('name_ru'=>$country));
				if($mCountries) {
					$mTariffsRoaming3gSingle = new TariffsRoaming3gSingle;
					$mTariffsRoaming3gSingle->country_id = $mCountries->id;
					foreach($tariffs as $name => $tariff)
						$mTariffsRoaming3gSingle->$name = $tariff;
					
					$mTariffsRoaming3gSingle->save();
				} else {
					echo $country . '<br />';
				}
				//var_dump($data);
			}
		}
	}
?>