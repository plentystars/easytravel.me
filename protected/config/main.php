<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'EASYTRAVEL - Разумный роуминг',

	// preloading 'log' component
	'preload'=>array('log', 'languages'),
	//'sourceLanguage'=>'en',
	'language'=>'ru',

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.user.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'application.modules.rights.*',
		'application.modules.rights.models.*',
		'application.modules.rights.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1111',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array(
			//'127.0.0.1','::1', '93.78.230.54', '93.78.192.245', '77.95.200.233', '93.78.231.94'
			),
		),
        
		'user'=>array(
            # encrypting method (php hash function)
            'hash' => 'md5',
            # send activation email
            'sendActivationMail' => true,
            # allow access for non-activated users
            'loginNotActiv' => false,
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,
            # automatically login from registration
            'autoLogin' => true,
            # registration path
            'registrationUrl' => array('/user/registration'),
            # recovery password path
            'recoveryUrl' => array('/user/recovery'),
            # login form path
            'loginUrl' => array('/user/login'),
            # page after login
            'returnUrl' => array('/user/profile'),
            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
		'rights'=>array(
			//'install'=>true,
			'userNameColumn'=>'email',
		),
	),

	// application components
	'components'=>array(
		'languages'=>array(
			'class' => 'Languages'
		),
		'clientScript' => array(
			'scriptMap' => array(
				//'jquery.js' => false,
			)
		),
		'mailer' => array(
			'class' => 'application.extensions.mailer.EMailer',
			'pathViews' => 'application.views.email',
			'pathLayouts' => 'application.views.email.layouts'
		),

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			//'caseSensitive'=>false, 
			'rules'=>array(
                'gii'=>'gii',
				//'robots.txt'=>'robots.txt',
				'rights'=>'rights',
				'history'=>'history',
                'GetFormPayment/<id:\d+>'=>'site/GetFormPayment',
				'<_c:(affiliate)>/<_a:(promo)>/<id>'=>'<_c>/<_a>',
				'payment/<id:\d+>'=>'site/payment',
				'page/<view:\w+>' => 'site/page',
				'country/<id:[\w\s&\(\)\.]+>'=>'site/countrystatic',
				'language/<id:\w+>'=>'site/language',
				'calculatorprepaid/downloads_price'=>'site/downloadsprice',
                '<action:\w+>'=>'site/<action>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		'db'=>array(
     		'connectionString' => 'mysql:host=localhost;dbname=easytravel',
			'emulatePrepare' => true,
			'username' => 'easytravel',
			'password' => 'KK6Q2rBCjPufWtuX',
			'charset' => 'utf8',
            //'tablePrefix' => 'tbl_',
			'tablePrefix' => '',
        ),
        'user'=>array(
            // enable cookie-based authentication
            //'class' => 'WebUser',
			'class' =>'RWebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
			//'loginUrl'=>array('/#login')
        ),
		'authManager'=>array(
		    'class'=>'RDbAuthManager',
		    'defaultRoles'=>array('Guest')
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'messages'=>array(
			'class'	=> 'CDbMessageSource',
			'sourceMessageTable' => 'i18n_source',
			'translatedMessageTable' => 'i18n_translate'
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'easyroaming-noreply@easytravel.me',
		'nEmails'=>array('payments@easyroaming.ru'/*'andpash@gmail.com'*/),
		
		'smtp' => array(
			'host' => 'ssl://smtp.gmail.com', //smtp сервер
			'debug' => 0, //отображение информации дебаггера (0 - нет вообще)
			'auth' => true, //сервер требует авторизации
			'port' => 465, //порт (по-умолчанию - 25)
			'username' => 'andrew.p@easyroaming.ru', //имя пользователя на сервере
			'password' => 'An48dREW', //пароль
			'addreply' => '', //ваш е-mail
			'replyto' => '', //e-mail ответа
			'fromname' => 'noreply@easyroaming.ru', //имя
			'from' => 'noreply@easyroaming.ru', //от кого
			'charset' => 'utf-8',
		),
	),
);
