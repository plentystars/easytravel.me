<?php
class Affiliate extends CController {
	const COOKIE_NAME = 'certificate';

	public static function isAgent($agent_id = NULL) {
		$roles = array();
		if($agent_id)
			$roles = Rights::getAssignedRoles($agent_id);
			
		return isset($roles['Agent']) ? true : false;
	}
	
	public static function getAffiliateId($mCert = NULL) {
		
	}
	
	public static function setAgent($agent_id = NULL, $user_id = NULL) {
		if($agent_id === NULL) $agent_id = self::getCookie();
		if($user_id === NULL) $user_id = (int)Yii::app()->user->id;
		
		if(self::isAgent($agent_id) && $user_id) {
			if($agent_id != $user_id) {
				$modelAgentRelations = AgentRelations::model()->findByAttributes(array('user_id' => $user_id));
				if(!$modelAgentRelations) {
					$modelAgentRelations			= new AgentRelations;
					$modelAgentRelations->agent_id	= $agent_id;
					$modelAgentRelations->user_id	= $user_id;
					if($modelAgentRelations->save())
						return true;
				}
			}
		}
		return false;
	}
	
	public static function setCookie($id = NULL) {
		if($id)
			Yii::app()->request->cookies[self::COOKIE_NAME] = new CHttpCookie(self::COOKIE_NAME, $id);
	}
	
	public static function getCookie() {
		return isset(Yii::app()->request->cookies[self::COOKIE_NAME]) ? Yii::app()->request->cookies[self::COOKIE_NAME]->value : NULL;
	}
	
	//возвращает размер вознаграждения (p - процент, s - сумма вознаграждения) в зависимости от типа аффилиата
	public static function getAward($sum = NULL) {
		$result			= array('p'=> 0, 's'=> 0);
		$affiliate_type = NULL;
		$rolesAssigned	= Rights::getAssignedRoles(Yii::app()->user->id);
		
		if(array_key_exists('Affiliate1', $rolesAssigned))
			$affiliate_type = 1;
		elseif(array_key_exists('Affiliate2', $rolesAssigned))
			$affiliate_type = 2;
		
		if($sum)
			switch($affiliate_type) {
				case 1:
					if($sum > 20 && $sum <= 50) $result = array('p'=> 3, 's'=> $sum * 0.03);
					elseif($sum > 50 && $sum <= 100) $result = array('p'=> 4, 's'=> $sum * 0.04);
					elseif($sum > 100 && $sum <= 200) $result = array('p'=> 5, 's'=> $sum * 0.05);
					elseif($sum > 200 && $sum <= 500) $result = array('p'=> 6, 's'=> $sum * 0.06);
					elseif($sum > 500 && $sum <= 1000) $result = array('p'=> 7, 's'=> $sum * 0.07);
					elseif($sum > 1000) $result = array('p'=> 8, 's'=> $sum * 0.08);
				break;
				case 2:
					if($sum > 20 && $sum <= 50) $result = array('p'=> 1, 's'=> $sum * 0.01);
					elseif($sum > 50 && $sum <= 100) $result = array('p'=> 2, 's'=> $sum * 0.02);
					elseif($sum > 100 && $sum <= 200) $result = array('p'=> 3, 's'=> $sum * 0.03);
					elseif($sum > 200 && $sum <= 500) $result = array('p'=> 4, 's'=> $sum * 0.04);
					elseif($sum > 500 && $sum <= 1000) $result = array('p'=> 5, 's'=> $sum * 0.05);
					elseif($sum > 1000) $result = array('p'=> 6, 's'=> $sum * 0.06);
			}
		
		return $result;
	}
	
	public static function getPromo() {
		$result = NULL;
		
		$mCertUsers = CertificatesUsers::model()->findByAttributes(
			array(
				'user_id'	=> Yii::app()->user->id,
				'type'		=> 'agent'
			)
		);
		if ($mCertUsers)
			$result = $mCertUsers->rCertificates->certificate;
			
		return $result;
	}
}