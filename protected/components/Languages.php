<?php    
	class Languages extends CApplicationComponent {
        public $defaultLanguage='ru';
		
        public function init() {
			$this->initLanguage();
        }
		
        private function initLanguage() {
			$language = Yii::app()->session->get('language');
			if (!$language)
				$language = $this->defaultLanguage;
				           
            Yii::app()->setLanguage($language);
        }
		
		public static function getList($excludeCurrent = true) {
			$languages			= array('ru', 'en');
			$currentLanguage	= array(Yii::app()->language);
		
			if ($excludeCurrent)
				return array_diff($languages, $currentLanguage);
			else
				return $languages;
		}
		
		public static function setLanguage($id = NULL) {
			if ($id)
				Yii::app()->session['language'] = $id;
		}
    } 