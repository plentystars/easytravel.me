<?php
	$orders = Orders::model()->findAllByAttributes(array('user_id'=>Yii::app()->user->id), array('order'=>'datetime DESC'));
?>
	<?php if($orders): ?>
	<?php
		$objOrders = array();
		foreach($orders as $order) {
		
			$objOrder = new stdClass;

			if (isset($order->type_id)) {
				if ($order->type_id >= 1 && $order->type_id <= 5) {
					$objOrder->cost				= $order->cost;
					$objOrder->id				= $order->id;
					$objOrder->datetime			= $order->datetime;
					$objOrder->payment_status	= $order->payment_status;
				}
				
				switch($order->type_id) {
					case 1:
						if($order->rOrderType1) {
							$objOrder->name		= $order->rOrderType1->countriesName;
						}
					break;
					case 2:
						if($order->rOrderType2) {
							$objOrder->name		= $order->rOrderType2->countries;
						}
					break;
					case 3:
						if($order->rOrderType3) {
							$objOrder->name		= 'Пополнение баланса';
						}
					break;
					case 4:
						if($order->rOrderType4) {
							$objOrder->name		= OrderTypeBehavior::getKVName('order_type_4.tariff_id', $order->rOrderType4->tariff_id);
						}
					break;
					case 5:
						if($order->rOrderType5) {
							$objOrder->name		= 'Пополнение баланса ' . $order->rOrderType5->iccid;
						}
					break;
				}
			}
		
			if (isset($objOrder->name))
				$objOrders[] = $objOrder;
		}
	?>
	
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-2">Дата</div>
		<div class="col-sm-4">Заказ</div>
		<div class="col-sm-2">Сумма</div>
		<div class="col-sm-2">Статус</div>
		<div class="col-sm-1"></div>
	</div>

	<?php foreach($objOrders as $item): ?>
	<div class="panel-group" id="accordion-<?php echo ''; ?>">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion-lk" href="#lk1">
						<div class="row">
							<div class="col-sm-1"></div>
							<div class="col-sm-2"><?php echo Yii::app()->dateFormatter->format("dd.MM.yyyy", $item->datetime); ?></div>
							<div class="col-sm-4"><?php echo $item->name; ?></div>
							<div class="col-sm-2">$<?php echo $item->cost; ?></div>
							<div class="col-sm-2"><?php echo ($item->payment_status == 1 ? 'Выполнен' : 'В обработке'); ?></div>
							<div class="col-sm-1"></div>
						</div>
					</a>
				</h4>
			</div>
			<div id="lk1" class="panel-collapse collapse">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12 order-info"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
	<?php endif; ?>