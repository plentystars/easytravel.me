<?php
class Certificate {

	public static function getCost($params = array()) {
		$result	= NULL;
		$params	= self::getValidParams($params);
		$mCert	= self::getValid($params);
		if($mCert) {
			self::typeAgent($mCert);
			$result	= self::getValue($params['cost'], $mCert);
		}

		return $result;
	}

	public static function getValue($cost = NULL, $mCert = NULL) {
		$result = array();
		
		if ($cost && $mCert) {
			$discount			= 0;
			$min_order_value	= 5;
			
			if ($mCert->discount_p)
				$discount = $cost * $mCert->discount_p;
			elseif ($mCert->discount_v)
				$discount = $mCert->discount_v;
				
			if($discount > ($cost - $min_order_value))
				$discount = 0;

			$result = array(
				'cost'		=> round($cost - $discount, 2),
				'discount'	=> $discount
			);
		}

		return $result;
	}

	public static function getValid($params = array()) {
		$mCertTypes		= NULL;
		$msCertParams	= NULL;
		$result			= NULL;

		$params = self::getValidParams($params);
		
		$mCert = Certificates::model()->findByAttributes(
			array(
				'certificate'	=>$params['certificate'],
				'disabled'		=> 0
			)
		);

		if($mCert) {
			$criteria				= new CDbCriteria;
			$criteria->condition	= 'certificate_id = :certificate_id AND (order_type_id = :order_type_id OR order_type_id IS NULL)';
			$criteria->params		= array(
				':certificate_id'	=> $mCert->id,
				':order_type_id'	=> $params['type_id']
			);
			
			$mCertTypes = CertificatesTypes::model()->find($criteria);
		}
		
		if($mCertTypes) {
			$msCertParams = CertificatesParams::model()->findAllByAttributes(
				array(
					'certificate_type_id'	=> $mCertTypes->id
				)
			);
			$result = $mCert;
		}
		
		if($msCertParams) {
			foreach($msCertParams as $item) {
				if(!(isset($params[$item->name]) && ($item->value == $params[$item->name])))
					$result = NULL;
					
			}
		}

		return $result;
	}
	
	public static function getValidParams($params = array()) {
		$params_default = array(
			'certificate'	=> NULL,
			'type_id'		=> NULL,
			'tariff_id'		=> NULL,
			'type_sim_id'	=> NULL,
			'countries'		=> NULL
		);
		
		$params = array_merge($params_default, $params);
		
		foreach($params as $key => $value)
			if(is_array($value))
				$params[$key] = reset($value);
				
		return $params;
	}
	
	public static function getType($cert = NULL, $disabled = 0) {
		$result = array();

		$mCert = Certificates::model()->findByAttributes(array('certificate'=>$cert, 'disabled'=>$disabled));
		if ($mCert) {
			$msCertTypes = $mCert->rCertificatesTypes;
			if ($msCertTypes) {
				foreach($msCertTypes as $mCertTypes)
					$result[] = $mCertTypes->order_type_id !== NULL ? 'order' : $mCertTypes->custom_type;
			}
		}
		
		return array_unique($result);
	}
	
	public static function getOwner($mCert = NULL) {
		$result = NULL;
		
		if ($mCert) {
			$mCertUsers = CertificatesUsers::model()->findByAttributes(
				array(
					'certificate_id'	=> $mCert->id,
					'type'				=> 'agent'
				), 'user_id IS NOT NULL'
			);
			
			if ($mCertUsers)
				$result = $mCertUsers->user_id;
		}

		return $result;
	}
	
	public static function setRelations($mCert = NULL) {
		$result = false;
		
		if ($mCert) {		
			$mCertUsers					= self::loadModel('CertificatesUsers', array(
				'user_id'			=> Yii::app()->user->id,
				'certificate_id'	=> $mCert->id
			));

			if ($mCertUsers) $mCertUsers->save();
		
			if ($id = self::getOwner($mCert)) {
				$mAgentRelations			= self::loadModel('AgentRelations', array(
					'agent_id'	=> $id,
					'user_id'	=> Yii::app()->user->id
				));

				if ($mAgentRelations) $mAgentRelations->save();
			}
			
			$result = true;
		}
		
		return $result;
	}
	
	public static function loadModel($name = NULL, $attributes = array()) {
		$model = NULL;

		if ($name) {
			$model = $name::model()->findByAttributes($attributes);
			if ($model === NULL) {
				$model = new $name;
				$model->attributes = $attributes;
			}
		}
		
		return $model;
	}

	public static function typeAgent($mCert = NULL) {
		if(!Yii::app()->user->isGuest) {
			$mCertUsers = CertificatesUsers::model()->findByAttributes(
				array(
					'certificate_id'	=> $mCert->id,
					'type'				=> 'agent'
				), 'user_id IS NOT NULL'
			);
			
			if ($mCertUsers) {
				$mAgentRelations = AgentRelations::model()->findByAttributes(
					array(
						'user_id'	=> Yii::app()->user->id,
						'agent_id'	=> $mCertUsers->user_id
					)
				);
			
				if (!$mAgentRelations) {
					$mAgentRelations			= new AgentRelations;
					$mAgentRelations->user_id	= Yii::app()->user->id;
					$mAgentRelations->agent_id	= $mCertUsers->user_id;
					$mAgentRelations->save();
				}
			}
		}
	}

	public static function getId($certificate = NULL) {
		$result = NULL;
		
		$mCert = Certificates::model()->findByAttributes(array('certificate'=>$certificate));
		
		if ($mCert)
			$result = $mCert->id;

		return $result;
	}
}