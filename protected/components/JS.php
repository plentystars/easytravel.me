<?php
class JS {
	public static function setVars() {
		Yii::app()->clientScript->registerScript('helpers', '
			url = {
				base: ' . CJSON::encode(Yii::app()->baseUrl) . ',
				get_cost: ' . CJSON::encode(Yii::app()->createUrl('site/GetCost')) . ',
			};', CClientScript::POS_HEAD
		);
	}
}
