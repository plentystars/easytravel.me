<?php
class Notification {
	
	const PATH_VIEW_ORDER = '//site/partials/email/order_types/';

	public static function send($emails = NULL, $type = NULL, $params = array()) {
		$subject	= NULL;
		$content	= NULL;
		$count		= 1;

		if($type && $params) {
			$emails = self::getEmails($emails);

			switch($type) {
				case 'order':
					$subject = self::subjectOrder($params);
					$content = self::contentOrder($params);
				break;
			}
		}

		if($subject && $content && $emails) {
			$count = count($emails);

			foreach($emails as $email)
				SendMail::send($email, $subject, $content) === NULL ? $count-- : $count++;
		}

		return $count === 0 ? true : false;
	}
	
	private function getEmails($emails = NULL) {
		if($emails == NULL)
			$emails = Yii::app()->params['nEmails'];
			
		if(is_string($emails))
			$emails = explode(',', $emails);
			
		return $emails;
	}
	
	private function subjectOrder() {
		return 'Новый заказ на сайте ' . Yii::app()->createAbsoluteUrl(Yii::app()->homeUrl);
	}
	
	private function contentOrder($params) {
		$mOrders	= isset($params['mOrders']) ? $params['mOrders'] : NULL;
		$mOrderType	= isset($params['mOrderType']) ? $params['mOrderType'] : NULL;
		$mDelivery	= isset($params['mDelivery']) ? $params['mDelivery'] : NULL;
		
		$content = NULL;
		
		if($mOrders && $mOrderType) {
			if($mOrders->type_id) {
				$method = 'contentOrderType' . $mOrders->type_id;
				$content = self::$method($mOrders, $mOrderType, $mDelivery);
			}
		}
		
		return $content;
	}
	
	private function contentOrderType1($mOrders, $mOrderType, $mDelivery) {
		$result				= '';
		$order				= new stdClass;
		
		$order->id				= $mOrderType->order_id;
		$order->datetime		= Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $mOrders->datetime);
		$order->countries		= $mOrderType->countriesName;
		$order->days			= $mOrderType->days;
		$order->date_activation	= $mOrderType->date_activation;
		$order->tariff			= Yii::t('app', $mOrderType->tariffName, array(), NULL, 'ru');
		$order->type_sim		= $mOrderType->typeSimName;
		$order->cost			= $mOrders->cost . '$';
		$order->payment_system	= $mOrders->paymentSystemName;
		
		if($mOrderType->equipment_id)
			$order->equipment = $mOrderType->equipmentName;
		
		$delivery = false;
		if($mDelivery) {
			$delivery				= new stdClass;
			$delivery->address		= $mDelivery->fullAddress;
			$delivery->name			= $mDelivery->fullName;
			$delivery->email		= $mDelivery->email;
			$delivery->phone_number	= $mDelivery->phone_number;
		}
		
		$result = Yii::app()->controller->renderPartial(self::PATH_VIEW_ORDER . '1',
			array(
				'order'		=> $order,
				'delivery'	=> $delivery
			), true
		);

		return $result;
	}
	
	private function contentOrderType2($mOrders, $mOrderType, $mDelivery) {
		$result				= '';
		$order				= new stdClass;
		
		$order->id				= $mOrderType->order_id;
		$order->datetime		= Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $mOrders->datetime);
		$order->type_sim		= $mOrderType->typeSimName;
		$order->tariff_sim		= $mOrderType->tariffSimName;
		$order->countries		= $mOrderType->countries;
		$order->cost			= $mOrders->cost . '$';
		$order->payment_system	= $mOrders->paymentSystemName;
		
		$delivery = false;
		if($mDelivery) {
			$delivery				= new stdClass;
			$delivery->address		= $mDelivery->fullAddress;
			$delivery->name			= $mDelivery->fullName;
			$delivery->email		= $mDelivery->email;
			$delivery->phone_number	= $mDelivery->phone_number;
		}
		
		$result = Yii::app()->controller->renderPartial(self::PATH_VIEW_ORDER . '2',
			array(
				'order'		=> $order,
				'delivery'	=> $delivery
			), true
		);

		return $result;
	}
	
	private function contentOrderType3($mOrders, $mOrderType, $mDelivery) {
		$result				= '';
		$order				= new stdClass;
		
		$order->id				= $mOrderType->order_id;
		$order->datetime		= Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $mOrders->datetime);
		$order->name			= 'Пополнение баланса';
		$order->number			= $mOrderType->fullNumber;
		$order->cost			= $mOrders->cost . '$';
		$order->payment_system	= $mOrders->paymentSystemName;
		
		$result = Yii::app()->controller->renderPartial(self::PATH_VIEW_ORDER . '3',
			array(
				'order'		=> $order
			), true
		);

		return $result;
	}
	
	private function contentOrderType4($mOrders, $mOrderType, $mDelivery) {
		$result				= '';
		$order				= new stdClass;
		
		$order->id				= $mOrderType->order_id;
		$order->datetime		= Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $mOrders->datetime);
		$order->name			= $mOrderType->tariffName;
		$order->quantity		= $mOrderType->quantity;
		$order->cost			= $mOrders->cost . '$';
		$order->payment_system	= $mOrders->paymentSystemName;
		
		$delivery = false;
		if($mDelivery) {
			$delivery				= new stdClass;
			$delivery->address		= $mDelivery->fullAddress;
			$delivery->name			= $mDelivery->fullName;
			$delivery->email		= $mDelivery->email;
			$delivery->phone_number	= $mDelivery->phone_number;
		}
		
		$result = Yii::app()->controller->renderPartial(self::PATH_VIEW_ORDER . '4',
			array(
				'order'		=> $order,
				'delivery'	=> $delivery
			), true
		);

		return $result;
	}
	
	private function contentOrderType5($mOrders, $mOrderType, $mDelivery) {
		$result				= '';
		$order				= new stdClass;
		
		$order->id				= $mOrderType->order_id;
		$order->datetime		= Yii::app()->dateFormatter->format("dd.MM.yyyy HH:mm:ss", $mOrders->datetime);
		$order->name			= 'Пополнение баланса';
		$order->iccid			= $mOrderType->iccid;
		$order->tariff			= $mOrderType->tariffName;
		$order->cost			= $mOrders->cost . '$';
		$order->payment_system	= $mOrders->paymentSystemName;
		
		$result = Yii::app()->controller->renderPartial(self::PATH_VIEW_ORDER . '5',
			array(
				'order'		=> $order
			), true
		);

		return $result;
	}
}
