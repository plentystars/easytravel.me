﻿<?php
class OrderTypeBehavior extends CActiveRecordBehavior {

	public static function getKVName($key = NULL, $value = NULL) {
		$result = '';
		
		$mKeyValue = KeyValue::model()->findByAttributes(
			array(
				'key'	=> $key,
				'value'	=> $value
			)
		);
		
		if($mKeyValue)
			$result = $mKeyValue->name;
			
		return $result;
	}
}