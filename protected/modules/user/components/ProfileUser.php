<?php
	class ProfileUser extends Controller {
		public function actionHistory() {
			$model = $this->loadUser();
			$this->render('history',array(
				'model'=>$model,
				'profile'=>$model->profile,
			));
		}
	
		public function actionPromo() {
			$model = $this->loadUser();
			$this->render('promo',array(
				'model'=>$model,
				'profile'=>$model->profile,
				'promo'=>Affiliate::getPromo()
			));
		}
		
		public function actionAgentDetails() {
			$model = $this->loadUser();
			$this->render('agent_details', array(
				'model'=>$model,
				'profile'=>$model->profile,
			));
		}
		
		public function actionAgentStatistics() {
			$models = NULL;
			$model = CertificatesUsers::model()->findByAttributes(
				array(
					'user_id'	=> Yii::app()->user->id,
					'type'		=> 'agent'
				)
			);

			if ($model) {
				$models = AgentStatistics::model()->findAllByAttributes(
					array(
						'certificate_id'	=> $model->certificate_id
					)
				);
			}
			
			$this->render('agent_statistics', array(
				'models'=>$models
			));
		}
		
		/*public function actionTempUpdateCost() {
			$types = array(1, 2, 3, 4);
			foreach($types as $type) {
				$type = 'OrderType' . $type;
				$modelsOrderType = $type::model()->findAll();
				if($modelsOrderType) {
					foreach($modelsOrderType as $modelOrderType) {
						$modelOrders = Orders::model()->findByPk($modelOrderType->order_id);
						$modelOrders->cost = $modelOrderType->cost;
						$modelOrders->update();
					}
				}
			}
		}*/
		
		public function actionAgentAwards() {
			$model 		= $this->loadUser();
			$total_cost = 0;
			$items		= array();
			
			$connection = Yii::app()->db;
			$sql		= 'SELECT GROUP_CONCAT(`user_id` SEPARATOR ",") `users` FROM `agent_relations` WHERE `agent_id` = ' . Yii::app()->user->id . ' GROUP BY `agent_id`';
			$command	= $connection->createCommand($sql);
			$dataReader	= $command->query();
			$users		= $dataReader->readAll();
			
			if(count($users) > 0) {
				$users		= $users[0]['users'];
				$sql		= 'SELECT ROUND(SUM(`cost`), 2) `cost`, GROUP_CONCAT(`id` SEPARATOR ",") `ids`, year(`datetime`) `y`, month(`datetime`) `m` FROM `orders` WHERE `user_id` IN (' . $users . ') GROUP BY year(`datetime`), month(`datetime`)';
				$command	= $connection->createCommand($sql);
				$dataReader	= $command->query();
				
				$data = $dataReader->readAll();
				
				foreach($data as $item) {
					$award				= Affiliate::getAward($item['cost']);
					$item['award_s']	= $award['s'];
					$item['award_p']	= $award['p'];
					$total_cost			+= $item['award_s'];
					$items[]			= $item;
				}
			}
			
			$this->render('agent_awards', array(
				'model'			=> $model,
				'profile'		=> $model->profile,
				'items'			=> $items,
				'total_cost'	=> $total_cost
			));
		/*
		foreach($dataReader as $row) {
			$award = $this->getAward($row['sum']);
			$result->total_award += $award['s'];
			$result->rows[] = array('date'=>date("m.Y", $row['date']), 'sum'=>$row['sum'], 'award'=>$award['p']);
		}*/
		
		
		
		
		
			/*$modelsAgentRelations = AgentRelations::model()->findAllByAttributes(array('agent_id'=>Yii::app()->user->id));
			if($modelsAgentRelations)
				var_dump($modelsAgentRelations); die();*/
		}
	}
?>