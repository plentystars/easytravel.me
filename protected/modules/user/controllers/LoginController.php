<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$this->redirect(Yii::app()->homeUrl . '#login');
		} else
			$this->redirect(Yii::app()->homeUrl);
			//$this->redirect(Yii::app()->user->returnUrl);
		Yii::app()->end();
		
		
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$this->lastViset();
					//$this->redirectOrder();
					if (Yii::app()->user->returnUrl=='/index.php')
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl);
				}
			}
			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else {
			//$this->redirectOrder();
			$this->redirect(Yii::app()->user->returnUrl);
			//$this->redirect(Yii::app()->controller->module->returnUrl);
		}
	}

	public function actionAjaxLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$this->lastViset();
					if (Yii::app()->user->returnUrl=='/index.php')
						echo '{"success":true, "url": "' . Yii::app()->controller->module->returnUrl . '"}';
					else
						echo '{"success":true, "url": "' . Yii::app()->user->returnUrl . '"}';
					Yii::app()->end();
				}
			}
			echo '{"success":false}';
		} else echo '{"success":false}';
		Yii::app()->end();
	}
	
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		//var_dump($lastVisit->lastvisit); die();
		$lastVisit->save();
	}

	/*private function redirectOrder() {
		$controllerSite = Yii::app()->createController('Site');
		if($controllerSite[0]->issetOrderInSession()) {
			$this->redirect(Yii::app()->createUrl('/site/delivery'));
			Yii::app()->end();
		}
	}*/
}
