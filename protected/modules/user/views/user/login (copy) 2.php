<?php
	$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
	$pathToImg = Yii::app()->request->baseUrl . '/resources/img';
?>
<div class="container">
	<div class="row" style="padding: 0 0 60px 0">
	<div style="padding: 50px 0 0 0" class="row">
		<div style="min-height: 340px;" class="span4 brotherbox-index">
			<h4>Новый клиент</h4>
				<form style="padding: 15px 15px 0px 15px">
				<div style="margin: 0 auto; text-align: center;">
					<?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl, array('class'=>'btn btn-large btn-warning')); ?>
				</div>
				<div style="text-align: center; padding: 20px 0 0 0"><i>или</i><br><br>войти через соцсеть</div>
			<p style="margin: 0px 10px; text-align: center;"><img src="<?php echo $pathToImg; ?>/social_login.png"></p>
				</form>
			
			</div>
		<div style="min-height: 340px;" class="span4 brotherbox-index">

			<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

			<div class="success">
				<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
			</div>

			<?php endif; ?>

			<h4><?php echo UserModule::t("Login"); ?></h4>

			<div class="form">
			<?php echo CHtml::beginForm(); ?>

				<?php echo CHtml::errorSummary($model); ?>
				<div style="margin: 0 auto; text-align: center;">
					<?php echo CHtml::activeTextField($model,'username', array('class'=>'span3', 'placeholder'=>'Email')) ?>
					<?php echo CHtml::activePasswordField($model,'password', array('class'=>'span3', 'placeholder'=>'Пароль')) ?>
				</div>
					<?php echo CHtml::submitButton(UserModule::t("Login"), array('class'=>'btn btn-warning', 'style'=>'float: right; margin: 15px 5px 15px 0')); ?>
				<br><br><br><?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl, array('style'=>'float:right;')); ?>
	
			<?php echo CHtml::endForm(); ?>
			</div><!-- form -->

		</div>
	</div>
</div>
</div>

<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>
