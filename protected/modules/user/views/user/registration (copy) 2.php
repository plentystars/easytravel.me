<style>
input.error {border: 1px #ff0000 solid;}
</style>
<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
	UserModule::t("Registration"),
);
?>

<div class="container" style="text-align: left;">
	<div class="row">
		<div class="span12">
			<h2><?php echo UserModule::t("Registration"); ?></h2>

			<?php if(Yii::app()->user->hasFlash('registration')): ?>
			<div class="success">
			<?php echo Yii::app()->user->getFlash('registration'); ?>
			</div>
			<?php else: ?>

			<p>Регистрация позволит получать еще больше бесплатной мобильной связи в международном роуминге благодаря нашей <a href="#">программе лояльности</a>. </p>
		</div>
	</div>
	
	<div class="row">
		<div class="span6">
		
					<h3>Заполните форму</h3>
					<?php $form=$this->beginWidget('UActiveForm', array(
						'id'=>'registration-form',
						'enableAjaxValidation'=>true,
						'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
						'clientOptions'=>array(
							'validateOnSubmit'=>true,
						),
						'htmlOptions' => array('enctype'=>'multipart/form-data'),
					)); ?>


					<?php 
						$profileFields=$profile->getFields();
						if ($profileFields) {
							foreach($profileFields as $field) {
								if ($widgetEdit = $field->widgetEdit($profile)) {
									echo $widgetEdit;
								} elseif ($field->range) {
									echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
								} elseif ($field->field_type=="TEXT") {
									echo$form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
								} else {
									echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255), 'placeholder'=>$field->title)) . '&nbsp;';
								}

								//echo $form->error($profile,$field->varname);
							}
						}
					?>

					<?php echo $form->textField($model,'email', array('placeholder'=>'Email', 'class'=>'span5')); ?>
					<?php //echo $form->error($model,'email'); ?>

					<?php echo $form->passwordField($model,'password', array('placeholder'=>'Пароль')); ?>
					<?php //echo $form->error($model,'password'); ?>

					<?php echo $form->passwordField($model,'verifyPassword', array('placeholder'=>'Повторите Пароль')); ?>
					<?php //echo $form->error($model,'verifyPassword'); ?>
					<div align="center"><?php echo CHtml::submitButton(UserModule::t("Register"), array('class'=>'btn btn-large btn-primary')); ?></div>
		<?php $this->endWidget(); ?>
		</div>
		<div class="span6">
			<h3>Войдите через аккаунт соц.сети</h3>
			<p style="margin: 50px 100px;"><img src="img/social_login.png"></p>
		</div>
	</div>
	
	
		<div class="row">
			<div class="span12">
				<p>Регистрируясь или Авторизируясь на нашем сайте вы соглашаетесь с <a href="#">Правилами пользователя</a> и <a href="#">Политикой конфиденциальности</a>, а также даете согласие на обработку ваших персональных данных. </p>
			</div>
		</div>
	<?php endif; ?>
</div>
