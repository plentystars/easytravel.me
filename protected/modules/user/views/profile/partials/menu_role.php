<nav class="navbar navbar-default bt-clr-nav lk-nav" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">

      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
		<span class="sr-only">Навигация</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
   
	<?php
		$menu_left	= '';
		$menu_right	= '';

		foreach($items as $item) {
			if (isset($item['last']) && $item['last'])
				$menu_right .= '<li><a href="' . $item['url'] . '">' . $item['label'] . '</a></li>';
			else
				$menu_left .= '<li><a href="' . $item['url'] . '">' . $item['label'] . '</a></li><li class="hidden-xs"><span>|</span></li>';
		}
	?>
   
   
  <div class="container">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
	  <ul class="nav navbar-nav">
		<?php echo $menu_left; ?>
	  </ul>
	  <ul class="nav navbar-nav navbar-right">
		<?php echo $menu_right; ?>
	  </ul>
	</div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>