<?php
	$moduleName		= Yii::app()->controller->module->id;
	$controllerName	= Yii::app()->controller->id;
	$url			= '/' . $moduleName . '/' . $controllerName . '/';
	
	if(Yii::app()->user->checkAccess('Agent'))
		$this->renderPartial('partials/menu_role', 
			array('items' => array(
				array('label'=>UserModule::t('На главную'), 'url'=>Yii::app()->homeUrl),
				array('label'=>UserModule::t('Profile'), 'url'=>$url . 'profile'),
				array('label'=>UserModule::t('Edit'), 'url'=>$url . 'edit'),
				array('label'=>UserModule::t('Change password'), 'url'=>$url . 'changepassword'),
				array('label'=>UserModule::t('Статистика'), 'url'=>$url . 'agentstatistics'),
				array('label'=>UserModule::t('Баннеры'), 'url'=>$url . 'promo'),
				//array('label'=>UserModule::t('Реквизиты'), 'url'=>$url . 'agentdetails'),
				//array('label'=>UserModule::t('Бонусы'), 'url'=>$url . 'agentawards'),
				array('label'=>UserModule::t('Logout'), 'url'=>'/' . $moduleName . '/logout', 'last'=>true)
			))
		);
	elseif(Yii::app()->user->checkAccess('Authenticated'))
		$this->renderPartial('partials/menu_role',
			array('items' => array(
				array('label'=>UserModule::t('На главную'), 'url'=>Yii::app()->homeUrl),
				array('label'=>UserModule::t('Profile'), 'url'=>$url . 'edit'),
				array('label'=>UserModule::t('Change password'), 'url'=>$url . 'changepassword'),
				array('label'=>UserModule::t('История заказов'), 'url'=>$url . 'history'),
				array('label'=>UserModule::t('Logout'), 'url'=>'/' . $moduleName . '/logout', 'last'=>true)
			))
		);
		
	else
		$this->renderPartial('partials/menu_role',
			array('items' => array(
				array('label'=>UserModule::t('На главную'), 'url'=>Yii::app()->homeUrl),
				array('label'=>UserModule::t('Profile'), 'url'=>$url . 'edit'),
				array('label'=>UserModule::t('Change password'), 'url'=>$url . 'changepassword'),
				array('label'=>UserModule::t('Logout'), 'url'=>'/' . $moduleName . '/logout', 'last'=>true)
			))
		);
?>