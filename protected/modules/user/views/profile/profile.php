<?php
	$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
	$this->renderPartial('partials/menu');
?>
<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
	<div class="alert alert-success">
		<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
	</div>
<?php endif; ?>
<section class="container content-internet faq pad40 profile">
	<h1>Профиль</h1>
	<?php 
		$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
		if($profileFields)
			foreach($profileFields as $field) {
	?>
				<div class="row">
					<div class="col-sm-3"></div>
					<div class="col-sm-3 lk-data"><?php echo CHtml::encode(UserModule::t($field->title)); ?></div>
					<div class="col-sm-3"><?php echo (($field->widgetView($profile))?$field->widgetView($profile):CHtml::encode((($field->range)?Profile::range($field->range,$profile->getAttribute($field->varname)):$profile->getAttribute($field->varname)))); ?></div>
					<div class="col-sm-3"></div>
				</div>
	<?php } ?>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3 lk-data"><?php echo CHtml::encode($model->getAttributeLabel('email')); ?></div>
		<div class="col-sm-3"><?php echo CHtml::encode($model->email); ?></div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3 lk-data"><?php echo CHtml::encode($model->getAttributeLabel('create_at')); ?></div>
		<div class="col-sm-3"><?php echo $model->create_at; ?></div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3 lk-data"><?php echo CHtml::encode($model->getAttributeLabel('status')); ?></div>
		<div class="col-sm-3"><?php echo CHtml::encode(User::itemAlias("UserStatus",$model->status)); ?></div>
		<div class="col-sm-3"></div>
	</div>
</section>