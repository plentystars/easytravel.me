<div class="container" >
	<?php if(count($items)): ?>
	<table class="table table-striped table-bordered" style="margin-top: 20px;">
	  <thead>
		<tr>
		  <th>дата</th>
		  <th>сумма покупок, $</th>
		  <th>вознаграждение, %</th>
		  <th>к выплате, $</th>
		</tr>
	  </thead>
	  <tbody>
		<?php foreach($items as $item): ?>
		<tr>
		  <td><?php echo $item['m'] . '.' . $item['y'] ; ?></td>
		  <td><?php echo $item['cost']; ?></td>
		  <td><?php echo $item['award_p']; ?></td>
		  <td><?php echo $item['award_s']; ?></td>
		</tr>
		<?php endforeach; ?>
		<tr>
			<tr>
				<td colspan="3" style="text-align: right;"><strong>Итого:</strong></td>
				<td><strong><?php echo $total_cost; ?></strong></td>
			</tr>
		</tr>
	  </tbody>
	</table>
	
	<div style="text-align: right;">
	<div class="btn-group">
		<button data-toggle="dropdown" class="btn dropdown-toggle btn-success btn-large">Вывести <span class="caret"></span></button>
		<ul class="dropdown-menu" style="text-align: left;">
		  <li><a href="#">Банк</a></li>
		  <li><a href="#">PayPal</a></li>
		  <li><a href="#">YandexMoney</a></li>
		</ul>
	</div>
	</div>
	<?php else: ?>
		<div class="well" style="margin-top: 10px;">
		<h4 style="text-align: center;">В данный момент у Вас нет бонусов</h4>
		</div>
	<?php endif; ?>
	<div class="row" style="padding: 20px 0 60px 0">
		<div class="span12">
			<?php
				$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
				$this->breadcrumbs=array(UserModule::t("Profile"),);
				$this->renderPartial('partials/menu');
			?>
		</div>
		
		<div class="span12">
			<div class="row">
				<div class="span12"></div>
				<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
					<div class="success">
						<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
</div>

<script type="text/javascript">
</script>

