<div class="container" >
	<div class="row" style="padding: 0px 0 60px 0">
		<div class="span12">
			<?php
				$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
				$this->renderPartial('partials/menu');
				$this->renderPartial('partials/promo', array('promo'=>$promo));
			?>
		</div>
		
		<div class="span12">
			<div class="row">
				<div class="span12">
					<h4>Баннер для вашего сайта</h4>

					<div class="tabbable">
						<ul class="nav nav-pills">
							<li class=""><a href="#tab_1" data-toggle="tab">160x600 px</a></li>
							<li class=""><a href="#tab_2" data-toggle="tab">300x250 px</a></li>
							<li class="active"><a href="#tab_3" data-toggle="tab">728x90 px</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" id="tab_1">
								<?php $this->renderPartial('partials/banner', array('width' => 160, 'height' => 600, 'promo'=>$promo)); ?>
							</div>
							<div class="tab-pane" id="tab_2">
								<?php $this->renderPartial('partials/banner', array('width' => 300, 'height' => 250, 'promo'=>$promo)); ?>
							</div>
							<div class="tab-pane active" id="tab_3">
								<?php $this->renderPartial('partials/banner', array('width' => 728, 'height' => 90, 'promo'=>$promo)); ?>
							</div>
						</div>
					</div>
				</div>
				<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
					<div class="success">
						<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
</div>	

