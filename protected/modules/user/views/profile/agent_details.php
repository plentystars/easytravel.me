<div class="container" >
	<div class="row" style="padding: 20px 0 60px 0">
		<div class="span12">
			<?php
				$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
				$this->breadcrumbs=array(UserModule::t("Profile"),);
				$this->renderPartial('partials/menu');
			?>
		</div>
		
		<div class="span12">
		    <div class="tabbable tabs-left">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#tab_1">Банк</a></li>
					<li><a data-toggle="tab" href="#tab_2">PayPal</a></li>
					<li><a data-toggle="tab" href="#tab_3">Яндекс Деньги</a></li>
				</ul>
				<div class="tab-content">
					<div id="tab_1" class="tab-pane active">
						<select name="affilate-type">
							<option value="1">Юридическое лицо</option>
							<option value="2">Физическое лицо</option>
						</select>
						<div id="affilate-type-1">
						<form>
								<div class="row-fluid">
									<div class="span3">ИНН</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">ОГРН</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">Юридический адрес</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">счет</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">банк</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">корр. счет</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">БИК</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">контактный телефон</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">e-mail</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span6" style="text-align: right;"><button class="btn btn-success">Сохранить</button></div>
								</div>
							</form>
						</div>
						<div id="affilate-type-2">
							<form>
								<div class="row-fluid">
									<div class="span3">ИНН</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">место жительства</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">счет</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">банк</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">корр. счет</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">БИК</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">контактный телефон</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span3">e-mail</div>
									<div class="span3"><input type="text" name="" /></div>
								</div>
								<div class="row-fluid">
									<div class="span6" style="text-align: right;"><button class="btn btn-success">Сохранить</button></div>
								</div>
							</form>
						</div>
					</div>
					<div id="tab_2" class="tab-pane">
						<form>
							<p>Укажите кошелек PayPal: <input type="text" style="margin: 0;" /> <button type="submit" class="btn btn-success">Сохранить</button></p>
						</form>
					</div>
					<div id="tab_3" class="tab-pane">
						<form>
							<p>Укажите кошелек Яндекс Деньги: <input type="text" style="margin: 0;" /> <button type="submit" class="btn btn-success">Сохранить</button></p>
						</form>
					</div>
				</div>
			</div>
		
			<div class="row">
				<div class="span12"></div>
				<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
					<div class="success">
						<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
</div>

<script type="text/javascript">
	getForm();
	
	$('select[name="affilate-type"]').change(function() {getForm();});
	
	function getForm() {
		var value = $('select[name="affilate-type"]').val();
		$('div[id*="affilate-type"]').hide();
		$('#affilate-type-' + value).show();
	}
</script>

