<?php
	$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
	$this->renderPartial('partials/menu');
?>
<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
	<div class="alert alert-success">
		<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
	</div>
<?php endif; ?>
<section class="container content-internet faq pad40 profile">
	<h1>Профиль</h1>

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'profile-form',
		'enableAjaxValidation'=>true,
		'htmlOptions' => array('enctype'=>'multipart/form-data'),
	)); ?>
	
	<?php 
		$profileFields=$profile->getFields();
		if($profileFields) {
			foreach($profileFields as $field) {
	?>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="form-group">
			<?php echo $form->labelEx($profile,$field->varname, array('class'=>'col-sm-3 easy-form-label')); ?>
			<div class="col-sm-3 ">
			<?php
				if($widgetEdit = $field->widgetEdit($profile)) {
					echo $widgetEdit;
				} elseif ($field->range) {
					echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
				} elseif ($field->field_type=="TEXT") {
					echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
				} else {
					echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255), 'class'=>'form-control easy-form-input'));
				}
				echo $form->error($profile,$field->varname);
			?>
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<?php } } ?>

	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<div class="addcashbtn">
				<?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class'=>'btn btn-default')); ?>
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<?php $this->endWidget(); ?>
</section>