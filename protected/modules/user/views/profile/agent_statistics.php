<?php
	$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Agent Statistics");
	$this->renderPartial('partials/menu');
?>
<section class="container content-internet faq pad40 profile">
	<?php if ($models): ?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Дата/время</th>
					<th>Источник</th>
					<th>Браузер</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($models as $model): ?>
				<tr>
				  <td><?php echo $model->datetime; ?></td>
				  <td><?php echo $model->referer !== NULL ? $model->referer : '-'; ?></td>
				  <td><?php echo $model->user_agent; ?></td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	<?php else: ?>
		<h3>На страницу с вашим промо-кодом не было переходов</h3>
	<?php endif; ?>
</section>