<?php
	$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password");
	$this->renderPartial('partials/menu');
?>
<section class="container content-internet faq pad40 profile">
	<h1>Изменить пароль</h1>
	<?php
		$form=$this->beginWidget('CActiveForm', array(
			'id'=>'changepassword-form',
			'enableAjaxValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		));
	?>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'oldPassword', array('class'=>'col-sm-3 easy-form-label')); ?>
			<div class="col-sm-3 ">
				<?php echo $form->passwordField($model, 'oldPassword', array('class'=>'form-control easy-form-input')); ?>
				<?php echo $form->error($model,'oldPassword'); ?>
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'password', array('class'=>'col-sm-3 easy-form-label')); ?>
			<div class="col-sm-3 ">
				<?php echo $form->passwordField($model,'password', array('class'=>'form-control easy-form-input')); ?>
				<?php echo $form->error($model,'password'); ?>
				<!-- <p class="hint"><?php echo UserModule::t("Minimal password length 4 symbols."); ?></p> -->
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'verifyPassword', array('class'=>'col-sm-3 easy-form-label')); ?>
			<div class="col-sm-3 ">
				<?php echo $form->passwordField($model,'verifyPassword', array('class'=>'form-control easy-form-input')); ?>
				<?php echo $form->error($model,'verifyPassword'); ?>
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<div class="addcashbtn">
				<?php echo CHtml::submitButton(UserModule::t("Save"), array('class'=>'btn btn-default')); ?>
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
	<?php $this->endWidget(); ?>
</section>