<?php
	$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
	$this->renderPartial('partials/menu');
?>
<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
			<div class="success">
				<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
			</div>
			<?php endif; ?>
<section class="container content-internet faq pad40 profile">
	<h1>История заказов</h1>
	<?php $this->widget('application.components.HistoryWidget'); ?>
</section>